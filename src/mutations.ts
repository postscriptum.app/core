import * as ranges from "./util/ranges.js";
import {breakAtPoint, unbreak} from "./breaks.js";
import type {Position} from "./breaks.js";

export type Mutation = {
	revert(): void;
	[key: string]: any;
};

export class Mutations {
	protected _entries: Mutation[] = [];

	push(...entry: Mutation[]): void {
		this._entries.push(...entry);
	}

	revert(): void {
		let entry: Mutation;
		while ((entry = this._entries.pop())) entry.revert();
	}

	commit(target?: Mutations): void {
		if (target) target.push(...this._entries);
		this._entries.length = 0;
	}

	setProp<T>(obj: T, propName: keyof T, propValue?: any): void {
		const prevValue = obj[propName];
		obj[propName] = propValue;
		this.push({
			obj,
			propName,
			prevValue,
			revert(): void {
				this.obj[this.propName] = this.prevValue;
			}
		});
	}

	setAttr(elem: Element, attrName: string, attrValue: string = ''): void {
		const prevValue = elem.getAttribute(attrName);
		elem.setAttribute(attrName, attrValue);
		this.push({
			elem,
			attrName,
			prevValue,
			revert(): void {
				if (this.prevValue === null) this.elem.removeAttribute(this.attrName);
				else this.elem.setAttribute(this.attrName, this.prevValue);
			}
		});
	}

	removeAttr(elem: Element, attrName: string): void {
		if (elem.hasAttribute(attrName)) {
			const prevValue = elem.getAttribute(attrName);
			elem.removeAttribute(attrName);
			this.push({
				elem,
				attrName,
				prevValue,
				revert(): void {
					this.elem.setAttribute(this.attrName, this.prevValue);
				}
			});
		}
	}

	setStyle(elem: HTMLElement, props: { [name: string]: string }): void {
		const beforeProps: { [name: string]: string } = {};
		for (const propName in props) {
			beforeProps[propName] = elem.style.getPropertyValue(propName);
			elem.style.setProperty(propName, props[propName]);
			if (!elem.getAttribute('style')) elem.removeAttribute('style');
		}
		this.push({
			elem,
			beforeProps,
			revert(): void {
				for (const propName in this.beforeProps) this.elem.style.setProperty(propName, this.beforeProps[propName]);
				if (!this.elem.getAttribute('style')) this.elem.removeAttribute('style');
			}
		});
	}

	setAnimations(elem: HTMLElement): void {
		this.push({
			elem,
			style: elem.getAttribute('style'),
			revert() {
				this.elem.style.animation = '';
			}
		});
		for (const anim of elem.getAnimations()) anim.commitStyles();
	}

	addClass(elem: HTMLElement, ...classNames: string[]): void {
		elem.classList.add(...classNames);
		this.push({
			elem,
			classNames,
			revert(): void {
				this.elem.classList.remove(...this.classNames);
			}
		});
	}

	breaks<T extends HTMLElement>(source: T, breakPoint: ranges.Point, position?: Position): T {
		const dest = breakAtPoint(source, breakPoint, position);
		this.push({
			before: position == "before" ? dest : source,
			after: position == "before" ? source : dest,
			trace: JSON.stringify(new Error().stack),
			position,
			revert(): void {
				unbreak(this.before, this.after, this.position == "before" ? "after" : "before");
			}
		});
		return dest;
	}

	insertNode(parent: Node, newChild: Node, refChild?: Node): void {
		parent.insertBefore(newChild, refChild);
		this.push({
			node: newChild,
			revert(): void {
				this.node.remove();
			}
		});
	}

	appendNodeContents(srcNode: Node, dstNode: Node): void {
		const lastDstChild = dstNode.lastChild;
		ranges.appendNodeContents(srcNode, dstNode);
		this.push({
			srcNode,
			dstNode,
			lastDstChild,
			revert(): void {
				const range = document.createRange();
				range.selectNodeContents(this.dstNode);
				if (this.lastDstChild) range.setStartAfter(this.lastDstChild);
				this.srcNode.appendChild(range.extractContents());
			}
		});
	}

	pushToArray<T>(array: Array<T>, item: T): void {
		array.push(item);
		this.push({
			array,
			item,
			revert() {
				array.pop();
			}
		});
	}
}

export default Mutations;
