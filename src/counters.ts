import valueParser from 'postcss-value-parser';
import {logger, ranges, walk} from './util.js';
import type {Fragment} from './fragments';
import type {Point} from "./util/ranges";

type CounterData = { value: number, scope: HTMLElement, pseudoScope?: boolean };
type CountersData = { [counterName: string]: CounterData };

export type Counters = Map<HTMLElement, CountersData>;

export type CountersPseudo = "before" | "after";

export type CountersStack = {
	[counterName: string]: { element: Element, value: number, elemScope?: boolean }[]
};


export type CountersValue = {
	[counterName: string]: number
};

export function parseCounterValue(value: string): CountersValue {
	const counters: CountersValue = {};
	if (value == 'none') return counters;
	const vals = valueParser(value);

	const words = vals.nodes.filter((node) => node.type == 'word');
	for (let i = 0; i < words.length; i++) {
		const name = words[i].value;
		const value = parseInt(words[i + 1].value);
		i++;
		counters[name] = value;
	}
	return counters;
}


// TODO Possible problem if a counter-reset or counter-increment exists on the documentElement
export function getCounterValue(element: Element, counterName: string): number {
	const doc = element.ownerDocument;
	const elements = walk.createWalker(doc.documentElement, walk.isElement);
	let counterValue = 0;
	elements.currentNode = element;
	while (elements.currentNode != doc.documentElement) {
		let counterReseted = false;
		const style = getComputedStyle(elements.currentNode);
		if (style.display != 'none') {
			const resets = parseCounterValue(style.counterReset);
			if (counterName in resets) {
				counterValue += resets[counterName];
				counterReseted = true;
			}

			const increments = parseCounterValue(style.counterIncrement);
			if (counterName in increments) counterValue += increments[counterName];

			if (counterReseted) return counterValue;
		}
		elements.previousNode();

	}
	logger.warn(`Counter '${counterName}' without a counter-reset.`);
	return counterValue;
}

export function getPageCounterValue(pages: Fragment[], element: Element): number {
	let pageBox = element;
	while (pageBox && pageBox.localName != 'ps-page') pageBox = pageBox.parentElement;

	for (let i = 0; i < pages.length; i++) {
		if (pageBox == pages[i].container) return i + 1;
	}

	throw "Page not found";
}

// TODO When a counter-increment of a counter without scope is set on a pseudo, the counter-reset scope does not include the brothers
export function parseCounters(root: Element, stop: Point): CountersStack {
	const counters: CountersStack = {};
	const stopNode = ranges.nodeAtPoint(stop);
	const docElem = root.ownerDocument.documentElement;

	// Lookup the start node
	let start = root;
	while (start != docElem && !start.hasAttribute('ps-counters')) start = start.parentElement;
	const elements = walk.createWalker(start, walk.isElement);
	do {
		if (elements.currentNode == stopNode || elements.currentNode.compareDocumentPosition(stopNode || stop.container) & Node.DOCUMENT_POSITION_PRECEDING) break;
		const isRoot = elements.currentNode == root;
		const currentStyle = getComputedStyle(elements.currentNode);
		const forceDisplay = isRoot || elements.currentNode.hasAttribute('ps-float-base');
		addElementCounters(elements.currentNode, counters, null, forceDisplay, currentStyle);
		addElementCounters(elements.currentNode, counters, "before", forceDisplay);
		if ((!forceDisplay && currentStyle.display == 'none') || !elements.firstChild()) {
			while (!elements.nextSibling() && elements.currentNode != start) {
				addElementCounters(elements.currentNode, counters, "after", forceDisplay);
				if (!elements.currentNode.hasAttribute('ps-breaked-after')) {
					for (const counterName in counters) {
						const counter = counters[counterName];
						if (counter.length) {
							const counterScope = counter.at(-1);

							if ((counterScope.elemScope && counterScope.element === elements.currentNode)
								|| (counterScope.element.parentNode == elements.currentNode.parentNode)) {
								counter.pop();
							}
						}
					}
				}
				elements.parentNode();
			}
		}
	} while (elements.currentNode != start);
	return counters;
}

export function addElementCounters(element: Element, counters: CountersStack, pseudo: CountersPseudo, forceDisplay: boolean, style?: CSSStyleDeclaration): void {
	if (!style) style = getComputedStyle(element, pseudo ? '::' + pseudo : null);
	if (!forceDisplay && style.display == 'none') return;
	if (pseudo && style.content == 'none') return;

	const resets = parseCounterValue(style.counterReset);
	for (const counterName in resets) {
		let counter = counters[counterName];
		if (!counter) counter = counters[counterName] = [];
		// A scope on following sibling replace the current
		if (counter.length && counter.at(-1).element.parentNode == element.parentNode) counter.pop();
		counter.push({
			element: element,
			value: resets[counterName],
			elemScope: pseudo != null
		});
	}

	const increments = parseCounterValue(style.counterIncrement);
	for (const counterName in increments) {
		let counter = counters[counterName];
		if (!counter) counter = counters[counterName] = [];
		if (!counter.length) {
			counter.push({
				element: element,
				value: 0,
				elemScope: pseudo != null
			});
		}
		counter.at(-1).value += increments[counterName];
	}
}
