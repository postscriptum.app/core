import * as rects from "./rects.js";
import * as ranges from "./ranges.js";
import * as dom from "./dom.js";

export function drawClientRects(rects: ClientRect[], doc: Document = document): void {
	for (let i = 0; i != rects.length; i++) {
		const rect = rects[i];
		drawClientRect(rect, doc);
	}
}

export function drawClientRect(rect: ClientRect, doc: Document = document): void {
	const view = doc.defaultView;
	drawScrollRect({
		top: rect.top + view.scrollY,
		bottom: rect.bottom + view.scrollY,
		left: rect.left + view.scrollX,
		right: rect.right + view.scrollX
	}, doc);
}

export function drawScrollRect(rect: rects.Rect, doc: Document = document): void {
	const rectDiv = document.createElement('div');
	rectDiv.style.position = 'absolute';
	rectDiv.style.border = '1px solid red';
	rectDiv.style.margin = rectDiv.style.padding = '0';
	rectDiv.style.top = rect.top + 'px';
	rectDiv.style.left = rect.left + 'px';
	rectDiv.style.width = (rect.right - rect.left - 2) + 'px';
	rectDiv.style.height = (rect.bottom - rect.top - 2) + 'px';
	doc.documentElement.appendChild(rectDiv);
}

export function drawPoint(point: ranges.Point): void {
	const view = point.container.ownerDocument.defaultView;
	let y: number;
	if (dom.isElement(point.container)) {
		const node = ranges.nodeAtPoint(point);
		if (!node) y = rects.boundingScrollRect(point.container).bottom;
		else if (dom.isElement(node)) y = rects.boundingScrollRect(node).top;
		else {
			const range = document.createRange();
			range.selectNode(node);
			y = rects.scrollRect(range.getBoundingClientRect(), view).top;
		}
	} else {
		const range = document.createRange();
		range.setStartBefore(point.container);
		range.setEnd(point.container, point.offset);
		y = rects.scrollRect(range.getBoundingClientRect(), view).bottom;
	}
	const lineDiv = document.createElement('div');
	lineDiv.style.position = 'absolute';
	lineDiv.style.background = 'red';
	lineDiv.style.height = '1px';
	lineDiv.style.left = '0';
	lineDiv.style.width = '100%';
	lineDiv.style.top = y + 'px';
	document.documentElement.appendChild(lineDiv);
}

export function logCaretPositionOnClick(doc: Document): void {
	if (!doc) doc = document;

	doc.addEventListener('click', (event: MouseEvent) => {
		const point = doc.caretPositionFromPoint(event.clientX, event.y);
		console.log(point.offsetNode, point.offset);
	});

}