import * as str from "./str.js";

type Options = { [key: string]: any };

export function parseOptionsFromElement<O extends Options = Options>(element: Element, defaultOptions: O): O {
	const options: Options = {};
	if (element.hasAttribute('ps-options')) Object.assign(options, JSON.parse(element.getAttribute('ps-options')));
	for (const key in defaultOptions) {
		const type = Array.isArray(defaultOptions[key]) ? 'array' : typeof defaultOptions[key];
		const attr = element.getAttribute('ps-' + str.decamelize(key));
		if (attr !== null) {
			if (type == 'boolean') options[key] = attr == 'true';
			else if (type == 'array') options[key] = attr.length ? attr.trim().split(' ') : null;
			else if (type == 'string') options[key] = attr;
			else if (type == 'number') options[key] = parseFloat(attr);
			else if (type == 'object') options[key] = JSON.parse(attr);
		}
	}

	return options as O;
}

export function mergeOptions<O extends Options>(target: Partial<O>, ...optionsList: Partial<O>[]): O {
	const options = optionsList.shift();
	if (options) {
		for (const key of Object.keys(options)) {
			if (options[key] && typeof options[key] == 'object' && !Array.isArray(options[key])) {
				const targetProp = target[key];
				if (targetProp !== null && target.hasOwnProperty(key) && typeof targetProp == 'object' && !Array.isArray(targetProp)) mergeOptions(target[key], options[key]);
				else target[key as keyof O] = Object.assign({}, options[key]);
			} else {
				target[key as keyof O] = options[key];
			}
		}
	}

	return optionsList.length ? mergeOptions(target, ...optionsList) as O : target as O;
}


// The fetch API is not used because it does not support the file protocol
// TODO Error status code ?
export function load(url: string): Promise<string> {
	return new Promise((resolve, reject) => {
		const request = new XMLHttpRequest();
		request.open('GET', url);
		request.onload = () => resolve(request.responseText);
		request.onerror = () => reject(new Error("Unable to fetch '" + url + "'"));
		request.send();
	});
}

// Usefull for plugins
export function loadScript(url: string): Promise<void> {
	return new Promise((resolve, reject) => {
		const script = document.createElement('script');
		script.src = url;
		script.addEventListener('error', () => {
			reject(new Error(`Unable to load the script '${url}'.`));
		});
		script.addEventListener('load', resolve as any as EventListenerOrEventListenerObject);
		document.head.appendChild(script);
	});
}