import * as ranges from "./ranges.js";

export interface Rect {
	bottom: number;
	left: number;
	right: number;
	top: number;
}

export interface DimRect extends Rect {
	width: number;
	height: number;
}


export function boundingScrollRect(object: Element | Range): DimRect {
	if (ranges.isRange(object)) return scrollRect(object.getBoundingClientRect(), object.commonAncestorContainer.ownerDocument.defaultView);
	else return scrollRect(object.getBoundingClientRect(), object.ownerDocument.defaultView);
}

export function scrollRect(clientRect: Rect, view: Window): DimRect {
	return {
		top: clientRect.top + view.scrollY,
		bottom: clientRect.bottom + view.scrollY,
		left: clientRect.left + view.scrollX,
		right: clientRect.right + view.scrollX,
		width: clientRect.right - clientRect.left,
		height: clientRect.bottom - clientRect.top
	};
}

export function rectsLines(rects: DOMRectList | Rect[], view: Window): Rect[] {
	const dedupRects: Rect[] = [];
	for (const rect of rects) {
		if (rect.left != rect.right && !dedupRects.find((r) => equalRects(r, rect))) dedupRects.push(rect);
	}
	const lines = [];
	let lastLine = null;
	// TODO Should we parse the line from right to left when the direction is rtl. Implies to reverse the rects order and to start from the end.
	for (const rect of dedupRects) {
		if (!lastLine || (rect.bottom > lastLine.bottom && rect.top > lastLine.top && rect.left <= lastLine.left)) {
			lastLine = {
				left: rect.left,
				bottom: rect.bottom,
				right: rect.right,
				top: rect.top
			};
			lines.push(lastLine);
		} else if (rect.bottom - lastLine.top > 2) {
			// In some special cases (inline with a pseudo-element, some rects are not correctly ordered. The previous condition avoids them.
			if (rect.top < lastLine.top) lastLine.top = rect.top;
			if (rect.bottom > lastLine.bottom) lastLine.bottom = rect.bottom;
			if (rect.right > lastLine.right) lastLine.right = rect.right;
		}
	}

	for (const line of lines) {
		line.top += view.scrollY;
		line.bottom += view.scrollY;
		line.left += view.scrollX;
		line.right += view.scrollX;
	}

	return lines;
}

export function equalRects(rect1: Rect, rect2: Rect): boolean {
	return rect1.top == rect2.top && rect1.right == rect2.right && rect1.bottom == rect2.bottom && rect1.left == rect2.left;
}

export function includesRect(parentRect: Rect, rect: Rect): boolean {
	return rect.top >= parentRect.top && rect.bottom <= parentRect.bottom && rect.left >= parentRect.left && rect.right <= parentRect.right;
}

export function contentSizingHeight(style: CSSStyleDeclaration, baseHeight: number): number {
	return baseHeight - parseFloat(style.paddingTop)
		- parseFloat(style.paddingBottom)
		- parseFloat(style.borderTopWidth)
		- parseFloat(style.borderBottomWidth);
}
