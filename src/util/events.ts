export type Listener<K extends keyof Events, Events> = ((event: Event<K, Events>, target: EventEmitter<Events>) => any) & {[inheritedSym]?: boolean};

export type Listeners<Events> = { [K in keyof Events]: Listener<K, Events>[] };

export interface ListenerOption {
	inherited?: boolean;
}

export type Event<K extends keyof Events, Events> = { type: K } & Events[K];

const inheritedSym = Symbol("inherited");

export class EventEmitter<Events = Record<string, unknown>> {
	listeners: Listeners<Events>;
	onceMapping: Map<Listener<any, any>, Listener<any, any>>;
	parentEmitter?: EventEmitter<Partial<Events>>;
	on = this.addListener;
	off = this.removeListener;

	addListener<K extends keyof Events>(eventType: K, listener: Listener<K, Events>, options?: ListenerOption): this {
		if (!this.listeners) this.listeners = Object.create(null);
		let lstns = this.listeners[eventType];
		if (!lstns) {
			lstns = [];
			this.listeners[eventType] = lstns;
		}
		if (options?.inherited) listener[inheritedSym] = true;
		lstns.push(listener);
		return this;
	}

	removeListener<K extends keyof Events>(eventType: K, listener: Listener<K, Events>): this {
		const lstns = this.listeners && this.listeners[eventType];
		if (lstns) {
			const lstn = this.onceMapping?.get(listener) || listener;
			lstns.splice(lstns.indexOf(lstn), 1);
		}
		return this;
	}

	listenerCount(event: keyof Events): number {
		let count = 0;
		let emitter: EventEmitter<Partial<Events>> = this;
		while (emitter) {
			const lstns = emitter.listeners && emitter.listeners[event];
			if (lstns) count += lstns.length;
			emitter = emitter.parentEmitter;
		}
		return count;
	}

	emit<K extends keyof Events>(event: string | Event<K, Events> | K): any {
		const eventObj = (typeof event == 'string' ? {type: event} : event) as Event<K, Events>;
		let emitter: EventEmitter<Partial<Events>> = this;
		while (emitter) {
			const lstns = emitter.listeners && emitter.listeners[eventObj.type];
			const result = this._emit(eventObj, lstns, emitter != this);
			if (result !== undefined) return result;
			emitter = emitter.parentEmitter;
		}
	}

	protected _emit<K extends keyof Events>(event: Event<K, Events> | K, lstns: Listeners<Events>[K], onlyInherited?: boolean): any {
		if (lstns?.length) for (const listener of lstns) {
			if (!onlyInherited || listener[inheritedSym]) {
				this.onceMapping?.delete(listener);
				const result = listener.call(this, event);
				if (result !== undefined) return result;
			}
		}
	}

	once<K extends keyof Events>(eventType: K, listener: Listener<K, Events>, options?: ListenerOption): this {
		const lstn: Listener<K, Events> = ((event: Event<K, Events>, target: EventEmitter<Events>): void => {
			listener(event, target);
			this.removeListener(eventType, lstn);
		});
		if (options?.inherited) lstn[inheritedSym] = true;
		if (!this.onceMapping) this.onceMapping = new Map();
		this.onceMapping.set(listener, lstn);
		this.addListener(eventType, lstn);
		return this;
	}
}