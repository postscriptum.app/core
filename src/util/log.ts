export type LogStringLevel = "error" | "warn" | "info" | "debug" | "trace";

export enum LogLevel { error = 0, warn = 1, info = 2, debug = 3 }

export interface Log {
	message: string | Error;
	level: LogLevel;
	scopes?: string[];
}

export interface LogReporter {
	log(log: Log): void;
}

type LazyString = (() => string) | string;

export class Logger {
	protected _reporter: LogReporter;
	protected _scopes: LazyString[] = [];
	protected _level: LogLevel = LogLevel.warn;

	get level(): LogLevel | LogStringLevel {
		return this._level;
	}

	set level(level: LogLevel | LogStringLevel) {
		if (typeof level == 'string') this._level = Logger.levelFromString(level);
		else this._level = level;
	}

	debug(message: LazyString, scope?: LazyString): void {
		this.log(LogLevel.debug, message, scope);
	}

	info(message: LazyString, scope?: LazyString): void {
		this.log(LogLevel.info, message, scope);
	}

	warn(message: LazyString, scope?: LazyString): void {
		this.log(LogLevel.warn, message, scope);
	}

	error(message: LazyString, scope?: LazyString): void {
		this.log(LogLevel.error, message, scope);
	}

	static levelFromString(level: LogStringLevel): number {
		if (level == "error") return 0;
		if (level == "warn") return 1;
		else if (level == "info") return 2;
		else if (level == "debug") return 3;
	}

	setReporter(reporter: LogReporter): void {
		this._reporter = reporter;
	}

	addScope(scope: LazyString): void {
		this._scopes.push(scope);
	}

	removeScope(count = 1): void {
		for (let i = 0; i < count; i++) this._scopes.pop();
	}

	log(level: LogLevel, message: LazyString | Error, scope?: LazyString): void {
		if (level > this._level || !this._reporter) return;
		const log: Log = {level, message: typeof message == 'function' ? message() : message};
		if (scope) this._scopes.push(scope);
		if (this._scopes.length) log.scopes = this._scopes.map((scope) => typeof scope == 'string' ? scope : scope());
		this._reporter.log(log);
		if (scope) this._scopes.pop();
	}
}

export const defaultReporter = {
	log: function (log: Log): void {
		let message = log.message; //`[${Math.floor(performance.now()) / 1000}] ${log.message}`;
		if (log.scopes) message += ` (${log.scopes.join(', ')})`;
		if (log.level == LogLevel.error) console.error(message);
		else if (log.level == LogLevel.warn) console.warn(message);
		else if (log.level == LogLevel.info) console.info(message);
		else if (log.level == LogLevel.debug) console.debug(message);
	}
};
