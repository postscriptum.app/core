export interface Point {
	container: Node;
	offset: number;
}

export function isRange(object: any): object is Range {
	return object && 'isPointInRange' in object;
}

export function isPoint(object: any): object is Point {
	return object && 'container' in object && 'offset' in object;
}

export function clonePoint(point: Point): Point {
	return {
		container: point.container,
		offset: point.offset
	};
}

export function pointEquals(point1: Point, point2: Point): boolean {
	return point1.container == point2.container && point1.offset == point2.offset;
}

export function breakRangeBefore(range: Range, node: Node): Range {
	const breaked = node.ownerDocument.createRange();
	breaked.setStart(range.startContainer, range.startOffset);
	breaked.setEndBefore(node);
	range.setStartBefore(node);
	return breaked;
}

export function breakRangeAfter(range: Range, node: Node): Range {
	const breaked = node.ownerDocument.createRange();
	breaked.setStart(range.startContainer, range.startOffset);
	breaked.setEndAfter(node);
	range.setStartAfter(node);
	return breaked;
}

export function positionBefore(node: Node): Point {
	const range = node.ownerDocument.createRange();
	range.setStartBefore(node);
	return startPoint(range);
}

export function positionAfter(node: Node): Point {
	const range = node.ownerDocument.createRange();
	range.setEndAfter(node);
	return endPoint(range);
}

export function positionAtStart(node: Node): Point {
	return {container: node, offset: 0};
}

export function positionAtEnd(node: Node): Point {
	return {container: node, offset: node.childNodes.length};
}

export function startPoint(range: Range): Point {
	return {
		container: range.startContainer as Element,
		offset: range.startOffset
	};
}

export function endPoint(range: Range): Point {
	return {
		container: range.endContainer as Element,
		offset: range.endOffset
	};
}

export function startNode(range: Range): Node {
	return range.startContainer.nodeType == Node.ELEMENT_NODE ? range.startContainer.childNodes[range.startOffset] : null;
}

export function endNode(range: Range): Node {
	return range.endContainer.nodeType == Node.ELEMENT_NODE ? range.endContainer.childNodes[range.endOffset - 1] : null;
}

export function nodeAtPoint(point: Point): Node {
	return point.container.nodeType == Node.TEXT_NODE ? point.container : point.container.childNodes.item(point.offset);
}

// Difference with Range.isPointInRange: the endPoint is exclusive
export function containsPoint(range: Range, point: Point): boolean {
	if (range.endContainer == point.container && range.endOffset == point.offset) return false;
	else return range.isPointInRange(point.container, point.offset);
}

export function isPrecedingPoint(refPoint: Point, testPoint: Point): boolean {
	if (testPoint.container == refPoint.container) {
		return testPoint.offset < refPoint.offset;
	} else {
		const refRange = document.createRange();
		refRange.setStart(refPoint.container, refPoint.offset);
		return refRange.comparePoint(testPoint.container, testPoint.offset) == -1;
	}
}

export function textPositionFromPoint(doc: Document, x: number, y: number): Point {
	doc.defaultView.scrollTo(x, y);

	const pos = doc.caretPositionFromPoint(x - doc.defaultView.scrollX, y - doc.defaultView.scrollY) as CaretPosition & Point;
	if (!pos) return null;
	pos.container = pos.offsetNode;
	return pos;
}


export function findPointInRanges(rangesList: Range[], point: Point, reverse = false): number {
	let range;
	if (reverse) {
		for (let rangeIndex = rangesList.length - 1; rangeIndex >= 0; rangeIndex--) {
			range = rangesList[rangeIndex];
			if (containsPoint(range, point)) return rangeIndex;
		}
	} else {
		for (let rangeIndex = 0; rangeIndex < rangesList.length; rangeIndex++) {
			range = rangesList[rangeIndex];
			if (containsPoint(range, point)) return rangeIndex;
		}
	}

	return -1;
}

export function appendNodeContents(srcNode: Node, dstNode: Node): void {
	const range = srcNode.ownerDocument.createRange();
	range.selectNodeContents(srcNode);
	const fragment = range.extractContents();
	dstNode.appendChild(fragment);
}
