let idCounter = 0;

export function id(element?: Element): string {
	if (!element) return 'ps-' + idCounter++;
	return element.id || (element.id = 'ps-' + idCounter++);
}

export function docInteractive(doc: Document = document): Promise<void> {
	return new Promise<void>((resolve) => {
		if (doc.readyState != 'loading') resolve();
		else doc.addEventListener('DOMContentLoaded', () => resolve());
	});
}

export function docComplete(doc: Document = document): Promise<void> {
	return new Promise<void>((resolve, reject) => {
		if (doc.readyState == 'complete') resolve();
		else {
			doc.defaultView.addEventListener('load', () => resolve());
			doc.defaultView.addEventListener('error', reject);
		}
	});
}

export function isElement(object: any): object is Element {
	return object && object.nodeType == Node.ELEMENT_NODE;
}