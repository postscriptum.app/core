import {logger} from "../util.js";

export type NodePredicate<T extends Node> = (node: Node) => node is T;

export interface TypedTreeWalker<T> {
	currentNode: T;
	readonly filter: NodeFilter | null;
	readonly root: T;
	readonly whatToShow: number;

	firstChild(): T | null;

	lastChild(): T | null;

	nextNode(): T | null;

	nextSibling(): T | null;

	parentNode(): T | null;

	previousNode(): T | null;

	previousSibling(): T | null;
}

export interface TypedNodeIterator<T> {
	readonly filter: NodeFilter | null;

	readonly pointerBeforeReferenceNode: boolean;
	readonly referenceNode: Node;
	readonly root: Node;
	readonly whatToShow: number;

	detach(): void;

	nextNode(): T | null;

	previousNode(): T | null;
}

type Constructor<T> = new(...args: any[]) => T;

export function createWalker<T extends Node>(root: Node, accept?: NodePredicate<T>, skip?: NodePredicate<T>): TypedTreeWalker<T> {
	return root.ownerDocument.createTreeWalker(root, NodeFilter.SHOW_ALL, {
		acceptNode(node: Node): number {
			if (!accept || accept(node)) return NodeFilter.FILTER_ACCEPT;
			else if (skip && skip(node)) return NodeFilter.FILTER_SKIP;
			return NodeFilter.FILTER_REJECT;
		}
	}) as TypedTreeWalker<T>;
}

export function createIterator<T extends Node>(root: Node, accept?: NodePredicate<T>, skip?: NodePredicate<T>): TypedNodeIterator<T> {
	return root.ownerDocument.createNodeIterator(root, NodeFilter.SHOW_ALL, {
		acceptNode(node: Node): number {
			if (!accept || accept(node)) return NodeFilter.FILTER_ACCEPT;
			else if (skip && skip(node)) return NodeFilter.FILTER_SKIP;
			return NodeFilter.FILTER_REJECT;
		}
	}) as TypedNodeIterator<T>;
}

export function firstChild<T extends Node>(from: Node, predicate: NodePredicate<T>): T {
	let node = from ? from.firstChild as Node : null;
	while (node) {
		if (predicate(node)) return node;
		node = node.nextSibling;
	}
	return null;
}

export function lastChild<T extends Node>(from: Node, predicate: NodePredicate<T>): T {
	let node = from ? from.lastChild as Node : null;
	while (node) {
		if (predicate(node)) return node;
		node = node.previousSibling;
	}
	return null;
}

export function nextSibling<T extends Node>(from: Node, predicate: NodePredicate<T>): T {
	let node = from ? from.nextSibling : null;
	while (node) {
		if (predicate(node)) return node;
		node = node.nextSibling;
	}
	return null;
}

export function previousSibling<T extends Node>(from: Node, predicate: NodePredicate<T>): T {
	let node = from ? from.previousSibling : null;
	while (node) {
		if (predicate(node)) return node;
		node = node.previousSibling;
	}
	return null;
}

export function ancestor<T extends Node>(from: Node, predicate: NodePredicate<T>, root?: Node): T {
	let node = from.parentNode;
	while (node && node !== root) {
		if (predicate(node)) return node;
		node = node.parentNode;
	}

	return null;
}

export function ancestorOrSelf<T extends Node>(from: Node, predicate: NodePredicate<T>, root?: Node): T {
	let node = from;
	while (node && node !== root) {
		if (predicate(node)) return node;
		node = node.parentNode;
	}

	return null;
}

export function next<T extends Node>(from: Node, predicate: NodePredicate<T>, root?: Node): T {
	let node = from.firstChild as Node;
	if (node) return predicate(node) ? node : next(node, predicate, root);
	if (from === root) return null;
	while (!(node = from.nextSibling)) {
		from = from.parentNode;
		if (from === root) return null;
	}

	return predicate(node) ? node : next(node, predicate, root);
}

export function previous<T extends Node>(from: Node, predicate: NodePredicate<T>, root?: Node): T {
	if (from === root) return null;
	let node = from.previousSibling as Node;
	if (node) {
		let child = node.lastChild;
		while (child) {
			node = child;
			child = child.lastChild;
		}
		return predicate(node) ? node : previous(node, predicate, root);
	}
	node = from.parentNode;

	return node === root ? null : predicate(node) ? node : previous(node, predicate, root);
}


export function all<T extends Node>(axis: (from: Node, predicate: NodePredicate<T>, root?: Node) => T, from: Node, predicate: NodePredicate<T>, root?: Node): T[] {
	const nodes: T[] = [];
	let node = from;
	while ((node = axis(node, predicate, root))) nodes.push(node as T);
	return nodes;
}

export function isNode(node: Node): node is Node {
	return true;
}

export function isElement(node: Node): node is Element {
	return node && node.nodeType == Node.ELEMENT_NODE;
}

export function isHTMLElement(node: Node): node is HTMLElement {
	return isElement(node) && 'innerText' in node;
}

export function isSVGElement(node: Node): node is SVGElement {
	return isElement(node) && 'ownerSVGElement' in node;
}

export function isSVGRootSVGElement(node: Node): node is SVGSVGElement {
	return isSVGElement(node) && node.ownerSVGElement == null;
}

export function isHTMLOrSVGRootSVGElement(node: Node): node is HTMLElement | SVGSVGElement {
	return isHTMLElement(node) || isSVGRootSVGElement(node);
}

export function isText(node: Node): node is Text {
	return node && node.nodeType == Node.TEXT_NODE;
}

export function isStaticText(node: Node): node is Element {
	return isText(node) && node.nodeValue.trim().length != 0;
}


export function isConstructedBy<T extends Node>(constructor: Constructor<T>): NodePredicate<T> {
	logger.warn("The function 'isConstructedBy' is deprecated. Please replace it by the function 'isHTMLTag'.");
	return function (node: Node): node is T {
		return node.constructor == constructor;
	};
}

export function isHTMLTag<K extends keyof HTMLElementTagNameMap>(tagName: K): NodePredicate<HTMLElementTagNameMap[K]> {
	return function (node: Node): node is HTMLElementTagNameMap[K] {
		return isHTMLElement(node) && node.localName == tagName;
	};
}

export const isHTMLTable = isHTMLTag('table');

/*
 * Fonction de parcours de l'arbre static
 */
export function isStaticStyle(style: CSSStyleDeclaration): boolean {
	return style.display != 'none' && style.display != 'table-column-group' && (style.position == 'static' || style.position == 'relative');
}

export function isStatic(node: Node): boolean {
	if (isStaticText(node)) return true;
	else if (isElement(node)) return isStaticStyle(getComputedStyle(node));
	return false;
}

export function hasStaticContent(node: Node): boolean {
	return isStatic(node) && (!isHTMLElement(node) || node.innerText.trim().length != 0);
}

export function isRow(style: CSSStyleDeclaration): 'table-row' | 'flex-row' | false {
	if (style.display == 'table-row') return 'table-row';
	else if (style.display == 'flex' && style.flexDirection.startsWith('row')) return 'flex-row';
	return false;
}

export function predicate<T extends Node>(typeGuard: NodePredicate<T>, ...predicates: ((node: Node) => boolean)[]): NodePredicate<T> {
	return ((node: Node) => {
		if (typeGuard && !typeGuard(node)) return false;
		return predicates.every((predicate) => predicate(node));
	}) as NodePredicate<T>;
}

export function displayAsBlockStyle(style: CSSStyleDeclaration): boolean {
	return style.float != 'none' || !(style.display.startsWith('inline') || style.display.startsWith('ruby') || style.display.startsWith('table-column'));
}

export function displayAsBlock(node: Node): boolean {
	if (isText(node)) return false;
	else if (isElement(node)) return displayAsBlockStyle(getComputedStyle(node));
}

export function displayAsInlineBlock(node: Node): boolean {
	if (isText(node)) return false;
	else if (isElement(node)) {
		const style = getComputedStyle(node);
		return style.display.startsWith("inline-") || (style.display == "inline" && isSVGElement(node));
	}
}

export function isDisplayed(element: HTMLElement): boolean {
	const style = getComputedStyle(element as HTMLElement);
	return style.display != 'none';
}

export function hasSize(element: HTMLElement): boolean {
	return element.clientWidth != 0 || element.clientHeight != 0;
}

export function isBreakedAfter(elem: Element): boolean {
	return elem.hasAttribute('ps-breaked-after');
}

export const isStaticNode = isStatic as NodePredicate<Node>;
export const isStaticElement = predicate(isElement, isStatic);
export const nodeHasStaticContent = hasStaticContent as NodePredicate<Node>;
export const isStaticHtmlElement = predicate(isHTMLElement, isStatic);
export const isStaticHtmlBlock = predicate(isHTMLElement, isStatic, displayAsBlock);
export const isBlockElement = predicate(isElement, displayAsBlock);
export const isInlineBlockElement = predicate(isElement, displayAsInlineBlock);
