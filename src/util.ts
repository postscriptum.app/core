import * as log from "./util/log.js";

export const logger = new log.Logger();
logger.setReporter(log.defaultReporter);

export * as debug from "./util/debug.js";
export * as dom from "./util/dom.js";
export * as events from "./util/events.js";
export * as io from "./util/io.js";
export * as log from "./util/log.js";
export * as ranges from "./util/ranges.js";
export * as rects from "./util/rects.js";
export * as str from "./util/str.js";
export * as walk from "./util/walk.js";

