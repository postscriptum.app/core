import {ranges, walk} from "./util.js";
import {getCustomProp, getCustomStyle} from "./css.js";

export type ForcedBreakType = 'always' | 'page' | 'column' | 'left' | 'right' | 'recto' | 'verso' | '-ps-column-span';

export interface Flow {
	range: Range;
	pageName: string;
	pageGroupStart: boolean;
	breakType: ForcedBreakType;
	maxTextLength: boolean;
}

/** Iterate over block elements except inner SVG elements */
export const flowPredicate = walk.predicate(walk.isHTMLOrSVGRootSVGElement, walk.isStatic, walk.displayAsBlock);

/**
 * Iterator of continuous flow
 *
 * A continuous flow is a range of contents not interrupted by forced page breaks.
 */
export class FlowIterator implements Iterable<Flow> {
	elements: walk.TypedTreeWalker<Element>;

	rootPageName: string;

	remaining: Range;

	forcedBreakTypes: ForcedBreakType[];

	nextBreakType: ForcedBreakType;

	nextPageGroupStart: boolean;

	maxTextLength: number;

	resetOnNext = false;

	columnSpanBreak: 'box' | 'element' = 'element';

	pageNames: string[];

	/**
	 * ContinuousFlowIterator constructor
	 * @param root              The root element to parse
	 * @param forcedBreakTypes  List of values to use as forced break
	 * @param maxTextLength
	 * @param firstBreakType    Type of the break, usefull on repagination
	 */
	constructor(root: Element, forcedBreakTypes: ForcedBreakType[], maxTextLength = 0, firstBreakType?: ForcedBreakType) {
		this.elements = walk.createWalker(root, flowPredicate);

		this.remaining = root.ownerDocument.createRange();
		this.forcedBreakTypes = forcedBreakTypes;
		this.nextBreakType = firstBreakType;
		this.nextPageGroupStart = true;
		this.maxTextLength = maxTextLength;

		let parent = root;
		this.rootPageName = 'auto';
		while (parent && this.rootPageName == 'auto') {
			this.rootPageName = getCustomProp(parent, 'page');
			parent = parent.parentElement;
		}
		if (this.rootPageName == 'auto') this.rootPageName = '';
		this.reset();
	}


	// Used by for..of
	[Symbol.iterator](): Iterator<Flow> {
		const self = this;
		return {
			next: function (): IteratorResult<Flow> {
				const flow = self.next();
				return {done: !flow, value: flow};
			}
		};
	}

	reset(): void {
		this.elements.currentNode = this.elements.root;
		this.elements.nextNode();
		this.remaining.selectNodeContents(this.elements.root);
		this.pageNames = [this.rootPageName];
	}


	/**
	 * Iterate to the next page break start
	 */
	next(): Flow {
		if (this.resetOnNext) {
			this.reset();
			this.resetOnNext = false;
		}
		let textLength = 0;
		if (this.remaining.collapsed) return null;

		let forcedBreakAfter: ForcedBreakType = null;

		const flow: Flow = {
			range: null,
			pageName: null,
			pageGroupStart: this.nextPageGroupStart,
			breakType: this.nextBreakType,
			maxTextLength: false
		};
		this.nextBreakType = null;
		this.nextPageGroupStart = false;

		const startNode = this.elements.currentNode;
		let boxRoot = startNode;
		let inTable = false;

		while (!flow.range) {
			const currentElement = this.elements.currentNode as HTMLElement;
			const customStyle = getCustomStyle(currentElement);
			const stylePageName = getCustomProp(customStyle, 'page');
			let elementPageName = stylePageName;
			const previousPageName = this.pageNames.at(-1);
			if (elementPageName == 'auto') elementPageName = previousPageName;
			const newPageName = elementPageName != flow.pageName;
			this.pageNames.push(elementPageName);

			if (walk.isHTMLTable(currentElement)) inTable = true;

			const columnSpan = getCustomProp(customStyle, 'column-span') == 'all';
			const breakBefore = columnSpan ? '-ps-column-span' : getCustomProp(currentElement, 'break-before') as ForcedBreakType;

			const staticBefore = walk.previousSibling(currentElement, walk.nodeHasStaticContent);
			if (currentElement == startNode || (boxRoot == startNode && !staticBefore)) {
				// First static descendants
				flow.pageName = elementPageName;
				if (!flow.pageGroupStart && newPageName && stylePageName != "auto") flow.pageGroupStart = true;
				if (breakBefore && this.forcedBreakTypes.includes(breakBefore)) {
					flow.breakType = breakBefore;
					if (staticBefore) {
						// If there is static contents before the forced break, a new flow is emitted
						flow.range = ranges.breakRangeBefore(this.remaining, currentElement);
						this.pageNames.pop();
						flow.pageName = previousPageName;
						break;
					}
				}
				if (!flow.pageGroupStart && newPageName && stylePageName != "auto") flow.pageGroupStart = true;
			} else {
				const forcedBreakBefore = this.forcedBreakTypes.includes(breakBefore) && breakBefore;

				const breakOnElement = columnSpan && this.columnSpanBreak == 'element';
				if (forcedBreakBefore || forcedBreakAfter || newPageName) {
					this.nextBreakType = forcedBreakBefore || forcedBreakAfter;
					this.nextPageGroupStart = newPageName || ((forcedBreakBefore || forcedBreakAfter) && stylePageName != "auto");
					flow.range = ranges.breakRangeBefore(this.remaining, breakOnElement ? currentElement : boxRoot);
					forcedBreakAfter = null;
					this.pageNames.pop();
					break;
				}
			}

			if (!this.elements.firstChild()) {
				this.pageNames.pop();

				if (this.maxTextLength && walk.isHTMLElement(currentElement) && currentElement.localName != 'script') {
					textLength += currentElement.innerText.length;

					// Avoid a cut inside a table to preserve his integrity
					if (textLength > this.maxTextLength && !inTable) {
						flow.range = ranges.breakRangeAfter(this.remaining, this.elements.currentNode);
						flow.maxTextLength = true;
						this.resetOnNext = true;
					}
				}

				if (!flow.range) {
					const breakAfter = this._checkBreakAfter(currentElement, flow, customStyle);
					if (!flow.range && breakAfter) forcedBreakAfter = breakAfter;
				}

				if (!flow.range) while (!this.elements.nextSibling()) {
					this.pageNames.pop();

					if (walk.isHTMLTable(this.elements.currentNode.parentNode)) inTable = false;
					// parentNode can be null if the root has display: none
					if (!this.elements.parentNode() || this.elements.currentNode == this.elements.root) {
						flow.range = ranges.breakRangeAfter(this.remaining, this.elements.root.lastChild);
						break;
					} else if (!forcedBreakAfter) {
						// Checks the break-after on ancestors
						const breakAfter = this._checkBreakAfter(this.elements.currentNode as HTMLElement, flow);
						if (flow.range) break;
						else if (breakAfter) forcedBreakAfter = breakAfter;
					}
				}

				boxRoot = this.elements.currentNode;
			}
		}

		return flow;
	}

	_checkBreakAfter(currentElement: HTMLElement, flow: Flow, customStyle?: CSSStyleDeclaration): ForcedBreakType | null {
		if (!customStyle) customStyle = getCustomStyle(currentElement);
		const columnSpan = getCustomProp(customStyle, 'column-span') == 'all';
		const breakAfter = columnSpan ? 'column' : getCustomProp(customStyle, 'break-after') as ForcedBreakType;
		if (this.forcedBreakTypes.includes(breakAfter) || (this.forcedBreakTypes.includes('column') && columnSpan)) {
			// Special case where a forced break-after is followed by an inline
			const nextStatic = walk.nextSibling(currentElement, walk.isStaticNode);
			if (nextStatic && !walk.displayAsBlock(nextStatic)) {
				flow.range = ranges.breakRangeAfter(this.remaining, currentElement);
				this.resetOnNext = true;
				return null;
			}
			return breakAfter;
		}
		return null;
	}
}
