/**
 * The entry point of postscriptum.
 */
import * as breaks from "./breaks.js";
import * as columns from "./columns.js";
import {Columnizator} from "./columns.js";
import * as counters from './counters.js';
import * as css from './css.js';
import * as flow from './flow.js';
import * as fragments from './fragments.js';
import * as gcpm from './gcpm.js';
import * as layout from './layout.js';
import * as mutations from "./mutations.js";
import * as pages from './pages.js';
import {Paginator} from './pages.js';
import * as precss from "./precss.js";
import * as process from "./process.js";
import * as rows from './rows.js';
import * as shim from "./shim.js";
import * as util from "./util.js";
import {dom, io, logger} from "./util.js";
import type {PaginatorOptions} from './pages.js';
import type {ColumnizatorOptions} from "./columns.js";

const currentScript = document.currentScript as HTMLScriptElement;
const PAGINATE_ATTR = 'ps-paginate';
const params = new URLSearchParams(location.search);

export type Postscriptum = {
	pagination(options?: Partial<PaginatorOptions>): Paginator;
	columnization(element: HTMLElement, options?: Partial<ColumnizatorOptions>): Columnizator;
	plugin: <O = Record<string, unknown>>(name: string, initializer?: process.Plugin<O>, defaultOptions?: O, script?: HTMLScriptElement) => process.Plugin<O>;
	getOptions(): Partial<PaginatorOptions>;
	breaks: typeof breaks;
	columns: typeof columns;
	counters: typeof counters;
	css: typeof css;
	gcpm: typeof gcpm;
	flow: typeof flow;
	fragments: typeof fragments;
	layout: typeof layout;
	mutations: typeof mutations;
	pages: typeof pages;
	precss: typeof precss;
	process: typeof process;
	rows: typeof rows;
	shim: typeof shim;
	util: typeof util;
};

/**
 * Global postscriptum object.
 */
const postscriptum: Postscriptum = {
	pagination: function (options?: Partial<PaginatorOptions>): Paginator {
		if (params.has('dev')) debugger;
		const processor = new Paginator(options);
		logger.debug(`Paginator default options: ${JSON.stringify(Paginator.defaultOptions)}`);
		logger.info(`Paginator created with options ${JSON.stringify(processor.options)}`);
		return processor;
	},

	columnization: function (element: HTMLElement, options?: ColumnizatorOptions): Columnizator {
		if (params.has('dev')) debugger;
		const processor = new Columnizator(element, options);
		return processor;
	},

	plugin: function <O = Record<string, unknown>> (name: string, initializer?: process.Plugin<O>, defaultOptions: O = null, script: HTMLScriptElement = document.currentScript as HTMLScriptElement): process.Plugin<O> {
		if (!initializer) {
			if (name in process.pluginRegistry) return process.pluginRegistry[name].initializer;
			return null;
		}
		process.pluginRegistry[name] = {
			initializer,
			defaultOptions,
			scriptOptions: script && io.parseOptionsFromElement(script, defaultOptions)
		};
	},

	getOptions: function (): PaginatorOptions {
		const optionsElement = document.getElementById('ps-options');
		let options: PaginatorOptions = null;
		if (optionsElement) {
			options = JSON.parse(optionsElement.textContent);
		} else {
			let configElement;
			if (currentScript && currentScript.hasAttribute(PAGINATE_ATTR)) configElement = currentScript;
			else if (document.documentElement.hasAttribute(PAGINATE_ATTR)) configElement = document.documentElement;
			if (configElement) options = io.parseOptionsFromElement(configElement, Paginator.defaultOptions);
		}
		if (options && Array.isArray(options.plugins)) options.plugins = Object.fromEntries(options.plugins.map((plugin) => Array.isArray(plugin) ? plugin : [ plugin, null ]));
		return options;
	},

	breaks,
	columns,
	counters,
	css,
	gcpm,
	flow,
	fragments,
	layout,
	mutations,
	pages,
	precss,
	process,
	rows,
	shim,
	util
} as Postscriptum;

export default postscriptum;

/**
 * Starts the pagination process if options are defined in the HTML.
 */
(async () => {
	await dom.docInteractive();
	if (!('postscriptumPreload' in window)) {
		const options = postscriptum.getOptions();
		if (options) {
			const process = postscriptum.pagination(options);
			try {
				await process.start().ended;
				return options;
			} catch (e) {
				try {
					postscriptum.util.logger.error(e);
				} catch (e) {
					console.error(e);
				}
			}
		}
	}
})();

declare global {
	interface Window {
		postscriptum: Postscriptum;
		postscriptumPreload?: any;
		postscriptumBridge: {
			loadPlugin?: (name: string) => Promise<void>;
			loadHyph?: (lang: string) => Promise<string>;
			getAccessibleString?: (elemId: string) => Promise<string>;
			getAccessibleStrings?: (elemIds: string[]) => Promise<string|null>;
		};
	}

	let postscriptum: Postscriptum;
}

