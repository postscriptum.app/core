import {io, rects, walk} from "./util.js";
import {addStyleImagesToLoad, getCustomProp} from "./css.js";
import {LayoutProcessor} from "./layout.js";
import Mutations from "./mutations.js";
import type {CssContextOptions} from "./css.js";
import type {Areas, LayoutProcessorEvents, LayoutProcessorOptions} from "./layout.js";

export class Fragment {
	container: HTMLElement;
	body: HTMLElement;
	area: HTMLElement;
	areas: Areas;
	number: number;
}

export interface FragmentorOptions extends LayoutProcessorOptions, CssContextOptions {
	dest?: HTMLElement;
	maxFragmentTextLength: number;
	mutationsCommitLevel: 'fragment' | 'processor';
}

export interface FragmentorEvents extends LayoutProcessorEvents {
	'fragment-start': { fragment: Fragment };
	'fragment-end': { fragment: Fragment };
}

const currentScript = document.currentScript as HTMLScriptElement;

export abstract class Fragmentor<Options extends FragmentorOptions = FragmentorOptions, Events extends FragmentorEvents = FragmentorEvents> extends LayoutProcessor<Options, Events> {
	static defaultOptions: FragmentorOptions = io.mergeOptions({}, LayoutProcessor.defaultOptions as FragmentorOptions, {
		dest: null,
		coreCSS: true,
		defaultCSS: true,
		preprocessCSS: true,
		defaultPageSize: 'A4',
		mediaType: 'print',
		hyphUri: currentScript && !currentScript.src.startsWith('blob:') && new URL('hyph', currentScript.src).toString(),
		/* slightly faster than 5K or 20K */
		maxFragmentTextLength: 10000
	});
	dest: HTMLElement;
	fragments: Fragment[] = [];
	currentFragment: Fragment = null;
	currentFragMut: Mutations = new Mutations();

	imagesToLoad = new Set<string>();

	// TODO Better way to handle this flag
	logNoValidBreakPoint = true;

	protected constructor(source: string | HTMLElement, options?: Partial<Options>) {
		super(source, options);

		this.dest = this.options.dest;
		if (!this.dest) this.dest = this.source.parentElement;
	}

	onFragmentStart(fragment: Fragment, forcedBreak: boolean): void {
		discardMarginBreakBefore(fragment.body, forcedBreak, this.currentFragMut);
		this.addImagesToLoad(fragment);
	}

	onFragmentBreak(fragment: Fragment): void {
		this.setLastBreakedHeight(fragment.body);
	}

	setLastBreakedHeight(container: HTMLElement): void {
		let lastStatic = container;
		let lastToFill: HTMLElement = null;
		const isStaticBreakedAfterHtmlBlock = walk.predicate(walk.isHTMLElement, walk.isStatic, walk.displayAsBlock, walk.isBreakedAfter);
		while (lastStatic && lastStatic.hasAttribute('ps-breaked-after')) {
			if (getCustomProp(lastStatic, 'box-remaining-extent') == 'fill') lastToFill = lastStatic;
			// TODO Set the height of the first breaked child of the row
			// TODO The bottomOffset can be null if the cell is empty
			if (walk.isRow(getComputedStyle(lastStatic))) {
				lastStatic = walk.firstChild(lastStatic, isStaticBreakedAfterHtmlBlock);
			} else {
				lastStatic = walk.lastChild(lastStatic, walk.isStaticHtmlBlock);
			}
		}
		if (lastToFill && lastToFill != container) {
			const bottomOffset = this.layoutContext.bottomOffsets.get(lastToFill);

			if (bottomOffset) {
				const maxBreakedBottom = this.layoutContext.bodyBottom - bottomOffset.after - bottomOffset.currentMargin;
				const lastBreakedBottom = rects.boundingScrollRect(lastToFill).bottom;
				const newHeight = parseFloat(getComputedStyle(lastToFill).height) + (maxBreakedBottom - lastBreakedBottom) + 'px';
				this.layoutContext.mutations.setStyle(lastToFill, {height: newHeight + 'px'});
			}
		}

	}

	addImagesToLoad(fragment: Fragment): void {
		addStyleImagesToLoad(fragment.container, this.imagesToLoad);
		if (fragment.area) addStyleImagesToLoad(fragment.area, this.imagesToLoad);
		for (const areaName in fragment.areas) {
			addStyleImagesToLoad(fragment.areas[areaName], this.imagesToLoad);
		}
	}

}

export function createAreas(fragment: Fragment, areasProp?: string): void {
	const doc = fragment.container.ownerDocument;
	fragment.areas = {};
	if (!areasProp) areasProp = getCustomProp(fragment.container, 'areas');
	if (areasProp == 'auto') return;
	const areaNames = areasProp.split(' ');
	for (const areaName of areaNames) {
		const area = doc.createElement('ps-area');
		area.setAttribute('ps-name', areaName);
		fragment.areas[areaName] = (fragment.area || fragment.container).appendChild(area) as HTMLElement;
	}
}

export function cleanupAreas(fragment: Fragment): void {
	for (const areaName in fragment.areas) {
		const area = fragment.areas[areaName];
		if (!area.firstChild) area.remove();
	}
}

function discardMarginBreakBefore(elem: HTMLElement, keepByDefault: boolean, mut: Mutations): void {
	const style = getComputedStyle(elem);
	if (style.float != 'none') return;

	const marginBreak = getCustomProp(elem, 'margin-break');
	if (marginBreak.startsWith('discard') || (marginBreak.startsWith('auto') && !keepByDefault)) {
		mut.setAttr(elem, 'ps-margin-break-before', 'discard');
	}

	if (parseFloat(style.borderTop) || parseFloat(style.paddingTop)) return;

	if (walk.isRow(style)) {
		for (let child = walk.firstChild(elem, walk.isStaticHtmlElement); child; child = walk.nextSibling(child, walk.isStaticHtmlElement)) {
			discardMarginBreakBefore(child, keepByDefault, mut);
		}
	} else {
		const firstStatic = walk.firstChild(elem, walk.isStaticNode);
		if (walk.isHTMLElement(firstStatic)) discardMarginBreakBefore(firstStatic, keepByDefault, mut);
	}
}
