import postcss from 'postcss';
import safeParser from "postcss-safe-parser";
import valueParser from "postcss-value-parser";
import precss, {importFold, importUnfold, replaceMediaType, styleAttrPlugin, getPageSize} from './precss.js';
import {dom, io, logger, walk} from "./util.js";
import coreCSS from "./css/core-css.js";
import defaultCSS from "./css/default-css.js";
import {Hypher} from "./hypher.js";
import type {Process} from "./process";
import type {Options as PrecssOptions} from './precss.js';
import type {FunctionNode} from "postcss-value-parser";

export const REGISTER_PROPERTIES = 'CSS' in window && 'registerProperty' in CSS;

export interface CssContextOptions {
	coreCSS: boolean;
	defaultCSS: boolean;
	preprocessCSS: boolean;
	defaultPageSize: string;
	mediaType: string;
	hyphUri: string;
}

export class CssContext {
	preparedSheets = new Set();
	coreSheet: StyleSheet = null;
	defaultSheet: StyleSheet = null;
	hyphers: { [key: string]: Hypher | false } = {};
	private _styleAttrProcessor: postcss.Processor;

	constructor(readonly doc: Document, readonly options: CssContextOptions) {
	}

	* process(): Process {
		const headStart = this.doc.head.firstChild;
		const defaultPageSize = this.options.defaultPageSize ? getPageSize(this.options.defaultPageSize) : [window.innerWidth + 'px', window.innerHeight + 'px'] as [string, string];
		const precssOptions = {
			defaultPageSize,
			mediaType: this.options.mediaType,
			propertiesRegistered: REGISTER_PROPERTIES
		};
		if (this.options.coreCSS) {
			this.coreSheet = insertStyleSheet(this.doc, coreCSS(defaultPageSize, REGISTER_PROPERTIES, this.doc), true, 'ps-core-sheet', headStart);
		}
		if (this.options.defaultCSS) {
			yield (async () => {
				const defaultCSSResult = await transform(defaultCSS(), document.baseURI, precssOptions);
				this.defaultSheet = insertStyleSheet(this.doc, defaultCSSResult.css, true, 'ps-default-sheet', headStart);
			})();
		}

		registerPaintModule('marks', MarksPainter);
		yield (async () => {
			await dom.docComplete(this.doc);
			if (this.options.preprocessCSS) await this.prepareSheets(precssOptions);
		})();

		// Ensures that the fonts are loaded
		if ('fonts' in this.doc) {
			const fontsLoaded = [];
			for (const font of this.doc.fonts) {
				if (font.status == 'unloaded') fontsLoaded.push(font.load());
				else if (font.status == 'loading') fontsLoaded.push(font.loaded);
			}
			if (fontsLoaded.length) yield Promise.all(
				fontsLoaded.map((promise) => promise.catch((e) => {
					logger.warn('A font could not be loaded.');
					logger.error(e);
				}))
			);
		}
	}

	/** Should be only called if CSS.registerProperty is available */
	processStyleAttr(elem: Element): void {
		if (elem.hasAttribute('ps-excluded')) return;
		if (this._styleAttrProcessor == null) this._styleAttrProcessor = postcss().use(styleAttrPlugin());
		const style = elem.getAttribute("style");
		if (style == null) return;
		const result = this._styleAttrProcessor.process(style, {from: document.baseURI, parser: safeParser});
		elem.setAttribute("style", result.css);
	}

	async prepareSheets(preprocOptions: PrecssOptions): Promise<any[]> {
		const preparations = [];

		const sheetsToPrepare: CSSStyleSheet[] = Array.prototype.filter.call(this.doc.styleSheets, (sheet: CSSStyleSheet) => {
			const cssNode = sheet.ownerNode as Element;
			return !cssNode.hasAttribute('ps-excluded') && !this.preparedSheets.has(cssNode);
		});

		for (const sheet of sheetsToPrepare) {
			const cssElem = sheet.ownerNode as HTMLLinkElement | HTMLStyleElement;
			let linkElem: HTMLLinkElement;
			let transformation: Promise<postcss.Result>;

			if (walk.isHTMLTag('link')(cssElem)) {
				linkElem = cssElem.cloneNode(false);
				linkElem.setAttribute('ps-href', cssElem.getAttribute('href'));
				transformation = io.load(linkElem.href).then((content) => transform(content, cssElem.href, preprocOptions));
			} else {
				linkElem = document.createElement('link');
				if (cssElem.id) linkElem.id = cssElem.id;
				if (cssElem.media) linkElem.media = cssElem.media;
				linkElem.type = "text/css";
				linkElem.rel = "stylesheet";
				transformation = transform(cssElem.textContent, this.doc.baseURI, preprocOptions) as any as Promise<postcss.Result>;
			}

			preparations.push(transformation.then((result) => {
				const blob = new Blob([result.css], {type: 'text/css'});
				linkElem.href = URL.createObjectURL(blob);
				if (this.options.mediaType && cssElem.media) linkElem.media = replaceMediaType(cssElem.media, this.options.mediaType);
				cssElem.parentNode.replaceChild(linkElem, cssElem);
				this.preparedSheets.add(linkElem);
				return new Promise<void>((resolve, reject) => {
					linkElem.addEventListener('load', resolve as any as EventListenerOrEventListenerObject);
					linkElem.addEventListener('error', reject);
				});
			}).catch((e) => {
				logger.warn(e.message || `An error occured while loading '${'href' in cssElem ? cssElem.href : linkElem.href}'`);
			}));
		}
		return Promise.all(preparations);
	}

	testHyphens(element: HTMLElement, style: CSSStyleDeclaration): false | Hypher | Promise<void | Hypher> {
		if (!walk.firstChild(element, walk.isStaticText)) return false;
		if (style.getPropertyValue('--ps-hyphens').trim() != 'auto') return false;

		let lang = '';
		for (let parent = element; parent; parent = parent.parentElement) {
			if ((lang = parent.lang)) break;
		}

		if (!lang) return false;
		const locale = new Intl.Locale(lang);
		lang = locale.language;
		if (locale.language == "en") {
			// TODO better selection based on the region "en-IN" => "en-gb" ?
			if (locale.region == "GB") lang = "en-gb";
			else lang = "en-us";
		}

		const hypher = this.hyphers[lang];
		if (hypher !== undefined) return hypher;
		else {
			let hyphLoad: Promise<string>;
			if (window.postscriptumBridge?.loadHyph) {
				hyphLoad = window.postscriptumBridge?.loadHyph(lang);
			} else {
				if (!this.options.hyphUri) {
					logger.warn(`Hyphenation dictionaries URL (hyphUri) not defined.`);
					return;
				}
				hyphLoad = io.load(`${this.options.hyphUri}/${lang}.json`);
			}
			return hyphLoad.then((json) => {
				const hypher = new Hypher(JSON.parse(json));
				this.hyphers[lang] = hypher;
				return hypher;
			}).catch(() => {
				this.hyphers[lang] = false;
				logger.warn(`Failed to load the hyphenation patterns for language ${lang}.`);
			});
		}
	}

	hyphenate(element: HTMLElement, hypher: Hypher): void {
		for (const child of element.childNodes) {
			if (walk.isStaticText(child)) {
				const text = child.nodeValue;
				if (text.trim().length) child.nodeValue = hypher.hyphenateText(child.nodeValue);
			}
		}
	}
}

export const cssContexts = new Map<Document, CssContext>();

export type Media = 'screen' | 'print' | 'projection';

function transform(cssText: string, baseURI: string, preprocOptions: PrecssOptions): postcss.LazyResult {
	const plugins = preprocOptions ? [precss(preprocOptions)] : [];

	const options = {from: baseURI, parser: safeParser};
	const processor = postcss().use(importUnfold(options));
	for (const plugin of plugins) processor.use(plugin);
	if (preprocOptions) processor.use(importFold());

	return processor.process(cssText, options);
}

export function insertStyleSheet(doc: Document, sheet: string, excluded?: boolean, id?: string, before?: Node): StyleSheet {
	const node = doc.createElement('style');
	node.textContent = sheet;

	if (id) node.id = id;
	if (excluded) node.setAttribute('ps-excluded', '');

	doc.head.insertBefore(node, before);

	return node.sheet;
}

export function insertLinkSheet(doc: Document, url: string | URL, excluded?: boolean, id?: string, before?: Node): Promise<StyleSheet> {
	const node = doc.createElement('link');
	node.rel = 'stylesheet';
	node.type = 'text/css';
	node.href = url.toString();

	if (id) node.id = id;
	if (excluded) node.setAttribute('ps-excluded', '');

	return new Promise((resolve, reject) => {
		node.addEventListener('load', () => {
			resolve(node.sheet);
		});
		node.addEventListener('error', () => {
			reject(node);
		});
		doc.head.insertBefore(node, before);
	});
}

export function getCustomStyle(element: Element): CSSStyleDeclaration {
	return REGISTER_PROPERTIES ? getComputedStyle(element) : getComputedStyle(element, '::backdrop');
}

export function getCustomProp(eltOrStyle: Element | CSSStyleDeclaration, propName: string): string {
	if (dom.isElement(eltOrStyle)) eltOrStyle = getCustomStyle(eltOrStyle);
	return eltOrStyle.getPropertyValue(`--ps-${propName}`).trim();
}

export function getCustomPluginProp(eltOrStyle: Element | CSSStyleDeclaration, propName: string): string {
	if (dom.isElement(eltOrStyle)) eltOrStyle = getCustomStyle(eltOrStyle);
	return eltOrStyle.getPropertyValue(`--psp-${propName}`).trim();
}

export function computeLength(strLength: string, percentBase: number | (() => number)): number {
	let value = 0;
	value = parseFloat(strLength);
	if (!value) return 0;
	try {
		const length = CSSStyleValue.parse("width", strLength) as CSSUnitValue;
		if (length.unit == 'percent') {
			if (typeof percentBase == 'function') percentBase = percentBase();
			return percentBase * value / 100;
		} else return length.to('px').value;
		// eslint-disable-next-line @typescript-eslint/no-unused-vars
	} catch (e) {
		const length = strLength.trim().match(/([\d.]+)(px|vw|vh|vmin|vmax|cm|mm|in|pt|pc|%)$/);
		if (length) {
			const [, , unit] = length;
			if (unit == '%') {
				if (typeof percentBase == 'function') percentBase = percentBase();
				return percentBase * value / 100;
			} else if (unit.startsWith('v')) {
				const width = window.innerWidth;
				const height = window.innerHeight;
				if (unit == 'vw') return value * width / 100;
				else if (unit == 'vh') return value * height / 100;
				else if (unit == 'vmin') return value * Math.min(width, height) / 100;
				else if (unit == 'vmax') return value * Math.max(width, height) / 100;
			} else {
				if (unit == 'px') return value;
				else {
					const inValue = value * 96;
					switch (unit) {
					case 'in':
						return inValue;
					case 'cm':
						return inValue / 2.54;
					case 'mm':
						return inValue / 25.4;
					case 'pt':
						return inValue / 72.0;
					case 'pc':
						return inValue / 6;
					}
				}
			}
		} else {
			logger.warn(`Unable to compute the length '${strLength}'.`);
		}
	}
}

export function computeMinOrMaxLength(minMaxValue: string, percentBase: number, noneValue: number): number {
	if (minMaxValue == 'none') return noneValue;
	else if (minMaxValue.match(/^\d/)) return computeLength(minMaxValue, percentBase);
	else return NaN;
}

export function addStyleImagesToLoad(element: Element, set: Set<string>): void {
	const bkgImageProp = getComputedStyle(element).backgroundImage;
	if (bkgImageProp != "none") {
		const bkgImageValue = valueParser(bkgImageProp);
		for (const bkgImageFn of bkgImageValue.nodes.filter((node) => node.type == "function") as FunctionNode[]) {
			if (bkgImageFn.value == "url") {
				const bkgImageUrl = bkgImageFn.nodes[0].value;
				set.add(bkgImageUrl);
			}
		}
	}
}

class MarksPainter {
	static get inputProperties(): string[] {
		return ['--ps-bleed-size', '--ps-marks'];
	}

	/* tslint:disable:variable-name */
	paint(ctx: CanvasRenderingContext2D, {width, height}: { width: number, height: number }, styleMap: Map<string, string[]>): void {
		const marksProp = styleMap.get('--ps-marks')[0];
		if (!marksProp || marksProp == 'none') return;

		const bleedProp = styleMap.get('--ps-bleed-size')[0];
		let bleedLength;
		if ('parse' in CSSUnitValue) bleedLength = CSSStyleValue.parse("width", bleedProp) as CSSUnitValue;
		else {
			const length = bleedProp.match(/([\d.]+)(px|cm|mm|in|pt|pc)$/);
			bleedLength = new CSSUnitValue(parseFloat(length[1]), length[2]);
		}

		const bleed = bleedLength.to('px').value;

		ctx.lineWidth = .5;
		const bleed_1_3 = bleed / 3;

		if (marksProp.includes('crop')) {
			const bleed_2_3 = bleed * 2 / 3;
			ctx.beginPath();

			/* top-left crop */
			ctx.moveTo(bleed_1_3, bleed);
			ctx.lineTo(bleed_2_3, bleed);
			ctx.moveTo(bleed, bleed_1_3);
			ctx.lineTo(bleed, bleed_2_3);

			/* top-right crop */
			ctx.moveTo(width - bleed, bleed_1_3);
			ctx.lineTo(width - bleed, bleed_2_3);
			ctx.moveTo(width - bleed_2_3, bleed);
			ctx.lineTo(width - bleed_1_3, bleed);

			/* bottom-right crop */
			ctx.moveTo(width - bleed_1_3, height - bleed);
			ctx.lineTo(width - bleed_2_3, height - bleed);
			ctx.moveTo(width - bleed, height - bleed_1_3);
			ctx.lineTo(width - bleed, height - bleed_2_3);

			/* bottom-left crop */
			ctx.moveTo(bleed, height - bleed_1_3);
			ctx.lineTo(bleed, height - bleed_2_3);
			ctx.moveTo(bleed_1_3, height - bleed);
			ctx.lineTo(bleed_2_3, height - bleed);

			ctx.stroke();
		}

		if (marksProp.includes('cross')) {
			const width_1_2 = width / 2;
			const height_1_2 = height / 2;
			const bleed_1_2 = bleed / 2;
			const bleed_1_6 = bleed / 6;

			/* top circle */
			ctx.beginPath();
			ctx.arc(width_1_2, bleed_1_2, bleed_1_6, 0, Math.PI * 2);
			ctx.stroke();

			/* right circle */
			ctx.beginPath();
			ctx.arc(width - bleed_1_2, height / 2, bleed_1_6, 0, Math.PI * 2);
			ctx.stroke();

			/* bottom circle */
			ctx.beginPath();
			ctx.arc(width / 2, height - bleed_1_2, bleed_1_6, 0, Math.PI * 2);
			ctx.stroke();

			/* left circle */
			ctx.beginPath();
			ctx.arc(bleed_1_2, height / 2, bleed_1_6, 0, Math.PI * 2);
			ctx.stroke();

			ctx.beginPath();
			/* top cross lines */
			ctx.moveTo(width_1_2 - bleed_1_3, bleed_1_2);
			ctx.lineTo(width_1_2 + bleed_1_3, bleed_1_2);
			ctx.moveTo(width_1_2, bleed_1_2 - bleed_1_3);
			ctx.lineTo(width_1_2, bleed_1_2 + bleed_1_3);

			/* right cross lines */
			ctx.moveTo(width - bleed_1_2 - bleed_1_3, height_1_2);
			ctx.lineTo(width - bleed_1_2 + bleed_1_3, height_1_2);
			ctx.moveTo(width - bleed_1_2, height_1_2 - bleed_1_3);
			ctx.lineTo(width - bleed_1_2, height_1_2 + bleed_1_3);

			/* bottom cross lines */
			ctx.moveTo(width_1_2 - bleed_1_3, height - bleed_1_2);
			ctx.lineTo(width_1_2 + bleed_1_3, height - bleed_1_2);
			ctx.moveTo(width_1_2, height - bleed_1_2 - bleed_1_3);
			ctx.lineTo(width_1_2, height - bleed_1_2 + bleed_1_3);

			/* left cross lines */
			ctx.moveTo(bleed_1_2 - bleed_1_3, height_1_2);
			ctx.lineTo(bleed_1_2 + bleed_1_3, height_1_2);
			ctx.moveTo(bleed_1_2, height_1_2 - bleed_1_3);
			ctx.lineTo(bleed_1_2, height_1_2 + bleed_1_3);

			ctx.stroke();
		}
	}
}

function registerPaintModule(name: string, klass: { new(): any }): void {
	if (!('paintWorklet' in CSS)) return;
	const module = `registerPaint('${name}', ${klass.toString()})`;
	const blob = new Blob([module], {type: 'application/javascript'});
	(CSS as any).paintWorklet.addModule(URL.createObjectURL(blob));
}

