interface Document {
	createTreeWalker(root: Node, whatToShow?: number, filter?: (node: Node) => number, entityReferenceExpansion?: boolean): TreeWalker;

	createNodeIterator(root: Node, whatToShow?: number, filter?: (node: Node) => number): NodeIterator;

	caretPositionFromPoint(x: number, y: number): CaretPosition;

	fonts: FontFaceSet;
}

interface Node {
	cloneNode(deep?: boolean): this;
}

interface CaretPosition {
	readonly offset: number;
	readonly offsetNode: Node;

	getClientRect(): DOMRect | null;
}

interface CSSStyleDeclaration {
	// eslint-disable-next-line @typescript-eslint/naming-convention
	MozTextAlignLast: string | null;
	// TODO Typescript bug report
	float: string | null;
}

interface CSSUnitValue {

}


declare namespace CSS {
	function registerProperty(definition: PropertyDefinition): void
}


declare module "css-typed-om" {
	function polyfill(window: Window): void;

	export default polyfill;
}

declare module 'postcss-safe-parser' {
	import postcss = require("postcss");
	const safeParser: typeof postcss.parse;
	export = safeParser;
}
