import type {FragmentorEvents, FragmentorOptions} from "./fragments.js";
import {createAreas, Fragment, Fragmentor} from "./fragments.js";
import {type CountersStack, getCounterValue} from "./counters.js";
import {parseCounters} from "./counters.js";
import {dom, io, logger, ranges, walk} from "./util.js";
import type {Flow, ForcedBreakType} from "./flow.js";
import {FlowIterator} from "./flow.js";
import {columnsSubProcessor} from "./columns.js";
import {
	CORNERS,
	createBookmarks,
	HORIZONTAL_MARGIN_BOXES,
	prepareGcpm,
	setBookmarkLabels,
	setLeaders,
	setMarginContents,
	setPageTargetCounters,
	setTargetCounters,
	setTargetsCounters,
	SIDES,
	VERTICAL_MARGIN_BOXES
} from "./gcpm.js";
import {breakAtPoint, unbreak} from "./breaks.js";
import {addStyleImagesToLoad, CssContext, cssContexts, getCustomProp, getCustomStyle, insertStyleSheet, REGISTER_PROPERTIES} from "./css.js";
import Mutations from "./mutations.js";
import {rowSubFragmentor, trWithRowSpanSubFragmentor} from "./rows.js";
import type {Hypher} from "./hypher.js";
import type {Point} from "./util/ranges.js";
import type {Process} from "./process.js";

export const printableFormat = ["a3", "a4", "a5", "b4", "b5", "jis-b4", "jis-b5", "letter", "legal", "ledger"];

export type Size = [number, number];

/**
 * Sequence of pages
 *
 * A sequence is a set of page without a forced page break.
 */
export class Sequence {
	pageName: string;
	pageGroupStart: boolean;
	breakType: ForcedBreakType;
	breakPoint: Point;
	startPageIndex = -1;
	endPageIndex = -1;
	batches: HTMLElement[] = [];

	/**
	 * Sequence constructor
	 *
	 * @param continuousFlow  The range returned by ContinuousFlowIterator.
	 */
	constructor(continuousFlow: Flow) {
		this.pageName = continuousFlow.pageName;
		this.pageGroupStart = continuousFlow.pageGroupStart;
		this.breakType = continuousFlow.breakType;
	}
}

export class Page extends Fragment {
	marginBoxes: { [marginName: string]: HTMLElement };
	side: string;
	blank: boolean;
	printSize: Size;
}

export interface PaginatorOptions extends FragmentorOptions {
	source?: HTMLElement;
	bookmarks: boolean;
	rawPageCounters: boolean;
}

export interface PaginatorEvents extends FragmentorEvents {
	'preparation-start': Record<string, never>;
	'prepare': { node: Node };
	'preparation-end': Record<string, never>;
	'pagination-start': Record<string, never>;
	'pagination-end': Record<string, never>;
	'sequence-start': { sequence: Sequence };
	'sequence-end': { sequence: Sequence };
	'page-start': { page: Page, body: HTMLElement };
	'page-standalone': { page: Page };
	'page-end': { page: Page };
	'print-start': Record<string, never>;
	'print-end': Record<string, never>;
}

export class Paginator extends Fragmentor<PaginatorOptions, PaginatorEvents> {
	static defaultOptions: PaginatorOptions = io.mergeOptions({}, Fragmentor.defaultOptions as PaginatorOptions, {
		source: null,
		bookmarks: true,
		rawPageCounters: true
	});
	sequences: Sequence[] = [];
	currentSequence: Sequence;
	previousSequence: Sequence;
	firstPageSide: "left" | "right";
	flowIterator: FlowIterator;
	avoidBreakTypes = ['avoid', 'avoid-page'];
	forcedBreakTypes: ForcedBreakType[] = ['always', 'page', 'left', 'right', 'recto', 'verso'];
	printSize: Size = [-Infinity, -Infinity];

	constructor(options?: Partial<PaginatorOptions>) {
		super(options?.source || document.body, options);
		this.addSubProcessor(trWithRowSpanSubFragmentor);
		this.addSubProcessor(rowSubFragmentor);
		this.addSubProcessor(columnsSubProcessor);
		this._process = this.paginationProcess();
		this.gcpmContext.rawPageCounters = options.rawPageCounters;
	}

	get pages(): Page[] {
		return this.fragments as Page[];
	}

	get currentPage(): Page {
		return this.currentFragment as Page;
	}

	set currentPage(page: Page) {
		this.currentFragment = page;
	}

	/**
	 * The main generator function of the pagination process.
	 */
	protected* paginationProcess(): Process {
		const pendingSource = this.source.closest('[ps-process=pending]');
		if (pendingSource) throw new Error("Fragmentation already underway on this source.");
		if (this.source == this.source.ownerDocument.documentElement) throw new Error("Pagination source cannot be the document element");

		this.dest.setAttribute('ps-process', 'pending');

		if (!walk.firstChild(this.source, walk.isStaticNode)) logger.warn("The pagination source has no static child");

		const initialScroll: [number, number] = [window.scrollX, window.scrollY];

		// We make sure that the destination has an id
		dom.id(this.dest);
		let cssContext = cssContexts.get(this.doc);
		if (!cssContext) {
			cssContext = new CssContext(this.doc, this.options);
			cssContexts.set(this.doc, cssContext);
			yield* cssContext.process();
		}

		if (this.listenerCount('preparation-start')) yield 'preparation-start';
		const nodes = walk.createIterator(this.source);
		let currentNode;
		while ((currentNode = nodes.nextNode())) {
			if (this.listenerCount('prepare')) this.emit({type: 'prepare', node: currentNode});
			if (walk.isElement(currentNode)) {
				if (REGISTER_PROPERTIES && currentNode.hasAttribute('style')) cssContext.processStyleAttr(currentNode);
				const style = getComputedStyle(currentNode);
				if (style.display != 'none') {
					// Move the caption on tables with `caption-side: bottom` at the end of the table
					// TODO test the property display 'table-caption' and 'table' of the currentNode and the parent instead of the prototype (perf ?)
					if (walk.isHTMLTable(currentNode) && currentNode.caption) {
						const captionStyle = getComputedStyle(currentNode.caption);
						if (captionStyle.captionSide == "bottom") currentNode.appendChild(currentNode.caption);
					}

					const customStyle = getCustomStyle(currentNode);
					prepareGcpm(currentNode, customStyle, this.gcpmContext);

					if (walk.isHTMLElement(currentNode)) {
						let hypher = cssContext.testHyphens(currentNode, style);
						if (hypher) {
							if (hypher instanceof Promise) hypher = yield hypher;
							if (hypher) cssContext.hyphenate(currentNode, hypher as Hypher);
						}
					}
				}
			}
		}
		if (this.listenerCount('preparation-end')) yield 'preparation-end';

		this.source.style.display = 'none';

		if (this.listenerCount('pagination-start')) yield 'pagination-start';

		const rootToSourceCounters = parseCounters(document.documentElement, ranges.positionBefore(this.source));
		let sourceCounterReset = getComputedStyle(this.source).counterReset;
		for (const counterName in rootToSourceCounters) {
			sourceCounterReset += ' ' + counterName + ' ' + rootToSourceCounters[counterName].at(-1).value;
		}
		this.source.style.counterReset = sourceCounterReset;

		this.flowIterator = new FlowIterator(this.source, this.forcedBreakTypes, this.options.maxFragmentTextLength);
		let flow = this.flowIterator.next();
		this.firstPageSide = flow.breakType == 'left' ? 'left' : 'right';
		this.currentSequence = new Sequence(flow);
		this.sequences.push(this.currentSequence);
		while (flow) {
			const body = breakAtPoint(this.source, ranges.endPoint(flow.range), 'before');
			this.currentSequence.batches.push(body);
			this.disconnectedDocFrag.appendChild(body);

			const newSequence = !flow.maxTextLength;
			flow = this.flowIterator.next();
			if (flow && newSequence) {
				this.currentSequence = new Sequence(flow);
				this.sequences.push(this.currentSequence);
			}
		}

		for (const sequence of this.sequences) {
			yield* this.sequenceProcess(sequence);
		}
		// TODO Better way to remove this attribute: move the hidden source in the last page instead of breaking it ?
		this.currentPage.body.removeAttribute('ps-breaked-after');
		for (const page of this.pages) {
			this.dest.appendChild(page.container);
		}
		for (const anim of this.doc.getAnimations()) {
			const target = (anim.effect as KeyframeEffect).target;
			if (target && target.hasAttribute('ps-anim-time')) {
				anim.currentTime = parseFloat(target.getAttribute('ps-anim-time'));
			}
		}
		// Writing the global page counter
		this.updatePagesCounter();

		let bookmarksRoot = null;
		if (this.options.bookmarks) {
			bookmarksRoot = createBookmarks(this.doc, this.gcpmContext);
			this.dest.insertBefore(bookmarksRoot, this.pages[0].container);
		}

		setTargetCounters(this.gcpmContext, this.pages);
		setTargetsCounters(this.gcpmContext);
		if (bookmarksRoot) yield setBookmarkLabels(bookmarksRoot);

		setLeaders(this.gcpmContext);

		yield Promise.all(Array.from(this.imagesToLoad, (url) => new Promise((resolve) => {
			const img = new Image();
			img.onload = resolve;
			img.onerror = resolve;
			img.src = url;
		})));

		this.source.remove();

		const firstPage = this.pages[0];
		let printSize;
		let firstPageSize = getComputedStyle(firstPage.container).getPropertyValue('--ps-size').trim();
		if (firstPageSize == "auto") firstPageSize = this.options.defaultPageSize;
		if (firstPageSize && printableFormat.includes(firstPageSize.split(' ')[0].toLowerCase()) && firstPage.printSize[0] == this.printSize[0] && firstPage.printSize[1] == this.printSize[1] && !firstPage.container.hasAttribute('ps-bleed')) {
			printSize = firstPageSize;
		} else {
			printSize = `${this.printSize[0]}px ${this.printSize[1]}px`;
		}
		insertStyleSheet(this.doc, `@page { size: ${printSize}; }`, true,"ps-print-size");

		window.addEventListener('beforeprint', () => {
			this.emit('print-start');
		});

		window.addEventListener('afterprint', () => {
			// Must be put in a microtask to be executed after the dialog on Firefox
			Promise.resolve().then(() => {
				this.emit('print-end');
			});
		});

		this.dest.setAttribute('ps-process', 'ended');
		if (this.listenerCount('pagination-end')) yield 'pagination-end';

		window.scrollTo(...initialScroll);
	}

	/**
	 * The generator function of the processing of a sequence.
	 *
	 * @param {Range} flow - A range returned by ContinuousFlowIterator
	 * @param {module:layout~Sequence} nextSequence - The following sequence
	 * @memberof module:postscriptum~Processor
	 */
	* sequenceProcess(sequence: Sequence): Process {
		this.previousSequence = this.currentSequence;
		this.currentSequence = sequence;
		if (this.listenerCount('sequence-start')) yield {type: 'sequence-start', sequence};

		// Creation of a blank page if needed
		if (this.pages.length) {
			// Ascertains the side of the next page according to the side of the first
			const nextPageSide = (this.pages.length + (this.firstPageSide == 'left' ? 1 : 0)) % 2 ? 'left' : 'right';
			const needBlankPage = (sequence.breakType == 'left' && nextPageSide == 'right')
				|| (sequence.breakType == 'right' && nextPageSide == 'left')
				|| (sequence.breakType == 'verso' && nextPageSide == this.firstPageSide)
				|| (sequence.breakType == 'recto' && nextPageSide != this.firstPageSide);

			if (needBlankPage) {
				this.addStandalonePage(this.currentPage.body.cloneNode(false), (this.previousSequence || this.currentSequence).pageName, true);
			}
		}

		// The start of the sequence is initialized to the root's start to includes the ancestors
		let body = sequence.batches.shift();
		while (body) {
			yield* this.pageProcess(body, sequence.batches.length > 0);
			this.currentSequence.pageGroupStart = false;

			if (this.lastEvent == 'no-overflow' && this.currentSequence.batches.length) {
				body = sequence.batches.shift();
				yield* this.continuePageProcess(body);
			}
			this.layoutContext.mutations.commit();
			this.currentFragMut.commit();

			if (this.lastEvent != "no-overflow") {
				body = breakAtPoint(this.currentPage.body, this.layoutContext.breakPoint);
				// In some case (invalid break point on the last page), the break can be at the end of body
				if (body.firstChild) this.onFragmentBreak(this.currentPage);
				else body = null;
			} else {
				body = null;
			}
			if (this.listenerCount('fragment-end')) yield {type: 'fragment-end', fragment: this.currentPage };
			// The definition of the margins of the previous page must be done after the cut, so that the last string-set is correctly identified
			this.onPageEnd(this.currentPage);
			if (this.listenerCount('page-end')) yield {type: 'page-end', page: this.currentPage};
			this.disconnectedDocFrag.appendChild(this.currentPage.container);
		}
		if (this.listenerCount('sequence-end')) yield {type: 'sequence-end', sequence};
		if (this.gcpmContext.pendingFloats.length) logger.warn("Some floats could not be inserted at the end of the page sequence.");
	}

	/**
	 * The generator function of the processing of a page.
	 *
	 * @param {boolean} blank - Signals if the page to process is blank
	 */
	* pageProcess(body: HTMLElement, abortIfUnderflow = false): Process {
		const firstSequencePage = this.currentSequence.startPageIndex == -1;
		const pageSide = (this.pages.length + (this.firstPageSide == 'right' ? 0 : 1)) % 2 ? 'left' : 'right';
		const page = this.currentPage = this.createPage(this.currentSequence.pageName, this.currentSequence.pageGroupStart, pageSide, false);
		this.pages.push(page);
		logger.addScope(`page ${page.number}`);
		if (firstSequencePage) this.currentSequence.startPageIndex = this.pages.length - 1;
		this.currentSequence.endPageIndex = this.pages.length - 1;
		this.dest.appendChild(page.container);
		if (this.listenerCount('page-start')) yield {type: 'page-start', page, body};
		this.setBleedAndMarks(page);

		this.layoutContext = null;
		body.style.display = '';

		body.setAttribute('ps-page-body', '');
		page.body = page.area.appendChild(body);
		createAreas(page);

		this.onFragmentStart(page, firstSequencePage);
		if (this.listenerCount('fragment-start')) yield {type: 'fragment-start', fragment: page };

		if (abortIfUnderflow && body.scrollHeight <= body.clientHeight) {
			this.lastEvent = "no-overflow";
			return;
		}
		yield* this.contentsProcess(page.body, page.areas);

		/**
		 * Dispatched when the pagination process starts.
		 *
		 * The attribute `ps-process` is set to `pending` right before this event.
		 * At this stage, the stylesheets are not parsed and the source is not hidden.
		 * @event start
		 */

		/**
		 * Dispatched when the pagination starts.
		 *
		 * At this stage, the stylesheets parsed and the source is hidden.
		 * @event pagination-start
		 */


		/**
		 * Dispatched when the pagination ends.
		 *
		 * @event pagination-end
		 */

		/**
		 * Dispatched when the pagination process ends.
		 *
		 * The attribute `ps-process` is set to `ended` right before this event.
		 * @event end
		 */
	}

	* continuePageProcess(body: HTMLElement): Process {
		if (this.layoutContext) {
			logger.debug("Max flow text length reached (after contents processing).");
			this.layoutContext.mutations.revert();
		} else {
			logger.debug("Max flow text length reached (before contents processing).");
		}
		this.currentFragMut.revert();
		unbreak(this.currentPage.body, body, 'before');
		this.onFragmentStart(this.currentPage, false);
		if (this.listenerCount('fragment-start')) yield {type: 'fragment-start', fragment: this.currentPage };
		yield* this.contentsProcess(this.currentPage.body, this.currentPage.areas);
	}

	addStandalonePage(body: HTMLElement, pageName: string, blank?: boolean, refPage?: Page): void {
		const refIndex = refPage ? this.pages.indexOf(refPage) : this.pages.length;
		const pageSide = (refIndex + (this.firstPageSide == 'right' ? 0 : 1)) % 2 ? 'left' : 'right';
		const page = this.currentPage = this.createPage(pageName, false, pageSide, blank);
		page.container.setAttribute('ps-processing', 'content');
		logger.addScope(`page ${page.number}`);
		if (refPage) {
			this.pages.splice(refIndex, 0, page);
			this.dest.insertBefore(page.container, refPage.container);
		} else {
			this.pages.push(page);
			this.dest.appendChild(page.container);
		}
		page.body = page.area.appendChild(body);
		createAreas(page);
		this.setBleedAndMarks(page);
		this.onFragmentStart(page, true);
		this.emit({type: 'fragment-start', fragment: page });
		this.emit({type: 'page-standalone', page});
		this.onPageEnd(page);
		this.emit({type: 'fragment-end', fragment: page });

		// Page added after the pagination: updates the global counter of the page count
		if (this.dest.getAttribute('ps-process') == 'ended') this.updatePagesCounter();
		else this.disconnectedDocFrag.appendChild(this.currentPage.container);
	}

	addImagesToLoad(page: Page): void {
		super.addImagesToLoad(page);
		for (const marginName in page.marginBoxes) {
			addStyleImagesToLoad(page.marginBoxes[marginName], this.imagesToLoad);
		}
	}

	// TODO onFragmentEnd ?
	onPageEnd(page: Page): void {
		page.container.setAttribute('ps-processing', 'margins');
		setMarginContents(page, this.gcpmContext);
		if (this.options.rawPageCounters) setPageTargetCounters(page, this.dest, this.gcpmContext);

		const ctnStyle = getComputedStyle(page.container);
		const width = parseFloat(ctnStyle.width);
		const height = parseFloat(ctnStyle.height);
		page.printSize = [width, height];
		if (width > this.printSize[0]) this.printSize[0] = width;
		if (height > this.printSize[1]) this.printSize[1] = height;

		page.container.removeAttribute('ps-processing');
		logger.removeScope();
	}

	setBleedAndMarks(page: Page): void {
		const style = getComputedStyle(page.container);
		const bleed = getCustomProp(style, 'bleed');
		let bleedSize = getCustomProp(style, 'bleed-size');
		const marks = getCustomProp(style, 'marks');
		if (bleed == 'auto' && marks.includes('crop')) {
			bleedSize = '6pt';
			page.container.style.setProperty('--ps-bleed-size', bleedSize);
		}
		if (parseFloat(bleedSize)) page.container.setAttribute('ps-bleed', marks);
	}


	/**
	 * Create a page
	 *
	 * @param doc {Document}
	 * @param name {string}
	 * @param number {integer}
	 * @param pseudos {string[]}
	 */
	createPage(name: string, firstOfGroup: boolean, side: string, blank: boolean): Page {
		const page = new Page();
		page.container = this.doc.createElement('ps-page');
		page.container.setAttribute('ps-processing', 'content');

		page.side = side;
		page.number = this.pages.length + 1;
		page.blank = blank;
		if (name) page.container.setAttribute('ps-name', name);
		let pseudos = side;
		if (page.number == 1) pseudos += ' first';
		if (blank) pseudos += ' blank';
		else if (firstOfGroup) pseudos += ' first-of-group';
		page.container.setAttribute('ps-pseudos', pseudos);

		page.marginBoxes = {};
		for (let i = 0; i < SIDES.length; i++) {
			const corner = this.doc.createElement('ps-margin-box');
			corner.setAttribute('ps-name', CORNERS[i]);
			page.marginBoxes[CORNERS[i]] = corner;
			page.container.appendChild(corner);

			const margin = this.doc.createElement('ps-margin');
			margin.setAttribute('ps-side', SIDES[i]);
			const boxNames = i % 2 == 0 ? HORIZONTAL_MARGIN_BOXES : VERTICAL_MARGIN_BOXES;
			for (const boxName of boxNames) {
				const marginBox = this.doc.createElement('ps-margin-box');
				const marginName = SIDES[i] + '-' + boxName;
				marginBox.setAttribute('ps-name', marginName);
				margin.appendChild(marginBox);
				page.marginBoxes[marginName] = marginBox;
			}
			page.container.appendChild(margin);
		}

		page.area = this.doc.createElement('ps-page-area');
		page.container.appendChild(page.area);
		return page;
	}

	updatePagesCounter(): void {
		const pagesCount= this.options.rawPageCounters ? this.pages.length : getCounterValue(this.pages.at(-1).container, 'page');
		const destCounterReset = getComputedStyle(this.dest).counterReset;
		const pagesCounterReset = `pages ${pagesCount}`;
		this.dest.style.counterReset = destCounterReset == "none" ? pagesCounterReset : destCounterReset + " " + pagesCounterReset;
	}
}
