import valueParser from "postcss-value-parser";
import {GCPMContext, isFloatCall} from "./gcpm.js";
import {Processor} from "./process.js";
import {events, io, logger, ranges, rects, walk} from "./util.js";
import {unbreakableElement} from "./breaks.js";
import shim from "./shim.js";
import {computeLength, getCustomProp, getCustomStyle} from "./css.js";
import Mutations from "./mutations.js";
import type {Fragmentor} from "./fragments";
import type {Unbreakable} from "./breaks.js";
import {type Point} from "./util/ranges";
import type {Process, ProcessorEvents, ProcessorOptions} from "./process.js";

export class Box {
	range: Range = new Range();
	inlinesRange?: Range;
	unbreakable?: Unbreakable;
	avoidBreakBefore = false;
	avoidBreakAfter = false;
	mainBlock?: HTMLElement;
	floats?: Float[];
	bottomOffset?: BottomOffset;
	startBlock: HTMLElement;
	endBlock: HTMLElement;
	bottom?: number;
	top?: number;
	inlinesRangeRect?: rects.Rect;
	inlinesStartBoundary?: boolean;
	inlineStart: number;
	hasInlineBlocks = false;
	isRepeated?: boolean;
	mutations? = new Mutations();
}

export interface LayoutContextEvents extends ProcessorEvents {
	'float-move': { float: Float, area: HTMLElement, areaBody: HTMLElement, areaBodyRect: rects.Rect, newAreaBodyRect: rects.Rect };
	'body-bottom-change': { oldBottom: number, newBottom: number };
}

export class LayoutContext extends events.EventEmitter<LayoutContextEvents> {
	root: HTMLElement;
	body: HTMLElement;
	areas: Areas;
	parentCtx?: LayoutContext;
	boxes: Box[] = [];
	floats: Float[] = [];
	overflowPoint: Point = null;
	overflowBox: Box = null;
	breakPoint?: Point;
	breakBox?: Box;
	bottomOffsets: Map<HTMLElement, BottomOffset>;
	mutations: Mutations = new Mutations();
	_bodyBottom: number;

	constructor(root: HTMLElement, body: HTMLElement, areas: Areas, parentCtx?: LayoutContext) {
		super();
		this.root = root;
		this.body = body;
		this.areas = areas;
		this.parentCtx = parentCtx;
		this.bottomOffsets = this.parentCtx ? this.parentCtx.bottomOffsets : new Map();
	}

	get bodyBottom(): number {
		if (!this._bodyBottom) this._bodyBottom = rects.boundingScrollRect(this.body).bottom;
		return this._bodyBottom;
	}

	set bodyBottom(val: number) {
		if (val != this._bodyBottom) {
			const oldBottom = this._bodyBottom;
			this._bodyBottom = val;
			this.emit({type: "body-bottom-change", oldBottom, newBottom: this._bodyBottom});
		}
	}
}


export type FloatAlignment = "auto" | "none" | "baseline" | "block-start-edge" | "block-end-edge" | "inline-start-edge" | "inline-end-edge";

export type Areas = { [areaName: string]: HTMLElement };

export interface Float {
	base: HTMLElement | SVGSVGElement;
	call: HTMLElement | SVGSVGElement;
	body: HTMLElement | SVGSVGElement;
	areaName: string;
	alignment: FloatAlignment;
}

export interface LayoutProcessorOptions extends ProcessorOptions {
	plugins: (string | [string, null | object])[] | Record<string, null | object>;
}

export interface LayoutProcessorEvents extends ProcessorEvents {
	'contents-start': Record<string, never>;
	'contents-end': Record<string, never>;
	'new-layout-context': { layoutContext: LayoutContext, previousContext: LayoutContext };
	'box-start': { layoutContext: LayoutContext, box: Box };
	'box-end': { layoutContext: LayoutContext, box: Box };
	'overflow-detected': { layoutContext: LayoutContext };
	'no-overflow': { layoutContext: LayoutContext };
	'no-break-point': { layoutContext: LayoutContext };
	'valid-break-point': { layoutContext: LayoutContext };
	'new-break-point-to-valid': { layoutContext: LayoutContext };
}

export interface BottomOffset {
	after: number;
	inside: number;
	currentMargin: number;
}

export abstract class LayoutProcessor<Options extends LayoutProcessorOptions = LayoutProcessorOptions, Events extends LayoutProcessorEvents = LayoutProcessorEvents> extends Processor<Options, Events> {
	static defaultOptions = io.mergeOptions({}, Processor.defaultOptions as LayoutProcessorOptions, {
		plugins: [],
	});

	source: HTMLElement;
	doc: HTMLDocument;

	subProcessors: SubProcessor[] = [];
	gcpmContext: GCPMContext;
	layoutContext: LayoutContext = null;
	avoidBreakTypes = ['avoid'];
	logNoValidBreakPoint = false;
	lastEvent: keyof Events;
	startOnRoot = false;
	disconnectedDocFrag: DocumentFragment;

	get mutations(): Mutations {
		logger.debug("LayoutProcessor.mutations deprecated. Please use LayoutContext.mutations");
		return this.layoutContext && this.layoutContext.mutations;
	}

	protected constructor(source: string | HTMLElement, options?: Partial<Options>) {
		super(options);
		this.source = typeof source == "string" ? document.querySelector(source) : source;
		if (!this.source) throw new Error("The pagination source is undefined");

		this.doc = this.source.ownerDocument;

		shim(this.source.ownerDocument.defaultView);

		if (this.options.plugins) {
			if (Array.isArray(this.options.plugins)) {
				for (const plugin of this.options.plugins) {
					if (Array.isArray(plugin)) this.use(plugin[0], plugin[1]);
					else this.use(plugin);
				}
			} else {
				for (const pluginName in this.options.plugins) this.use(pluginName, this.options.plugins[pluginName]);
			}
		}
		this.disconnectedDocFrag = this.doc.createDocumentFragment();
		this.gcpmContext = new GCPMContext(this.disconnectedDocFrag);
	}

	addSubProcessor(subProcessor: SubProcessor): this {
		this.subProcessors.push(subProcessor);
		return this;
	}

	prependSubProcessor(subProcessor: SubProcessor): this {
		this.subProcessors.unshift(subProcessor);
		return this;
	}

	* contentsProcess(root: HTMLElement, areas: Areas, parentCtx?: LayoutContext): Process {
		if (this.listenerCount('contents-start')) yield {type: 'contents-start'};
		let step = 'detect-overflow';
		let event: keyof LayoutProcessorEvents;
		let defaultNextStep;

		let ctx: LayoutContext = null;

		do {
			if (step == 'detect-overflow') {
				const previousContext = ctx;
				ctx = this.layoutContext = this.createLayoutContext(root, areas, parentCtx);
				if (this.listenerCount('new-layout-context')) yield {type: 'new-layout-context', layoutContext: ctx, previousContext };
				if (this.gcpmContext.pendingFloats.length) {
					const pendingFloats = this.gcpmContext.pendingFloats.splice(0);
					for (const float of pendingFloats) moveFloat(float, this.layoutContext, this.gcpmContext);
				}
				yield* this.boxesProcess(root);

				// A subfragmentor (ie columns) can return a break point at this stage
				if (ctx.breakPoint) {
					event = 'valid-break-point';
					defaultNextStep = 'break-point-found';
				} else {
					if (ctx.overflowPoint) {
						ctx.breakPoint = ranges.clonePoint(ctx.overflowPoint);
						ctx.breakBox = ctx.overflowBox;
						defaultNextStep = 'validate-break-point';
						event = 'overflow-detected';
					} else {
						ctx.breakPoint = ranges.positionAfter(root.lastChild);
						ctx.breakBox = null;
						event = 'no-overflow';
						defaultNextStep = 'break-point-found';
					}
				}
			} else if (step == 'validate-break-point') {
				const future = validBreakPoint(ctx, this.avoidBreakTypes);
				if (typeof future != 'boolean' && ranges.pointEquals(future.breakPoint, ctx.breakPoint)) throw new Error("Loop on break point validation");
				if (!future) {
					defaultNextStep = 'no-break-point';
					event = 'no-break-point';
					const lastBox = ctx.boxes.at(-1);
					const firstBoxIdx = ctx.boxes.findIndex((box) => !box.isRepeated);
					if (ctx.boxes.length - 1 > firstBoxIdx) {
						ctx.breakPoint = ranges.startPoint(lastBox.range);
						ctx.breakBox = lastBox;
					} else {
						ctx.breakPoint = ranges.endPoint(lastBox.range);
						ctx.breakBox = null;
					}
				} else if (future === true) {
					defaultNextStep = 'break-point-found';
					event = 'valid-break-point';
				} else {
					ctx.breakPoint = future.breakPoint;
					ctx.breakBox = future.breakBox;
					removeFloats(ctx.floats, ctx.breakPoint);
					event = 'new-break-point-to-valid';
					defaultNextStep = 'validate-break-point';
				}
			} else if (step == 'no-break-point') {
				if (this.logNoValidBreakPoint) logger.warn('No valid break point found.');
				defaultNextStep = 'break-point-found';
			}
			if (defaultNextStep == 'break-point-found') {
				let beforeBreakBox = !ctx.breakBox;
				let commitBox = beforeBreakBox;
				for (let i= ctx.boxes.length - 1; i>=0; i--) {
					const box = ctx.boxes[i];
					if (box == ctx.breakBox) {
						commitBox = !ranges.pointEquals(ctx.breakPoint, ranges.startPoint(ctx.breakBox.range));
						beforeBreakBox = true;
					} else if (beforeBreakBox) {
						commitBox = true;
					}
					if (commitBox) box.mutations.commit(ctx.mutations);
					else box.mutations.revert();
				}
			}
			step = event && this.listenerCount(event) ? yield {type: event, layoutContext: ctx} : null;
			this.lastEvent = event;
			if (!step) step = defaultNextStep;
		} while (step != 'break-point-found');

		if (this.listenerCount('contents-end')) yield {type: 'contents-end'};
		return event;
	}


	* boxesProcess(root: HTMLElement): Process {
		const doc = root.ownerDocument;

		const ctx = this.layoutContext;

		const bottomOffsetsStack = ctx.bottomOffsets.has(root.parentElement) ? [ctx.bottomOffsets.get(root.parentElement)] : [];
		const bottomOffset = updateBottomOffsets(bottomOffsetsStack, getComputedStyle(root), getCustomStyle(root));
		ctx.bottomOffsets.set(root, bottomOffset);

		// TODO DisplayedOrderTreeWalker to handle flex box and table captions?
		const nodes = walk.createWalker(root, walk.isStaticNode);
		if (!this.startOnRoot) nodes.nextNode();

		let box = new Box();
		box.range.setStartBefore(this.startOnRoot ? root : root.firstChild);
		if (this.listenerCount('box-start')) yield { type: 'box-start', layoutContext: ctx, box };

		const filledAreas = new Set<string>();

		const subFragmentorCtx: SubProcessorContext = {
			nodes,
			currentElement: null,
			currentCustomStyle: null,
			currentStyle: null,
			currentBox: box
		};

		const newBox = (startIsBlock: boolean): void => {
			box = new Box();
			subFragmentorCtx.currentBox = box;
			box.range.setStartBefore(nodes.currentNode);
			if (startIsBlock) box.startBlock = nodes.currentNode as HTMLElement;
		};

		const onBoxEnd = (isBlock: boolean): Point => {
			ctx.boxes.push(box);
			box.bottomOffset = isBlock ? bottomOffsetsStack.pop() : bottomOffsetsStack.at(-1);
			ctx.overflowPoint = testBoxOverflow(box, ctx.bodyBottom);
			if (box.floats && box.floats.length) {
				for (const float of box.floats) {
					if (float && !filledAreas.has(float.areaName)) {
						const filled = !moveFloat(float, ctx, this.gcpmContext, box);
						if (filled) filledAreas.add(float.areaName);
					}
				}
			}
			if (ctx.overflowPoint) ctx.overflowBox = box;
			return ctx.overflowPoint;
		};

		while (true) {
			let isBlock = false;
			if (nodes.currentNode.nodeType == Node.ELEMENT_NODE) {
				let currentElt = nodes.currentNode as HTMLElement;
				subFragmentorCtx.currentElement = currentElt;
				if (currentElt.localName == 'ps-debugger') debugger;

				if (isFloatCall(currentElt)) {
					const float = this.gcpmContext.floats.get(currentElt.previousElementSibling as HTMLElement);
					if (!box.floats) box.floats = [];
					box.floats.push(float);
					ctx.floats.push(float);
				}

				let style = subFragmentorCtx.currentStyle = getComputedStyle(currentElt);
				let customStyle = subFragmentorCtx.currentCustomStyle = getCustomStyle(currentElt);
				isBlock = walk.displayAsBlockStyle(style);

				if (isBlock) {
					if (!box.startBlock) box.startBlock = currentElt;
					box.endBlock = currentElt;
					if (!box.unbreakable) box.mainBlock = currentElt;
					const bottomOffset = updateBottomOffsets(bottomOffsetsStack, style, customStyle);
					ctx.bottomOffsets.set(currentElt, bottomOffset);

					if (!box.avoidBreakBefore) box.avoidBreakBefore = this.avoidBreakTypes.includes(getCustomProp(customStyle, 'break-before'));
					if (!box.unbreakable) box.unbreakable = unbreakableElement(currentElt);
					if (!box.isRepeated) {
						box.isRepeated = currentElt.hasAttribute('ps-repeated');
						const previousBox = ctx.boxes.at(-1);
						box.isRepeated = previousBox?.isRepeated && currentElt.closest('[ps-repeated]') != null;
					}

					if (!this.avoidBreakTypes.includes(getCustomProp(customStyle, 'break-inside'))) this.fixWidth(currentElt, style, ctx);
					for (const subFragmentor of this.subProcessors) {
						let subProcResult = subFragmentor.call(this, subFragmentorCtx) as SubProcessorResult;
						if (subProcResult && subProcResult instanceof Promise) subProcResult = yield subProcResult;
						if (subProcResult) {
							box.unbreakable = 'subFragmentor';
							const subLayoutResult = subProcResult as unknown as SubLayoutProcessResult;
							if (subLayoutResult.event == 'valid-break-point') {
								// TODO The last box can be invalid due to the interruption
								box.range.setEndAfter(currentElt);
								ctx.breakPoint = subLayoutResult.breakPoint;
								ctx.breakBox = box;
								ctx.boxes.push(box);
								return ctx;
							} else if (subLayoutResult.event == 'no-break-point') {
								box.range.setEndAfter(currentElt);
								ctx.overflowPoint = ranges.positionBefore(currentElt);
								ctx.overflowBox = box;
								ctx.boxes.push(box);
								return ctx;
							} else if (subLayoutResult.event == 'no-overflow') {
								break;
							}
						}
					}

					const newCurrentElt = nodes.currentNode as HTMLElement;
					if (newCurrentElt != currentElt) {
						currentElt = newCurrentElt;
						customStyle = getCustomStyle(currentElt);
						style = getComputedStyle(currentElt);
						isBlock = walk.displayAsBlockStyle(style);
						if (isBlock) box.endBlock = currentElt;
					}
					if (!box.avoidBreakAfter) box.avoidBreakAfter = box.isRepeated || this.avoidBreakTypes.includes(getCustomProp(customStyle, 'break-after'));
				}
			}

			let isInlineBlock = false;
			if (!isBlock) {
				// Not a block : creation of the inline range
				if (!box.inlinesRange) {
					box.inlinesRange = doc.createRange();
					box.inlinesRange.setStartBefore(nodes.currentNode);
					box.inlinesStartBoundary = true;
				}
				// The inline end boundary is updated at each node
				box.inlinesRange.setEndAfter(nodes.currentNode);
				if (walk.isInlineBlockElement(nodes.currentNode)) {
					isInlineBlock = true;
					box.hasInlineBlocks = true;
				}
			}

			const replaced = box.unbreakable == 'replacedElement' || box.unbreakable == 'subFragmentor';
			if (replaced || isInlineBlock || !nodes.firstChild()) {
				while (!nodes.nextSibling()) {
					const parentElement = nodes.parentNode() as HTMLElement;

					isBlock = !parentElement || walk.displayAsBlock(parentElement);
					if (parentElement && !box.avoidBreakAfter) box.avoidBreakAfter = this.avoidBreakTypes.includes(getCustomProp(parentElement, 'break-after'));

					if (nodes.currentNode == root) {
						box.range.setEndAfter(this.startOnRoot ? root : root.lastChild);
						onBoxEnd(isBlock);
						if (this.listenerCount('box-end')) yield { type: 'box-end', layoutContext: ctx, box };
						return ctx;
					} else {
						box.endBlock = parentElement;
					}
				}
				if (box.inlinesRange) box.inlinesStartBoundary = false;
				const nextIsBlock = walk.displayAsBlock(nodes.currentNode);
				if (!isBlock && nextIsBlock) {
					box.mainBlock = null;
				}
				if (isBlock || nextIsBlock) {
					box.range.setEndBefore(nodes.currentNode);
					const overflow = onBoxEnd(isBlock);
					if (this.listenerCount('box-end')) yield { type: 'box-end', layoutContext: ctx, box };
					if (overflow) return ctx;
					newBox(nextIsBlock);
					if (this.listenerCount('box-start')) yield { type: 'box-start', layoutContext: ctx, box };
				}
			} else if (!isBlock && !box.inlinesStartBoundary && walk.displayAsBlock(nodes.currentNode)) {
				// Case where the first descendant of an inline is a block
				box.mainBlock = box.endBlock = null;
				box.range.setEndBefore(nodes.currentNode);
				box.inlinesRange.setEndBefore(nodes.currentNode);
				const overflow = onBoxEnd(isBlock);
				if (this.listenerCount('box-end')) yield { type: 'box-end', layoutContext: ctx, box };
				if (overflow) return ctx;
				newBox(true);
				if (this.listenerCount('box-start')) yield { type: 'box-start', layoutContext: ctx, box };
			}
		}
		return ctx;
	}

	createLayoutContext(root: HTMLElement, areas: Areas, parentCtx: LayoutContext): LayoutContext {
		return new LayoutContext(root, root, areas, parentCtx);
	}

	fixWidth(elem: HTMLElement, style: CSSStyleDeclaration, ctx: LayoutContext): void {
		if (elem.hasAttribute('ps-fixed-width')) return;
		let fixed = false;

		// On tables, the width is fixed by setting the colgroup/col widths and and a min width
		if (walk.isHTMLTable(elem) && style.display == 'table' && elem.rows.length) {
			const caption = elem.caption == elem.firstElementChild && elem.caption;
			let colgroup = caption ? caption.nextElementSibling : elem.firstElementChild;

			const firstRowCells = Array.from(elem.rows[0].cells);
			const colCount = firstRowCells.reduce((count: number, cell: HTMLTableCellElement) => count + cell.colSpan, 0);

			if (colgroup.localName == 'colgroup') {
				if (colCount != colgroup.children.length) {
					logger.warn("Invalid colgroup/col count. The width of the table cannot be fixed.");
					return;
				}
			} else {
				// Creation of the colgroup if he does not exist
				colgroup = this.doc.createElement('colgroup');
				elem.insertBefore(colgroup, caption.nextElementSibling || elem.firstElementChild);
				for (let i = 0; i < colCount; i++) colgroup.appendChild(this.doc.createElement('col'));
				ctx.mutations.push({
					colgroup,
					revert(): void {
						this.colgroup.remove();
					}
				});
			}

			const widths = new Map();
			for (const col of colgroup.children) widths.set(col, getComputedStyle(col).width);
			for (const [col, width] of widths) ctx.mutations.setStyle(col, {width});

			ctx.mutations.setStyle(elem, {'min-width': style.width});
			fixed = true;
		} else if (elem.localName != 'tr' && walk.isRow(style)) {
			const widths = new Map();
			for (const cell of elem.children) {
				if (walk.isStatic(cell)) widths.set(cell, getComputedStyle(cell).width);
			}
			for (const [cell, width] of widths) ctx.mutations.setStyle(cell, {width});
			fixed = true;
		}
		if (fixed) ctx.mutations.setAttr(elem, 'ps-fixed-width');
	}
}

export function updateBottomOffsets(bottomOffsets: BottomOffset[], style: CSSStyleDeclaration, customStyle: CSSStyleDeclaration): BottomOffset {
	const lastBottomOffset = bottomOffsets.at(-1);
	const boxDecorationBreak = getCustomProp(customStyle, 'box-decoration-break');
	const {inside: lastInside, currentMargin: lastCurrentMargin} = lastBottomOffset || {inside: 0, currentMargin: 0};
	let bottomOffset: BottomOffset = {
		after: lastInside,
		inside: lastInside,
		currentMargin: 0
	};
	const marginBreak = getCustomProp(customStyle, 'margin-break');
	if (marginBreak == 'keep') bottomOffset.currentMargin = Math.max(parseFloat(style.marginBottom), lastCurrentMargin);
	let decoration = 0;
	if (boxDecorationBreak == 'clone') {
		decoration += parseFloat(style.paddingBottom);
		if (style.borderBottomStyle != 'none') {
			decoration += parseFloat(style.borderBottomWidth);
			if (style.display == 'table' && style.borderCollapse == 'separate') {
				const spacing = parseFloat(style.borderSpacing.split(' ')[1]);
				if (spacing) decoration += spacing;
			}
		}
	}
	if (decoration) {
		bottomOffset.after += bottomOffset.currentMargin;
		bottomOffset.inside += decoration + bottomOffset.currentMargin;
		bottomOffset.currentMargin = 0;
	}

	if (lastBottomOffset && Object.keys(bottomOffset).every((key: keyof BottomOffset) => bottomOffset[key] == lastBottomOffset[key])) bottomOffset = lastBottomOffset;
	else Object.freeze(bottomOffset);
	bottomOffsets.push(bottomOffset);
	return bottomOffset;
}

// TODO Should we handle by custom prop les orphans/widows? Some special values (inherit, initial, unset) are not computable and the native values can be useful for native columns.
// TODO Do not cut between an element and a preceding float
function validBreakPoint(ctx: LayoutContext, avoidBreakValues: string[]): boolean | { breakPoint: Point, breakBox: Box } {
	const doc = ctx.body.ownerDocument;
	const view = ctx.body.ownerDocument.defaultView;

	const boxIndex = ranges.findPointInRanges(ctx.boxes.map((box) => box.range), ctx.breakPoint, true);
	if (boxIndex == -1) throw new Error("Box of the break point not found.");
	let box = ctx.boxes[boxIndex];

	function boxStartBreak(box: Box): { breakPoint: Point, breakBox: Box } {
		return {breakPoint: ranges.startPoint(box.range), breakBox: box};
	}

	// Test of the break-inside
	const avoidedBreakInside = testAvoidBreakInside(ctx.breakPoint.container, ctx, avoidBreakValues, ctx.breakPoint);
	if (avoidedBreakInside) {
		const beforeAvoidBreakInside = ranges.positionBefore(avoidedBreakInside);
		const rootPoint = ranges.positionBefore(ctx.root);
		if (ranges.pointEquals(beforeAvoidBreakInside, rootPoint) || ranges.isPrecedingPoint(rootPoint, beforeAvoidBreakInside)) return false;
		const avoidedBreakBoxIndex = ranges.findPointInRanges(ctx.boxes.map((box) => box.range), beforeAvoidBreakInside, true);
		return boxStartBreak(ctx.boxes[avoidedBreakBoxIndex]);
	}

	// Point at the beginning of the box: we test the pages-break-before / avoid of the previous one
	if (ctx.breakPoint.container == box.range.startContainer && ctx.breakPoint.offset == box.range.startOffset) {
		const previousBox = boxIndex > 0 && ctx.boxes[boxIndex - 1];
		if (!previousBox) return false;

		if (box.avoidBreakBefore || previousBox.avoidBreakAfter) {
			// If the previous box has an inline we continue the treatment on this box
			if (previousBox.inlinesRange) {
				box = previousBox;
				ctx.breakPoint = ranges.endPoint(previousBox.inlinesRange);
			} else {
				return boxStartBreak(previousBox);
			}
		} else {
			return true;
		}
	}

	if (box.unbreakable) return boxStartBreak(box);

	const textBreak = box.inlinesRange && box.inlinesRange.isPointInRange(ctx.breakPoint.container, ctx.breakPoint.offset);
	if (textBreak) {
		if (ctx.breakPoint.offset == 0 && walk.firstChild(box.inlinesRange.startContainer, walk.isStaticNode) == ctx.breakPoint.container) return boxStartBreak(box);
		// Break in a text node
		const startRange = doc.createRange();
		startRange.setStart(box.inlinesRange.startContainer, box.inlinesRange.startOffset);
		startRange.setEnd(ctx.breakPoint.container, ctx.breakPoint.offset);
		const style = getComputedStyle(box.inlinesRange.commonAncestorContainer as Element);

		const startLines = rects.rectsLines(startRange.getClientRects(), view);
		if (!startLines.length) return boxStartBreak(box);
		// Firefox does not support orphans and widows, we use CSS variables for them
		const orphans = parseInt(style.getPropertyValue('--ps-orphans'));
		if (startLines.length < orphans) return boxStartBreak(box);

		const widows = parseInt(style.getPropertyValue('--ps-widows'));
		if (widows > 0) {
			const endRange = doc.createRange();
			endRange.setStart(ctx.breakPoint.container, ctx.breakPoint.offset);
			endRange.setEnd(box.inlinesRange.endContainer, box.inlinesRange.endOffset);

			const endLines = rects.rectsLines(endRange.getClientRects(), view);
			// TODO If there is a pseudo-element immediately before the break point and on the same line, startLines incorrectly includes this line.
			// TODO So, we cannot use startLines to identify the missing line for widows: we works on all the lines of the inlines range.
			const lines = rects.rectsLines(box.inlinesRange.getClientRects(), view);

			const missingLineCount = widows - endLines.length;
			if (missingLineCount > 0) {
				const missingLineIndex = lines.length - endLines.length - missingLineCount;
				if (missingLineIndex < orphans) return boxStartBreak(box);
				else {
					const missingLine = lines[missingLineIndex];
					const direction = getComputedStyle(box.mainBlock || walk.ancestorOrSelf(box.range.commonAncestorContainer, walk.isElement)).direction;
					box.inlineStart = direction == 'ltr' ? Math.ceil(missingLine.left) : Math.floor(missingLine.right);
					const linePoint = testLinesOverflow(box, startLines, Math.ceil(lines[missingLineIndex - 1].bottom));
					return {breakPoint: linePoint, breakBox: box};
				}
			}
		}
	} else {
		// TODO Should not happen, warning?
		return boxStartBreak(box);
	}

	return true;
}

// TODO Should we handle the textPosition in unbreakable inline elements?
export function testBoxOverflow(box: Box, bodyBottom: number): Point {
	if (box.mainBlock || !box.inlinesRange) {
		const startBlockRect = rects.boundingScrollRect(box.startBlock);
		box.top = startBlockRect.top;
		if (box.startBlock == box.endBlock) box.bottom = startBlockRect.bottom;
		else {
			box.bottom = rects.boundingScrollRect(box.endBlock).bottom;
			// In some case (negative margins), the start block bottom can be bigger than the end block bottom
			// TODO The intermediate block bottoms between the end and the start are not handled
			if (box.endBlock.contains(box.startBlock) && startBlockRect.bottom > box.bottom) box.bottom = startBlockRect.bottom;
		}
	} else {
		box.inlinesRangeRect = rects.boundingScrollRect(box.inlinesRange);
		box.bottom = box.inlinesRangeRect.bottom;
		box.top = box.inlinesRangeRect.top;
	}

	const maxBottom = bodyBottom - box.bottomOffset.after - box.bottomOffset.currentMargin;

	if (maxBottom - box.bottom < -0.1) {
		let overflowPoint = ranges.startPoint(box.range);
		// In some case (arabic with ltr direction...), the text position may be invalid if the top of the box is too close to the bottom of the body.
		// Inlines are not tested if there is less than 4px between the top of the box and the maxBottom
		// TODO Arbitrary, 1px is not enough, should be based on the fontSize but avoided for performances (getComputedStyle)
		if (!box.unbreakable && box.inlinesRange && maxBottom - box.top >= 4) overflowPoint = testInlineOverflow(box, maxBottom);
		return overflowPoint;
	}
	return null;
}

export function testInlineOverflow(box: Box, bodyBottom: number, maxBottom?: number): Point {
	if (!maxBottom) maxBottom = bodyBottom - box.bottomOffset.inside - box.bottomOffset.currentMargin;
	if (!box.inlinesRangeRect) box.inlinesRangeRect = rects.boundingScrollRect(box.inlinesRange);
	// The maxBottom can occur between the box bottom and the inline range even if there is no padding
	if (box.inlinesRangeRect.bottom < maxBottom) return ranges.endPoint(box.inlinesRange);
	if (!box.inlineStart) {
		const direction = getComputedStyle(box.mainBlock || walk.ancestorOrSelf(box.range.commonAncestorContainer, walk.isElement)).direction;
		box.inlineStart = direction == 'ltr' ? Math.ceil(box.inlinesRangeRect.left) : Math.floor(box.inlinesRangeRect.right);
	}

	/*
	 * TODO Chrome 97 returns the start of the text node if the test is done for the integer matching line.top.
	 * In other words, it rounds up the start of the line.top and fail to return the previous line if less than that round.
	 * Therefore, we are obliged to run through all the line to not fail in this case.
	 */
	const lines = rects.rectsLines(box.inlinesRange.getClientRects(), box.inlinesRange.startContainer.ownerDocument.defaultView);
	if (lines.length <= 1) return ranges.startPoint(box.range);

	return testLinesOverflow(box, lines, maxBottom);
}

export function testLinesOverflow(box: Box, lines: rects.Rect[], maxBottom: number): Point {
	const doc = box.range.commonAncestorContainer.ownerDocument;
	/*
	 * TODO Chrome 97 returns the start of the text node if the test is done for the integer matching line.top.
	 * In other words, it rounds up the start of the line.top and fail to return the previous line if less than that round.
	 * Therefore, we are obliged to run through all the line to not fail in this case.
	 */
	for (let i = 0; i <= lines.length; i++) {
		const line = lines[i];
		let overflowLine;
		// Overflow between the last line and the end of the box
		if (i == lines.length) overflowLine = lines[i - 1];
		else if (line.bottom > Math.ceil(maxBottom)) {
			if (i === lines.length - 1 || lines[i + 1].top > Math.ceil(maxBottom)) overflowLine = line;
		}

		if (overflowLine) {
			let overflowPoint = ranges.textPositionFromPoint(doc, box.inlineStart, Math.ceil(overflowLine.top) + 2);
			if (!overflowPoint) return ranges.startPoint(box.range);
			else if (!ranges.containsPoint(box.inlinesRange, overflowPoint)) {
				overflowPoint = ranges.textPositionFromPoint(doc, box.inlineStart, Math.ceil(overflowLine.top));
			}

			if (!ranges.containsPoint(box.inlinesRange, overflowPoint)) {
				logger.warn("Overflowing text position outside of the box inline range.");
				return ranges.startPoint(box.range);
			} else {
				// Case of a break at the beginning of an inline
				if (overflowPoint.container.nodeType == Node.TEXT_NODE && overflowPoint.offset == 0) overflowPoint = ranges.positionBefore(overflowPoint.container);
				const nodeAtPoint = ranges.nodeAtPoint(overflowPoint) || overflowPoint.container;
				if (walk.isElement(nodeAtPoint) && isFloatCall(nodeAtPoint)) overflowPoint = ranges.positionBefore(nodeAtPoint.previousElementSibling);
				if (box.hasInlineBlocks) {
					let parent = nodeAtPoint;
					while (parent != (box.mainBlock || box.range.commonAncestorContainer)) {
						if (walk.displayAsInlineBlock(parent)) {
							overflowPoint = ranges.positionBefore(parent);
							break;
						}
						parent = parent.parentElement;
					}
				}
				return overflowPoint;
			}
		}
	}
}

export function testAvoidBreakInside(fromNode: Node, ctx: LayoutContext, avoidBreakValues: string[], breakPoint?: Point, heightAtBreakElem = false): Element | false {
	let parent = fromNode.nodeType == Node.TEXT_NODE ? fromNode.parentElement : fromNode as Element;

	let rootBodyHeight: number;

	function getRootBodyHeight(): number {
		if (!rootBodyHeight) {
			let rootCtx = ctx;
			while (rootCtx.parentCtx) rootCtx = rootCtx.parentCtx;
			rootBodyHeight = rootCtx.body.getBoundingClientRect().height;
		}
		return rootBodyHeight;
	}

	let rootCtx = ctx;
	while (rootCtx.parentCtx) rootCtx = rootCtx.parentCtx;
	const toNode = rootCtx.body;

	while (parent && parent != toNode.parentElement) {
		if (walk.displayAsBlock(parent)) {
			const breakInside = getCustomProp(parent, 'break-inside');
			if (breakInside != "auto") {
				let avoidInside = avoidBreakValues.includes(breakInside) && !parent.hasAttribute("ps-breaked-before");
				if (!avoidInside) {
					const firstVal = valueParser(breakInside).nodes[0];
					if (firstVal.type == 'function' && firstVal.value == '-ps-avoid-if-below') {
						const elemThreshold = computeLength(firstVal.nodes[0].value, getRootBodyHeight());
						if (elemThreshold) {
							if (parent.scrollHeight - 1 < elemThreshold) avoidInside = true;
						}
						if (!avoidInside && breakPoint && firstVal.nodes.length > 2) {
							const beforeBreakThreshold = computeLength(firstVal.nodes[2].value, parent.getBoundingClientRect().height);
							if (beforeBreakThreshold) {
								if (heightAtBreakElem) {
									const breakElem = ranges.nodeAtPoint(breakPoint) as Element;
									const height = breakElem.getBoundingClientRect().top - parent.getBoundingClientRect().top;
									if (height - 1 < beforeBreakThreshold) avoidInside = true;
									heightAtBreakElem = false;
								} else {
									const range = document.createRange();
									range.setStartBefore(parent);
									range.setEnd(breakPoint.container, breakPoint.offset);
									if (range.getBoundingClientRect().height - 1 < beforeBreakThreshold) avoidInside = true;
								}
							}
						}
					}
				}
				if (avoidInside) return parent;
			}
		}
		parent = parent.parentElement;
	}
	return false;
}


// TODO Find a better way to modify/return the overflowPoint and rootBottom
export function moveFloat(float: Float, ctx: LayoutContext, gcpmContext: GCPMContext, box?: Box): boolean {
	if (ctx.overflowPoint && ranges.isPrecedingPoint(ranges.positionBefore(float.call), ctx.overflowPoint)) return false;
	const floatCallRect = rects.boundingScrollRect(float.call);
	if (floatCallRect.bottom >= ctx.bodyBottom) return false;

	const area = ctx.areas[float.areaName];
	if (!area) {
		ctx.mutations.pushToArray(gcpmContext.pendingFloats, float);
		return false;
	}
	const areaCtx = layoutCtxFromArea(ctx, area);
	if (area.lastChild && area.lastChild.nodeType != Node.TEXT_NODE) area.appendChild(document.createTextNode(' '));
	area.appendChild(float.body);

	const areaRect = rects.boundingScrollRect(area);
	const areaBody = areaCtx.body;
	const areaBodyRect = rects.boundingScrollRect(areaBody);

	const mut = {
		floatBody: float.body,
		revert(): void {
			this.floatBody.remove();
			this.floatBody.style.marginTop = '';
		}
	};

	/* ______________________________________
	// TODO Mutation to revert the marginTop ?
	float.body.style.marginTop = '';
	floatCallRect = rects.boundingScrollRect(float.call);
	let floatBodyRect = rects.boundingScrollRect(float.body);

	if (!rects.includesRect(areaRect, floatBodyRect)) {
		gcpmContext.pendingFloats.push(float);
		float.body.remove();
		return false;
	}
	console.log(areaRect, areaBodyRect, floatCallRect);
	______________________________________ */

	const verticalArea = areaRect.right <= areaBodyRect.left || areaRect.left >= areaBodyRect.right;
	let alignment = float.alignment;
	if (alignment == "auto") alignment = verticalArea ? 'baseline' : 'none';
	let floatBodyRect = rects.boundingScrollRect(float.body);
	if (alignment != 'none') {
		if (alignment == 'baseline') {
			if (verticalArea) {
				const baselineLoc = document.createElement('img');
				baselineLoc.setAttribute('ps-baseline-loc', '');
				float.body.insertBefore(baselineLoc, float.body.firstChild);
				const floatBaseline = rects.boundingScrollRect(baselineLoc).bottom;
				float.call.parentElement.insertBefore(baselineLoc, float.call.nextSibling);
				const targetBaseline = rects.boundingScrollRect(baselineLoc).bottom;
				baselineLoc.remove();

				if (floatBaseline < targetBaseline) float.body.style.marginTop = Math.max(targetBaseline - floatBaseline, 0) + 'px';
			}
		} else {
			let alignTopTo = -1;
			let alignLeftTo = -1;

			const block = walk.ancestor(float.call, walk.isBlockElement);
			const blockRect = rects.boundingScrollRect(block);
			if (alignment == 'block-start-edge') {
				if (verticalArea) alignTopTo = blockRect.top;
				else alignLeftTo = blockRect.left;
			} else if (alignment == 'block-end-edge') {
				if (verticalArea) alignTopTo = blockRect.bottom - floatBodyRect.height;
				else alignLeftTo = blockRect.right - floatBodyRect.width;
			} else if (!verticalArea) {
				if (alignment == 'inline-start-edge') alignLeftTo = floatCallRect.left;
				else alignLeftTo = floatCallRect.right - floatBodyRect.width;
			} else {
				const blockContents = document.createRange();
				blockContents.selectNodeContents(block);
				const lines = rects.rectsLines(blockContents.getClientRects(), window);
				const line = lines.find((line) => rects.includesRect(line, floatCallRect));
				if (alignment == 'inline-start-edge') alignTopTo = line.top;
				else alignTopTo = line.bottom - floatBodyRect.height;
			}

			if (alignTopTo != -1 && floatBodyRect.top < alignTopTo) float.body.style.marginTop = Math.max(alignTopTo - floatBodyRect.top, 0) + 'px';
			else if (alignLeftTo != -1 && floatBodyRect.left < alignLeftTo) float.body.style.marginLeft = Math.max(alignLeftTo - floatBodyRect.left, 0) + 'px';
		}

		floatBodyRect = rects.boundingScrollRect(float.body);
	}

	if (!rects.includesRect(areaRect, floatBodyRect)) {
		ctx.mutations.pushToArray(gcpmContext.pendingFloats, float);
		mut.revert();
		return false;
	}

	const newAreaBodyRect = rects.boundingScrollRect(areaBody);
	const eventResult = ctx.emit({type: "float-move", float, area, areaBody, areaBodyRect, newAreaBodyRect});
	if (eventResult == 'pending') ctx.mutations.pushToArray(gcpmContext.pendingFloats, float);
	if (eventResult === false || eventResult == 'pending') {
		mut.revert();
		return false;
	}

	if (box) {
		if (floatCallRect.bottom >= newAreaBodyRect.bottom) {
			mut.revert();
			ctx.overflowPoint = testInlineOverflow(box, newAreaBodyRect.bottom, floatCallRect.top);
			return false;
		}
	} else if (!newAreaBodyRect.height || !newAreaBodyRect.width) {
		mut.revert();
		return false;
	}
	/*
	let currentCtx = ctx;
	while (currentCtx.body != areaCtx.body) {
		const bodyMaxBottom = newAreaBodyRect.bottom - (currentCtx.bottomOffsets.get(areaBody).after - currentCtx.bottomOffsets.get(currentCtx.body).after);
		if (currentCtx.bodyBottom > bodyMaxBottom) currentCtx.bodyBottom = bodyMaxBottom;
		currentCtx = currentCtx.parentCtx;
	}
	*/
	if (areaCtx.bodyBottom != newAreaBodyRect.bottom) areaCtx.bodyBottom = newAreaBodyRect.bottom;

	if (box) ctx.overflowPoint = testBoxOverflow(box, ctx.bodyBottom);
	ctx.mutations.push(mut);

	return true;
}

export function removeFloats(floats: Float[], stopPoint?: Point): void {
	for (let i = floats.length - 1; i >= 0; i--) {
		const float = floats[i];
		if (!stopPoint || ranges.isPrecedingPoint(ranges.positionBefore(float.call), stopPoint)) {
			float.body.remove();
			floats.pop();
		} else break;
	}
}

function layoutCtxFromArea(ctx: LayoutContext, area: HTMLElement): LayoutContext {
	let areaCtx = ctx;
	while (areaCtx.body.parentElement != area.parentElement) areaCtx = areaCtx.parentCtx;
	return areaCtx;
}

export interface SubProcessorContext {
	currentElement: HTMLElement;
	currentCustomStyle: CSSStyleDeclaration;
	currentStyle: CSSStyleDeclaration;
	nodes: TreeWalker;
	currentBox: Box;
}

export interface SubFragmentor {
	parentFragmentor: Fragmentor<any>;
}

export function isSubFragmentor(object: any): object is SubFragmentor {
	return 'parentFragmentor' in object;
}

export type SubLayoutProcessResult = { event: 'valid-break-point' | 'no-break-point' | 'no-overflow', breakPoint?: Point };
export type SubProcessorResult =
	false |
	Promise<false | SubLayoutProcessResult>;

export type SubProcessor = (ctx: SubProcessorContext) => SubProcessorResult;
