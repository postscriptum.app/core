import type {Areas, SubFragmentor, SubProcessor, SubProcessorContext, SubProcessorResult} from "./layout.js";
import {isSubFragmentor, LayoutContext} from "./layout.js";
import {io, logger, ranges, rects, walk} from "./util.js";
import type {FragmentorEvents, FragmentorOptions} from "./fragments.js";
import {createAreas, Fragment, Fragmentor} from "./fragments.js";
import {breakAtPoint, unbreak} from "./breaks.js";
import {computeLength, computeMinOrMaxLength, getCustomProp, getCustomStyle} from "./css.js";
import type {ForcedBreakType} from "./flow.js";
import {FlowIterator} from "./flow.js";
import {Mutations} from "./mutations.js";
import type {Point} from "./util/ranges";
import type {Process} from "./process";

export class Column extends Fragment {
}

export class ColumnSpan extends Fragment {
}

class ColumnsGroup {
	sequences: HTMLElement[] = [];
	columnParent: HTMLElement = null;
	span: HTMLElement = null;
	columnsMut: Mutations = null;
	spanMut: Mutations = null;
}

export interface ColumnizatorOptions extends FragmentorOptions {
	parentAreas?: Areas;
	columnCount?: number;
	parentLayoutContext?: LayoutContext;
}

export interface ColumnizatorEvents extends FragmentorEvents {
	'column-start': { column: Column };
	'column-end': { column: Column };
	'columnization-start': Record<string, never>;
	'columnization-end': Record<string, never>;
}

export class ColumnsLayoutContext extends LayoutContext {
	maxHeightChanged = false;

	constructor(source: HTMLElement, root: HTMLElement, areas: Areas, parentCtx?: LayoutContext) {
		super(root, root, areas, parentCtx);

		if (parentCtx) parentCtx.on('body-bottom-change', () => {
			this.maxHeightChanged = true;
		});
	}
}

export class Columnizator extends Fragmentor<ColumnizatorOptions, ColumnizatorEvents> {
	static defaultOptions: ColumnizatorOptions = io.mergeOptions({}, Fragmentor.defaultOptions as ColumnizatorOptions, {
		parentAreas: {},
		parentLayoutContext: null,
	});

	avoidBreakTypes = ['avoid', 'avoid-column'];
	forcedBreakTypes: ForcedBreakType[] = ['always', 'column', '-ps-column-span'];
	layoutContext: ColumnsLayoutContext = null;

	columnizationMut: Mutations = new Mutations();
	breakPoint: Point | false | null = null;

	parentAvoidBreakTypes: string[];

	columnCount: number;
	fillProp: string;

	sourceBody: HTMLElement;
	isLastGroup: boolean;

	_maxBottom: number;

	logNoValidBreakPoint = false;

	constructor(source: HTMLElement, options?: ColumnizatorOptions) {
		super(source, options);
		this.addSubProcessor(columnsSubProcessor);
		this._process = this.columnizationProcess();
	}

	/** Returns the max height defined on the element style or Infinity */
	columnsMaxHeight(top: number): number {
		if (!this._maxBottom) {
			const style = getComputedStyle(this.source);
			let maxHeight = computeMinOrMaxLength(style.maxHeight, this.source.parentElement.clientHeight, Infinity);
			if (isNaN(maxHeight)) maxHeight = Infinity;
			if (maxHeight == Infinity) this._maxBottom = Infinity;
			else {
				const sourceTop = rects.boundingScrollRect(this.source).top;
				this._maxBottom = sourceTop + maxHeight;
			}
		}
		// -0.001 to avoid some rounding error on Firefox (87)
		return this._maxBottom - top - 0.001;
	}

	* columnizationProcess(): Process {
		// Does a column count exist?
		const customStyle = getCustomStyle(this.source);
		this.columnCount = this.options.columnCount || parseColumnCount(this.source, customStyle);
		if (!this.columnCount) throw new Error("No columns count defined on the source element.");

		// Start of the column processes
		this.columnizationMut.setAttr(this.source, 'ps-process', 'pending');
		if (this.listenerCount('columnization-start')) yield 'columnization-start';
		this.columnizationMut.setAttr(this.source, 'ps-columns', '');


		// Move of all the contents in an hidden body
		this.sourceBody = document.createElement('ps-column-body');
		ranges.appendNodeContents(this.source, this.sourceBody);
		this.sourceBody.style.display = 'none';
		this.source.appendChild(this.sourceBody);

		this.columnizationMut.push({
			source: this.source,
			sourceBody: this.sourceBody,
			revert(): void {
				ranges.appendNodeContents(this.sourceBody, this.source);
				this.sourceBody.remove();
			}
		});

		// Columns properties
		this.fillProp = getCustomProp(customStyle, 'column-fill');
		let gapProp = getCustomProp(customStyle, 'column-gap');
		if (gapProp == 'normal') gapProp = '1em';
		this.columnizationMut.setStyle(this.source, {'--ps-gap': gapProp});

		let ruleProp = getCustomProp(customStyle, 'column-rule');
		if (!ruleProp) {
			const ruleStyle = getCustomProp(customStyle, 'column-rule-style') || 'none';
			const ruleWidth = getCustomProp(customStyle, 'column-rule-width') || 'medium';
			const ruleColor = getCustomProp(customStyle, 'column-rule-color') || getComputedStyle(this.source).color;
			ruleProp = ruleStyle + ' ' + ruleWidth + ' ' + ruleColor;

		}
		// The rule is created if the style is not "none"
		if (!ruleProp.match(/(^|\s)none($|\s)?/)) this.columnizationMut.setStyle(this.source, {'--ps-rule': ruleProp});

		// Iterate and extract over the groups (pair columns + span)
		const flowIterator = new FlowIterator(this.sourceBody, this.forcedBreakTypes);
		flowIterator.columnSpanBreak = getCustomProp(customStyle, 'column-span-break') as 'box' | 'element';
		this.isLastGroup = false;
		let flow = flowIterator.next();
		if (!flow) return;
		const groups: Array<ColumnsGroup> = [];
		let group: ColumnsGroup;
		let maxSequencesReached: boolean;
		do {
			group = new ColumnsGroup();
			groups.push(group);
			maxSequencesReached = false;
			do {
				const sequenceMut = new Mutations();
				const sequence = sequenceMut.breaks(this.sourceBody, {
					container: flow.range.endContainer as Element,
					offset: flow.range.endOffset
				}, 'before');
				sequence.setAttribute('ps-column-sequence', '');
				sequence.style.display = '';

				// The span should have an unique element (on box or element break)
				if (flow.breakType == '-ps-column-span') {
					if (group.span) {
						unbreak(group.span, sequence, 'before');
					} else {
						group.spanMut = sequenceMut;
						group.span = sequence;
					}
				} else {
					if (group.columnsMut) sequenceMut.commit(group.columnsMut);
					else group.columnsMut = sequenceMut;
					group.sequences.push(sequence);
				}

				this.isLastGroup = flowIterator.remaining.collapsed;
				flow = flowIterator.next();

				// The iteration is stopped if they are more sequences than columns (+1 to include a possible span)
				maxSequencesReached = group.sequences.length >= this.columnCount + 1;
			} while (flow && !maxSequencesReached && (!group.span || flow.breakType == '-ps-column-span'));

			// Process the column group
			// this.breakPoint == false means that no valid break point is found on the last columns or last span.
			yield* this.columnsGroupProcess(group, groups.length == 1 && !this.source.hasAttribute('ps-breakead-before'));

		} while (!this.isLastGroup && this.breakPoint === null && !maxSequencesReached);

		let remainingBody: HTMLElement;

		const mutationPairs = groups.flatMap((group) => {
			const pairs: [fragment: HTMLElement, mutations: Mutations][] = [];
			if (group.columnParent) pairs.push([group.columnParent, group.columnsMut]);
			if (group.span && group.span.isConnected) pairs.push([group.span, group.spanMut]);
			return pairs;
		});

		// Put back the remaining span and sequences in the source body
		if (group.span && !group.span.isConnected) unbreak(group.span, this.sourceBody, 'after');
		for (let i = group.sequences.length - 1; i >= 0; i--) {
			if (!group.sequences[i].isConnected) unbreak(group.sequences[i], this.sourceBody, 'after');
		}

		if (this.breakPoint) {
			for (const pair of mutationPairs) pair[1].commit(this.columnizationMut);
			remainingBody = breakAtPoint(this.currentFragment.body, this.breakPoint);
			this.breakPoint = ranges.positionBefore(this.sourceBody);
			unbreak(remainingBody, this.sourceBody, 'before');
			this.columnizationMut.push({
				remainingBody,
				fragmentBody: this.currentFragment.body,
				revert(): void {
					unbreak(this.fragmentBody, this.remainingBody, 'before');
				}
			});
		} else {
			let lastPairIdx = mutationPairs.length - 1;
			if (this.breakPoint === false) mutationPairs[lastPairIdx--][1].revert();

			let validPairIdx = -1;
			for (let i = lastPairIdx; i >= 0; i--) {
				const pair = mutationPairs[i];
				if (validPairIdx == -1 && pair[0].getAttribute('ps-break-after') !== 'avoid') {
					validPairIdx = i;
					break;
				} else {
					pair[1].revert();
				}
			}

			if (validPairIdx == -1) {
				this.breakPoint = false;
			} else {
				for (let i = 0; i <= validPairIdx; i++) mutationPairs[i][1].commit(this.columnizationMut);
				this.breakPoint = this.sourceBody.firstChild ? ranges.positionBefore(this.sourceBody) : null;
			}

			remainingBody = this.sourceBody;
			remainingBody.remove();
		}

		this.columnizationMut.appendNodeContents(remainingBody, this.source);

		this.columnizationMut.setAttr(this.source, 'ps-process', 'ended');
		if (this.listenerCount('columnization-end')) yield 'columnization-end';
	}

	/**
	 * A the end of this process, the remaining bodies are replaced in the source body
	 * @param group
	 */
	* columnsGroupProcess(group: ColumnsGroup, firstGroup: boolean): Process {
		if (group.sequences.length) {
			const columns: Column[] = [];

			// Creation of the columns parent element
			const columnsParent = group.columnParent = this.doc.createElement('ps-columns');
			group.columnsMut.setAttr(columnsParent, 'ps-process', 'pending');
			this.source.insertBefore(columnsParent, this.sourceBody);

			// Creation of the column elements
			for (let i = 0; i < this.columnCount; i++) {
				const column = createColumn(this.doc, i + 1);
				if (i != 0) {
					const columnGap = document.createElement('ps-column-gap');
					if (this.source.style.getPropertyValue('--ps-rule')) columnGap.appendChild(document.createElement('ps-column-rule'));
					columnsParent.appendChild(columnGap);
				}
				columnsParent.appendChild(column.container);
				createAreas(column, getCustomProp(this.source, 'areas'));
				columns.push(column);
				this.fragments.push(column);
			}

			group.columnsMut.push({
				columnsParent,
				revert(): void {
					columnsParent.remove();
				}
			});

			const sourceStyle = getComputedStyle(this.source);
			const lineHeightProp = sourceStyle.lineHeight;
			const lineHeight = lineHeightProp == 'normal' ? parseInt(sourceStyle.fontSize) * 1.15 : parseInt(lineHeightProp);

			let spanFirstPass = group.span != null;
			let recolumnize = false;

			const top = rects.boundingScrollRect(columnsParent).top;
			let height = this.columnsMaxHeight(top);

			const columnsContentMut = new Mutations();
			do {
				const firstColumn = columns[0];
				let toBalance = spanFirstPass || this.isLastGroup && (height == Infinity || this.fillProp != 'auto');
				if (toBalance && !recolumnize) {
					let balancedHeight = lineHeight;
					for (let i = group.sequences.length - 1; i >= 0; i--) {
						const columnBody = group.sequences[i];
						firstColumn.body = firstColumn.container.insertBefore(columnBody, firstColumn.container.firstChild);
						const columnMinHeight = columnBody.scrollHeight / (this.columnCount - group.sequences.length + 1);
						if (columnMinHeight > balancedHeight) balancedHeight = columnMinHeight;
						if (i != 0) columnBody.remove();
					}
					if (balancedHeight < height) height = balancedHeight;
					else toBalance = false;
				} else {
					firstColumn.body = firstColumn.container.insertBefore(group.sequences[0], firstColumn.container.firstChild);
				}

				group.columnsMut.push({
					firstColumn,
					revert(): void {
						this.firstColumn.body.remove();
					}
				});

				columnsContentMut.push({
					columns,
					group,
					revert(): void {
						for (let i = this.columns.length - 1; i > 0; i--) {
							const column = columns[i];
							if (column.body) {
								if (i > 0 && this.group.sequences.includes(column.body)) {
									// The column is a sequence start, presents in group.sequences
									column.body.remove();
								} else {
									unbreak(columns[i - 1].body, column.body);
								}
								column.body = null;
							}
						}
					}
				});

				// At this point, the content of the first sequence is in the first column
				recolumnize = false;
				let stop = false;
				group.columnsMut.setStyle(columnsParent, {'height': height + 'px'});

				let currentSequence = 0;
				let currentColumn = 0;
				let column = columns[currentColumn];
				let firstGroupCurrent = firstGroup;
				do {
					this.currentFragment?.container?.removeAttribute('ps-processing');
					this.currentFragment = column;
					this.currentFragment.container.setAttribute('ps-processing', 'content');
					this.breakPoint = null;
					logger.addScope(`column ${column.number}`);
					this.onFragmentStart(column, firstGroupCurrent);
					if (this.listenerCount('fragment-start')) yield {type: 'fragment-start', fragment: column };
					if (this.listenerCount('column-start')) yield {type: 'column-start', column };
					firstGroupCurrent = false;
					const columnAreas = Object.assign({}, this.options.parentAreas, column.areas);
					this.layoutContext?.mutations?.commit(columnsContentMut);
					if (currentColumn == this.columnCount - 1 && isSubFragmentor(this)) this.avoidBreakTypes.push("avoid-page");
					const contentsEvent = yield* this.contentsProcess(column.body, columnAreas, isSubFragmentor(this) && this.parentFragmentor.layoutContext);
					if (currentColumn == this.columnCount - 1 && isSubFragmentor(this)) this.avoidBreakTypes.pop();

					const newMaxHeight = this.columnsMaxHeight(top);
					if (this.layoutContext.maxHeightChanged && height > newMaxHeight) {
						// New max height
						height = newMaxHeight;
						toBalance = false;
						recolumnize = true;
						logger.removeScope();
						break;
					} else if (toBalance) {
						let increaseHeight = false;
						let minHeight = 0;
						const {boxes} = this.layoutContext;
						if (contentsEvent == 'no-break-point') {
							increaseHeight = true;
							// Optimization: set a minimum height to the first box height if it is unbreakable
							if (boxes.length == 1 && boxes[0].unbreakable) minHeight = boxes[0].bottom - boxes[0].top;
						}
						if (!increaseHeight && contentsEvent == "valid-break-point") {
							const lastColumnBody = columns.at(-1).body;
							increaseHeight = lastColumnBody && lastColumnBody.scrollHeight >= lastColumnBody.clientHeight;
						}
						if (increaseHeight) {
							let newHeight = height + lineHeight;
							if (newHeight < minHeight) newHeight = minHeight;
							if (newHeight < newMaxHeight) {
								height = newHeight;
								recolumnize = true;
								logger.removeScope();
								break;
							}
						}
						if (contentsEvent == "valid-break-point") this.breakPoint = this.layoutContext.breakPoint;
						else if (contentsEvent == 'no-break-point') this.breakPoint = false;
					} else {
						if (contentsEvent == 'no-break-point') {
							this.layoutContext.mutations.revert();
							this.currentFragMut.revert();
							columnsContentMut.revert();
							this.breakPoint = false;
							logger.removeScope();
							break;
						} else if (contentsEvent == 'valid-break-point') {
							this.breakPoint = this.layoutContext.breakPoint;
						}
					}

					logger.removeScope();

					currentColumn++;
					// All the column have contents
					if (currentColumn == columns.length) {
						stop = true;
					} else {
						if (this.breakPoint) {
							// Not breaked in a mutation, the revert is handled by columnsContentMut
							const remainingBody = breakAtPoint(column.body, this.breakPoint);
							this.onFragmentBreak(column);
							if (this.listenerCount('fragment-end')) yield {type: 'fragment-end', fragment: column };
							if (this.listenerCount('column-end')) yield {type: 'column-end', column };
							column = columns[currentColumn];
							column.body = column.container.insertBefore(remainingBody, column.container.firstChild);
							this.breakPoint = null;
						} else {
							currentSequence++;
							firstGroupCurrent = firstGroup;
							if (currentSequence < group.sequences.length) {
								column = columns[currentColumn];
								column.body = column.container.insertBefore(group.sequences[currentSequence], column.container.firstChild);
							} else {
								// Forced height is changed to max-height to fit the content while avoiding overflow on the page
								columnsParent.style.height = '';
								group.columnsMut.setStyle(columnsParent, {'max-height': height + 'px'});
								stop = true;
							}
						}
					}
				} while (!stop);
				if (this.listenerCount('fragment-end')) yield {type: 'fragment-end', fragment: column };
				if (this.listenerCount('column-end')) yield {type: 'column-end', column };

				if (!recolumnize && spanFirstPass && this.breakPoint === null) {
					this.currentFragMut.commit(columnsContentMut);
					this.layoutContext.mutations.commit(columnsContentMut);
					columnsContentMut.commit(group.columnsMut);
					group.columnsMut.setAttr(columnsParent, 'ps-process', 'ended');

					const contentsEvent = yield* this.columnSpanProcess(group);
					if (contentsEvent == 'no-break-point') {
						// TODO récupérer la liste des floats
						this.layoutContext.mutations.revert();
						this.currentFragMut.revert();
						this.breakPoint = false;
					} else if (contentsEvent == 'valid-break-point') {
						this.currentFragMut.commit(columnsContentMut);
						this.layoutContext.mutations.commit(group.spanMut);
						this.breakPoint = this.layoutContext.breakPoint;
					}
					spanFirstPass = false;

				} else if (recolumnize) {
					// The content of the columns are merged in the first column
					this.layoutContext.mutations.revert();
					this.currentFragMut.revert();
					columnsContentMut.revert();
					this.breakPoint = null;
				} else if (this.breakPoint !== false) {
					this.currentFragMut.commit(columnsContentMut);
					this.layoutContext.mutations.commit(columnsContentMut);
				}
			} while (recolumnize);
			// TODO Duplicated code when the group has a span
			columnsContentMut.commit(group.columnsMut);
			group.columnsMut.setAttr(columnsParent, 'ps-process', 'ended');

			const lastColumnBody = columns.at(-1).body;
			if (lastColumnBody?.hasAttribute('ps-breaked-after') && getCustomProp(this.source, 'box-remaining-extent') == 'fill') {
				columnsParent.style.height = this.columnsMaxHeight(rects.boundingScrollRect(this.source).top) + "px";
			} else {
				columnsParent.style.height = '';
			}
		} else {
			const contentsEvent = yield* this.columnSpanProcess(group);
			if (contentsEvent == 'no-break-point') {
				this.layoutContext.mutations.revert();
				this.breakPoint = false;
			} else if (contentsEvent == 'valid-break-point') {
				this.layoutContext.mutations.commit(group.spanMut);
				this.breakPoint = this.layoutContext.breakPoint;
			}
		}
	}

	* columnSpanProcess(group: ColumnsGroup): Process {
		logger.addScope(`column-span`);
		const spanFragment = this.currentFragment = new ColumnSpan();
		spanFragment.body = group.span;
		this.source.insertBefore(spanFragment.body, this.sourceBody);
		const top = rects.boundingScrollRect(spanFragment.body).top;
		spanFragment.body.style.height = this.columnsMaxHeight(top) + 'px';
		if (isSubFragmentor(this)) this.avoidBreakTypes.push("avoid-page");
		const contentsEvent = yield* this.contentsProcess(spanFragment.body, this.options.parentAreas, isSubFragmentor(this) && this.parentFragmentor.layoutContext);
		if (isSubFragmentor(this)) this.avoidBreakTypes.pop();
		spanFragment.body.style.height = '';
		// TODO More robust and generic way to support page break rules inside columns
		if (this.parentAvoidBreakTypes) {
			let lastChild: Node = spanFragment.body;
			do {
				if (!walk.isElement(lastChild)) break;
				if (this.parentAvoidBreakTypes.includes(getCustomProp(lastChild, 'break-after'))) {
					group.spanMut.setAttr(spanFragment.body, 'ps-break-after', 'avoid');
					break;
				}
				lastChild = walk.lastChild(lastChild, walk.isStaticNode);
			} while (lastChild && walk.isElement(lastChild));
		}
		logger.removeScope();
		return contentsEvent;
	}

	createLayoutContext(root: HTMLElement, areas: Areas, parentCtx: LayoutContext): LayoutContext {
		return new ColumnsLayoutContext(this.source, root, areas, parentCtx);
	}
}

export function createColumn(doc: Document, number: number): Column {
	const column = new Column;
	column.container = doc.createElement('ps-column');
	column.number = number;
	return column;
}

export const columnsSubProcessor: SubProcessor = function (this: Fragmentor, ctx: SubProcessorContext): SubProcessorResult {
	// TODO check that the source is not the same as the pagination (conflict on the ps-process attribute)
	const columnCount = parseColumnCount(ctx.currentElement, ctx.currentCustomStyle);
	if (!columnCount) return false;

	class SubColumnizator extends Columnizator implements SubFragmentor {

		constructor(readonly parentFragmentor: Fragmentor<any>, source: HTMLElement, options?: ColumnizatorOptions) {
			super(source, options);
			this.gcpmContext = parentFragmentor.gcpmContext;
			this.subProcessors = Array.from(parentFragmentor.subProcessors);
			this.parentEmitter = parentFragmentor;
		}

		/*** Returns the max height constrained by the parent fragment and the element style */
		columnsMaxHeight(top: number): number {
			const bottomOffset = this.parentFragmentor.layoutContext.bottomOffsets.get(this.source).after;
			let fragmentMaxHeight = this.parentFragmentor.layoutContext.bodyBottom - bottomOffset - top;
			if (ctx.currentStyle.boxSizing == 'content-box') fragmentMaxHeight = rects.contentSizingHeight(ctx.currentStyle, fragmentMaxHeight);
			return Math.min(super.columnsMaxHeight(top), fragmentMaxHeight);
		}
	}

	const columnizator = new SubColumnizator(this, ctx.currentElement, Object.assign(Object.create(Fragmentor.defaultOptions), {
		parentAreas: this.layoutContext.areas,
		columnCount,
		logLevel: this.options.logLevel
	}));

	columnizator.parentAvoidBreakTypes = this.avoidBreakTypes;

	return columnizator.start().ended.then(() => {
		columnizator.columnizationMut.commit(ctx.currentBox.mutations);
		if (columnizator.breakPoint) return {event: 'valid-break-point', breakPoint: columnizator.breakPoint};
		else if (columnizator.breakPoint === false) return {event: 'no-break-point'};
		else return {event: 'no-overflow'};
	});
};

function parseColumnCount(element: HTMLElement, customStyle: CSSStyleDeclaration): number {
	let count = 0;
	const countProp = getCustomProp(customStyle, 'column-count');
	if (countProp != "auto") count = parseInt(countProp);
	else {
		const widthProp = getCustomProp(customStyle, 'column-width');
		const elemWidth = element.clientWidth;
		if (widthProp != 'auto') {
			const width = computeLength(widthProp, elemWidth);
			let gapProp = getCustomProp(customStyle, 'column-gap');
			if (gapProp == 'normal') gapProp = '1em';
			const gap = computeLength(gapProp, elemWidth);

			count = Math.floor(elemWidth / width);
			if (count > 1) while (count > 1 && count * width + (gap * (count - 1)) > elemWidth) count--;
		}
	}
	if (isNaN(count) || count == 1) return 0;
	return count;
}
