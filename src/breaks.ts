import {walk} from "./util.js";
import * as counters from "./counters.js";
import {getCustomProp, getCustomStyle} from "./css.js";
import type {Point} from "./util/ranges";
import type {CountersStack} from "./counters.js";

export type Position = "before" | "after";
export type Unbreakable =
	'float'
	| 'intrisicDimension'
	| 'horizontalLayout'
	| 'replacedElement'
	| 'subFragmentor'
	| false;

const REPLACED_ELEMENTS = ['img', 'object', 'video', 'audio', 'canvas', 'iframe', 'textarea', 'input', 'button'];

export interface Breaker<E extends HTMLElement = HTMLElement> {
	matches: (elem: HTMLElement) => elem is E;
	break: (before: E, after: E) => void;
	unbreak: (before: E, after: E) => void;
}

export const breakers: Breaker[] = [];

export function breakAtPoint<T extends HTMLElement>(source: T, breakPoint: Point, side: Position = 'after'): T {
	const textBreak = breakPoint.container.nodeType == Node.TEXT_NODE;
	const breaking = textBreak ? breakPoint.container.parentElement : breakPoint.container as Element;
	const inlineBreak = textBreak || getComputedStyle(breaking).display == "inline";
	const breakingOnSource = breaking == source;

	// An attribute is set to find the breaking element after the extraction of contents
	breaking.setAttribute('ps-breaking', '');

	const prevCounters = counters.parseCounters(source, breakPoint);

	const contentsRange = source.ownerDocument.createRange();
	if (side == "before") {
		contentsRange.setStartBefore(source);
		contentsRange.setEnd(breakPoint.container, breakPoint.offset);
	} else {
		contentsRange.setStart(breakPoint.container, breakPoint.offset);
		contentsRange.setEndAfter(source);
	}
	const contents = contentsRange.extractContents();
	const dest = contents.firstElementChild as T;

	let sourceBreaking = breakingOnSource ? source : source.querySelector('[' + 'ps-breaking' + ']') as HTMLElement;
	let destBreaking = breakingOnSource ? dest : dest.querySelector('[' + 'ps-breaking' + ']') as HTMLElement;

	sourceBreaking.removeAttribute('ps-breaking');
	destBreaking.removeAttribute('ps-breaking');

	let firstAncestorBox = false;
	while (destBreaking != dest.parentElement) {
		let beforeElt, afterElt;
		if (side == "after") {
			beforeElt = sourceBreaking;
			afterElt = destBreaking;
		} else {
			beforeElt = destBreaking;
			afterElt = sourceBreaking;
		}

		beforeElt.setAttribute('ps-breaked-after', '');
		afterElt.setAttribute('ps-breaked-before', '');
		if (prevCounters) writeCounters(prevCounters, beforeElt, afterElt);

		for (const breaker of breakers) {
			if (breaker.matches(destBreaking)) {
				breaker.break(beforeElt, afterElt);
				break;
			}
		}

		if (textBreak) {
			const textNode = breakPoint.container;
			/* Force an hyphen if the break is on one */
			// TODO check that the 'hyphens' property != none
			if (textNode.nodeValue.endsWith('\u00AD')) {
				const style = getComputedStyle(textNode.parentElement);
				if (style.getPropertyValue('--ps-hyphens').trim() == "auto") {
					const hyphenChar = style.getPropertyValue("-webkit-hyphenate-character")[1] || '\u2010';
					textNode.nodeValue = textNode.nodeValue.slice(0, -1) + hyphenChar;
					textNode.parentElement.setAttribute('ps-hyphen-last', '');
				}
			}
		}
		if (inlineBreak && !firstAncestorBox && walk.displayAsBlock(beforeElt)) {
			beforeElt.style.textAlignLast = beforeElt.style.MozTextAlignLast = getComputedStyle(beforeElt).textAlign;
			firstAncestorBox = true;
		}

		sourceBreaking = sourceBreaking.parentElement;
		destBreaking = destBreaking.parentElement;
	}

	return dest;
}

export function writeCounters(counters: CountersStack, prevElt: HTMLElement, nextElt: HTMLElement): void {
	let counterReset = '';
	let counterResetOnDesc = '';
	for (const counterName in counters) {
		const counterScopes = counters[counterName];
		for (let i = counterScopes.length - 1; i >= 0; i--) {
			const counterScope = counterScopes[i];
			const counterParent = counterScope.element.parentElement;
			if (counterScope.elemScope) {
				if (counterParent == prevElt.parentNode || counterParent == nextElt.parentNode) {
					counterResetOnDesc += ' ' + counterName + ' ' + counterScope.value;
					counterScopes.splice(i, 1);
				}
			} else {
				if ((counterParent == prevElt && !counterScope.element.hasAttribute('ps-breaked-after')) || (counterParent == nextElt && !counterScope.element.hasAttribute('ps-breaked-before'))) {
					counterResetOnDesc += ' ' + counterName + ' ' + counterScope.value;
					counterScopes.splice(i, 1);
				}
				if (counterParent == prevElt.parentNode || counterParent == nextElt.parentNode) {
					counterReset += ' ' + counterName + ' ' + counterScope.value;
					counterScopes.splice(i, 1);
					break;
				}
			}
		}
	}
	if (counterResetOnDesc) {
		nextElt.setAttribute('ps-counters-on-desc', '');
		nextElt.style.setProperty('--ps-counters-on-desc', counterResetOnDesc);
	} else {
		nextElt.removeAttribute('ps-counters-on-desc');
	}
	if (counterReset) {
		nextElt.setAttribute('ps-counters', '');
		nextElt.style.counterReset = counterReset;
	}
}

// TODO removeBreakedAttr: keep track of the breaked element before / after in a symbol instead?
export function unbreak(before: HTMLElement, after: HTMLElement, position: Position = 'before'): void {
	before.removeAttribute('ps-breaked-after');
	after.removeAttribute('ps-breaked-before');

	for (const breaker of breakers) {
		if (breaker.matches(before)) {
			breaker.unbreak(before, after);
			break;
		}
	}

	if (before.style.textAlignLast) {
		before.style.textAlignLast = before.style.MozTextAlignLast = '';
		if (!before.getAttribute('style')) before.removeAttribute('style');
	}

	if (after.hasAttribute('ps-counters')) {
		after.style.counterReset = '';
		after.removeAttribute('ps-counters');
	}

	if (after.hasAttribute('ps-counters-on-desc')) {
		after.style.removeProperty('--ps-counters-on-desc');
		after.removeAttribute('ps-counters-on-desc');
	}

	let childBefore: HTMLElement, childAfter: HTMLElement;
	if (walk.isHTMLElement(before.lastChild) && before.lastChild.hasAttribute('ps-breaked-after')) {
		childBefore = before.lastChild;
		childAfter = after.firstChild as HTMLElement;
	}

	// Maybe a text break
	if (!childBefore) {
		if (before.hasAttribute('ps-hyphen-last')) {
			before.lastChild.nodeValue = before.lastChild.nodeValue.slice(0, -1) + '\u00AD';
			before.removeAttribute('ps-hyphen-last');
		}
	}

	if (position == 'before') {
		if (after.hasAttribute('ps-breaked-after')) before.setAttribute('ps-breaked-after', '');
		//for (const attr of after.attributes) if (attr.name != 'style') before.setAttribute(attr.name, attr.value);
		while (after.firstChild) before.appendChild(after.firstChild);
		after.remove();
		before.normalize();
	} else {
		// Copy of the attributes of the before when the unbreak lands on the after
		if (before.hasAttribute('ps-breaked-before')) after.setAttribute('ps-breaked-before', '');
		if (before.hasAttribute('ps-counters')) {
			after.setAttribute('ps-counters', '');
			after.style.counterReset = before.style.counterReset;
		}
		if (before.hasAttribute('start')) after.setAttribute('start', before.getAttribute('start'));
		// TODO Should we copy all the attributes ?
		// for (const attr of before.attributes) if (attr.name != 'style') after.setAttribute(attr.name, attr.value);
		while (before.lastChild) after.insertBefore(before.lastChild, after.firstChild);
		before.remove();
		after.normalize();
	}

	if (childBefore && childAfter) unbreak(childBefore, childAfter, position);
}

const orderedListBreaker: Breaker<HTMLOListElement> = {
	matches(elem): elem is HTMLOListElement {
		return elem.localName == "ol";
	},
	break(before, after): void {
		const listItems = Array.prototype.filter.call(before.children, (child: Element) => {
			return getComputedStyle(child).display == 'list-item';
		});
		let beforeStart = before.hasAttribute('start') ? parseInt(before.getAttribute('start')) : 1;
		// Substract one if the first child is breaked
		if (after.firstElementChild && after.firstElementChild.hasAttribute('ps-breaked-before')) beforeStart--;
		after.setAttribute('start', beforeStart + listItems.length);
	},
	unbreak(before, after) {
		after.removeAttribute('start');
	}
};

const tableBreaker: Breaker<HTMLTableElement> = {
	matches: walk.isHTMLTable,
	break(before, after): void {
		const colgroup = before.getElementsByTagName('colgroup').item(0);
		if (colgroup) after.insertBefore(colgroup.cloneNode(true), after.firstChild);

		const customStyle = getCustomStyle(before);
		if (!after.tHead && after.tBodies.length && before.tHead && getCustomProp(customStyle, 'repeat-thead') != 'no-repeat') {
			after.tHead = before.tHead.cloneNode(true);
			after.tHead.setAttribute('ps-repeated', '');
		}
		/* It is not possible to repeat a footer or the bottom caption:  the elements must be known before a page break can occur. */
	},
	unbreak(before, after) {
		const afterColgroups = after.querySelectorAll('colgroup');
		for (let i = 1; i < afterColgroups.length; i++) afterColgroups[i].remove();
		if (after.tHead && after.tHead.hasAttribute('ps-repeated')) after.tHead.remove();
	}
};

breakers.push(orderedListBreaker);
breakers.push(tableBreaker);

// Only the intrinsic dimension specified in the element's style is taken into account.
export function unbreakableElement(element: Element): Unbreakable {
	if (!walk.isHTMLElement(element)) return 'replacedElement';
	const style = getComputedStyle(element);
	if (style.float != 'none') return 'float';
	if (element.style.height || element.style.minHeight) return 'intrisicDimension';
	if (REPLACED_ELEMENTS.includes(element.localName) && walk.displayAsBlock(element)) return 'replacedElement';
}
