import valueParser from 'postcss-value-parser';
import {dom, logger, rects, str, walk} from "./util.js";
import {getCustomProp, getCustomStyle} from "./css.js";
import {getCounterValue, getPageCounterValue, parseCounterValue} from "./counters.js";
import type {FunctionNode, Node, StringNode} from 'postcss-value-parser';

import type {Fragment} from "./fragments";
import type {Page} from "./pages";
import type {Float, FloatAlignment} from "./layout";

export const SIDES = ['top', 'right', 'bottom', 'left'];
export const CORNERS = ['top-left-corner', 'top-right-corner', 'bottom-right-corner', 'bottom-left-corner'];
export const HORIZONTAL_MARGIN_BOXES = ['left', 'center', 'right'];
export const VERTICAL_MARGIN_BOXES = ['top', 'middle', 'bottom'];

export type Pseudo = "before" | "after" | "marker";
export const PSEUDOS: Pseudo[] = ['before', 'after', 'marker'];

interface ValueFnContext {
	element: HTMLElement;
	srcElement: HTMLElement;
	ctx: GCPMContext;
	pseudo?: Pseudo;
	destElt?: Element;
	varName?: string;
}

type ValueFn = (args: Node[], context: ValueFnContext) => string | Node | Node[];

class InvalidArgCountError extends TypeError {
	constructor(fnName: string, args: Node[], argsRequired: number) {
		super(`Failed to parse '${fnName}(${valueParser.stringify(args)}): ${argsRequired} argument${argsRequired > 1 ? 's' : ''} required.`);
	}
}

type MarginContent<C> = { [key: string]: C };

export class PageMarginContent<C> {
	readonly start: MarginContent<C> = {};
	first: MarginContent<C> = {};
	last: MarginContent<C> = {};
}

interface Leader {
	pseudo: string;
	element: HTMLElement;
	pattern: string;
}

interface TargetCounter {
	element: HTMLElement;
	varName: string;
	style?: string;
}

export class GCPMContext {
	rawPageCounters: boolean;
	/**
	 * Map of floats by float-call
	 */
	floats = new Map<Element, Float>();

	/**
	 * Map of floats by float-call
	 */
	pendingFloats: Float[] = [];

	/**
	 * Map of leaders.
	 */
	leaders = new Map<HTMLElement, Leader>();

	/**
	 * The global string sets of the pagination.
	 */
	stringSets: MarginContent<string> = {};

	/**
	 * The string sets of the current page.
	 */
	pageStringSets = new PageMarginContent<string>();

	/**
	 * The global running elements of the pagination.
	 */
	runnings: MarginContent<HTMLElement> = {};

	/**
	 * The running elements of the current page.
	 */
	pageRunnings = new PageMarginContent<HTMLElement>();

	bookmarks: Set<HTMLElement> = new Set();

	targetCounters: { [id: string]: { [counterName: string]: TargetCounter[] } } = {};

	targetsCounters: TargetCounter[];

	disconnectedDocFrag: DocumentFragment;

	constructor(disconnectedDocFrag: DocumentFragment) {
		this.disconnectedDocFrag = disconnectedDocFrag;
	}
}

export function prepareGcpm(element: Element, customStyle: CSSStyleDeclaration, ctx: GCPMContext): void {
	if (walk.isHTMLElement(element)) {
		const boxDecorationBreak = getCustomProp(customStyle, "box-decoration-break");
		if (boxDecorationBreak == 'clone') {
			element.setAttribute('ps-box-decoration-break', 'clone');
		}

		const marginBreak = getCustomProp(customStyle, "margin-break");
		if (marginBreak.endsWith('keep')) {
			element.setAttribute('ps-margin-break-after', 'keep');
		}


		const bookmarkLevel = getCustomProp(customStyle, "bookmark-level");
		if (bookmarkLevel != 'none') {
			if (bookmarkLevel) ctx.bookmarks.add(element);
		}

		const position = getCustomProp(customStyle, "position");
		if (position.includes('(')) {
			const positionVal = valueParser(position).nodes[0];
			if (positionVal.type == 'function' && positionVal.value == 'running') {
				element.setAttribute('ps-running', positionVal.nodes[0].value);
			}
		}

		for (const pseudo of PSEUDOS) parsePseudosVals(element, pseudo, ctx);
	}

	if (walk.isHTMLOrSVGRootSVGElement(element)) {
		const float = getCustomProp(customStyle, 'float');
		if (float) {
			const floatVal = valueParser(float).nodes[0];
			if (floatVal.type == 'function' && floatVal.value == "-ps-area") {
				const doc = element.ownerDocument;
				const areaName = floatVal.nodes[0].value;
				const call = doc.createElement('a');
				call.setAttribute('ps-float-call', '');
				const body = element.cloneNode(true) as HTMLElement | SVGSVGElement;
				body.setAttribute('ps-float', areaName);
				body.setAttribute('ps-float-display', getCustomProp(customStyle, 'float-display'));
				element.removeAttribute('id');
				element.setAttribute('ps-float-base', '');
				element.parentNode.insertBefore(call, element.nextSibling);

				call.setAttribute('href', '#' + dom.id(body));
				call.setAttribute('ps-float-area', areaName);

				const alignment = (floatVal.nodes.length > 2 ? floatVal.nodes[2].value : "auto") as FloatAlignment;
				ctx.floats.set(element, {
					base: element,
					body,
					call,
					areaName,
					alignment
				});
			}
		}
	}
}

export function parsePseudosVals(element: HTMLElement, pseudo: Pseudo, ctx: GCPMContext): void {
	const vals = getComputedStyle(element, '::' + pseudo).getPropertyValue(`--ps-${pseudo}-vals`).trim();
	if (vals) {
		const pseudoValues = valueParser(vals).nodes.filter((val) => val.type == 'function');
		for (let i = 0; i < pseudoValues.length; i++) {
			const varName = `--ps-${pseudo}-val-${i}`;
			// In some case (leaders), the value can be already parsed
			if (!element.style.getPropertyValue(varName)) {
				const vals = [pseudoValues[i]];
				parseContentVals(vals, {element, pseudo, varName, srcElement: element, ctx});
				element.style.setProperty(varName, valueParser.stringify(vals));
			}
		}
	}
}

function parseContentVals(vals: Node[], valFnCtx: ValueFnContext): void {
	valueParser.walk(vals, (val: Node, index: number, vals: Node[]) => {
		if (val.type == 'function') {
			let valFn: ValueFn;
			const args = val.nodes.filter((val) => val.type != 'div');
			if (val.value == 'attr') valFn = fnAttr;
			else if (val.value == 'url') valFn = fnUrl;
			else if (val.value == 'string') valFn = fnString;
			else if (val.value == 'element') valFn = fnElement;
			else if (val.value == 'leader') valFn = fnLeader;
			else if (val.value == 'target-content' || val.value == 'target-text') valFn = fnTargetContent;
			else if (val.value == 'target-counter') valFn = fnTargetCounter;
			else if (val.value == '-ps-targets-counter') valFn = fnTargetsCounter;
			else if (val.value == '-ps-target-attr') valFn = fnTargetAttr;
			if (valFn) {
				const value = valFn(args, valFnCtx);
				if (typeof value == 'string') vals[index] = stringNode(str.cssEscape(value));
				else if (Array.isArray(value)) vals[index] = {nodes: value} as Node;
				else vals[index] = value;
			}
		}
	}, true);
}

function stringSetToContentVals(vals: Node[], valFnCtx: ValueFnContext): void {
	valueParser.walk(vals, (val: Node, index: number, vals: Node[]) => {
		if (val.type == 'function') {
			let valFn: ValueFn;
			const args = val.nodes.filter((val) => val.type != 'div');
			if (val.value == 'attr') valFn = fnAttr;
			else if (val.value == 'url') valFn = fnUrl;
			else if (val.value == 'counter' || val.value == 'content') {
				val.value = `target-${val.value}`;
				val.nodes.unshift(stringNode('#' + dom.id(valFnCtx.element)), {
					type: "div",
					value: ','
				} as Node);
			}
			if (valFn) {
				const value = valFn(args, valFnCtx);
				if (typeof value == 'string') vals[index] = stringNode(value);
				else if (Array.isArray(value)) vals[index] = {nodes: value} as Node;
			}
		}
	}, true);
}

function fnAttr(args: Node[], {srcElement}: ValueFnContext): string {
	if (args.length < 1) throw new InvalidArgCountError('attr', args, 1);
	const attrName = args[0].value;
	if (!srcElement.hasAttribute(attrName)) {
		logger.warn(`Invalid attr() content function call : the attribute '${attrName}' does not exist.`);
		return '';
	}
	return srcElement.getAttribute(attrName);
}

function fnTargetAttr(args: Node[], {srcElement}: ValueFnContext): string {
	if (args.length < 2) throw new InvalidArgCountError('target-attr', args, 2);
	const targetId = checkTarget(args[0].value);
	if (!targetId) return '';
	const targetElement = srcElement.ownerDocument.getElementById(targetId);

	if (!targetElement) return '';
	const attrName = args[1].value;

	if (!targetElement.hasAttribute(attrName)) {
		logger.warn(`Invalid target-attr() content function call : the attribute '${attrName}' does not exist on element '#${targetId}'.`);
		return '';
	}
	return targetElement.getAttribute(attrName);
}

function fnUrl(args: Node[]): string {
	if (args.length < 1) throw new InvalidArgCountError('url', args, 1);
	return args[0].value;
}

function fnString(args: Node[], {srcElement, ctx}: ValueFnContext): string | Node[] {
	if (args.length < 1) throw new InvalidArgCountError('string', args, 1);
	const name = args[0].value;
	const position = args.length > 1 ? args[1].value : 'first';
	let stringContent = '';

	if (position == 'first') stringContent = ctx.pageStringSets.first[name] || ctx.stringSets[name] || '';
	else if (position == "start") stringContent = ctx.pageStringSets.start[name] || ctx.stringSets[name] || '';
	else if (position == 'last') stringContent = ctx.pageStringSets.last[name] || ctx.stringSets[name] || '';
	else if (position == "first-except") stringContent = ctx.pageStringSets.first[name] ? '' : ctx.stringSets[name] || '';

	const stringVals = valueParser(stringContent).nodes;
	parseContentVals(stringVals, {element: srcElement, srcElement, ctx});
	return stringVals;
}

function fnElement(args: Node[], {srcElement, ctx}: ValueFnContext): string {
	if (args.length < 1) throw new InvalidArgCountError('element', args, 1);
	const name = args[0].value;
	const position = args.length > 1 ? args[1].value : 'first';

	let running: Element;
	if (position == 'first') running = ctx.pageRunnings.first[name] || ctx.runnings[name];
	else if (position == "start") running = ctx.pageRunnings.start[name] || ctx.runnings[name];
	else if (position == 'last') running = ctx.pageRunnings.last[name] || ctx.runnings[name];
	else if (position == "first-except") running = ctx.pageRunnings.first[name] ? null : ctx.runnings[name];

	if (running) {
		const clone = srcElement.appendChild(running.cloneNode(true)) as Element;
		clone.removeAttribute('ps-running');
	}
	return '';
}

function fnLeader(args: Node[], {element, pseudo, ctx}: ValueFnContext): string {
	if (args.length < 1) throw new InvalidArgCountError('leader', args, 1);
	const patternVal = args[0];
	let pattern: string;
	if (patternVal.type == 'word') {
		if (patternVal.value == 'dotted') pattern = '. ';
		else if (patternVal.value == 'solid') pattern = '_';
		else if (patternVal.value == 'space') pattern = ' ';
	} else if (patternVal.type == 'string') {
		pattern = str.cssUnescape(patternVal.value);
	}

	const leader: Leader = {
		pseudo,
		pattern,
		element: element.ownerDocument.createElement('ps-leader')
	};
	if (pseudo == 'after') element.appendChild(leader.element);
	else element.insertBefore(leader.element, element.firstChild);

	leader.element.textContent = leader.pattern;
	leader.element.setAttribute('ps-position', pseudo);
	leader.element.setAttribute('ps-direction', getComputedStyle(element).direction == 'ltr' ? 'rtl' : 'ltr');

	ctx.leaders.set(element, leader);
	return '';
}

function fnTargetCounter(args: Node[], {element, ctx, varName}: ValueFnContext): string | Node {
	if (args.length < 2) throw new InvalidArgCountError('target-counter', args, 2);
	const targetId = checkTarget(args[0].value);
	if (!targetId) return '';
	const counterName = args[1].value;
	const counterStyle = args[2]?.value;

	let targetCounters = ctx.targetCounters[targetId];
	if (!targetCounters) targetCounters = ctx.targetCounters[targetId] = {};

	// If the parsing comes from a variable  (@see parsePseudosVals), it is used for future replacement
	let targetVarName = varName;
	if (!targetVarName) {
		let i = 0;
		// Search for an existing counter on this target to increase the index
		for (const targetId in ctx.targetCounters) {
			for (const counterName in ctx.targetCounters[targetId]) {
				for (const counter of ctx.targetCounters[targetId][counterName]) {
					if (counter.element == element) i++;
				}
			}
		}
		targetVarName = `--ps-target-counter-${i}`;
	}

	let targetCounter = targetCounters[counterName];
	if (!targetCounter) targetCounter = targetCounters[counterName] = [];

	targetCounter.push({element, varName: targetVarName, style: counterStyle});

	return varName ? "000" : valueParser(`var(${targetVarName}, "000")`).nodes[0];
}

function fnTargetsCounter(args: Node[], {element, srcElement, ctx, varName}: ValueFnContext): string | Node[] {
	if (args.length < 3) throw new InvalidArgCountError('targets-counter', args, 2);
	let targetIds = args[0].value.trim().split(' ');
	targetIds = targetIds.map((target) => checkTarget(target), false).filter((target) => target);
	if (!targetIds.length) return '';
	const counterSep = args[2].value;

	if (!ctx.targetsCounters) ctx.targetsCounters = [];

	let targetVarName = varName;
	if (!targetVarName) {
		let i = 0;
		// Search for an existing counter on this target to increase the index
		for (const targetsCounter of ctx.targetsCounters) {
			if (targetsCounter.element == element) i++;
		}
		targetVarName = `--ps-targets-counter-${i}`;
	}

	const values: Node[] = [];
	let first = true;
	for (const targetId of targetIds) {
		if (!first) values.push(stringNode(counterSep));
		else first = false;
		let targetVal = fnTargetCounter([stringNode('#' + targetId), args[1], args[3]], {element, srcElement, ctx});
		if (typeof targetVal == "string") targetVal = stringNode(targetVal);
		values.push(targetVal);
	}

	ctx.targetsCounters.push({element, varName: targetVarName});

	return values;
}


// TODO Does not match the spec: pseudo-elements of descendants are not included
function fnTargetContent(args: Node[], {srcElement, ctx}: ValueFnContext): string | Node[] {
	if (args.length < 1) throw new InvalidArgCountError('target-content', args, 1);
	const targetId = checkTarget(args[0].value);
	if (!targetId) return '';
	let targetElement = srcElement.ownerDocument.getElementById(targetId);
	if (!targetElement) targetElement = ctx.disconnectedDocFrag.getElementById(targetId);

	if (!targetElement) return '';

	const type = args.length > 1 ? args[1].value : 'text';

	if (type == 'text') {
		return targetElement.textContent.trim();
	} else if (type == "first-letter") {
		// TODO Punctuation (i.e, characters defined in Unicode in the "open" (Ps), "close" (Pe), "initial" (Pi),
		// "final" (Pf) and "other" (Po) punctuation classes),that precedes or follows the first letter should be included.
		return targetElement.textContent.trim().substr(0, 1);
	} else if (type == 'before' || type == 'after' || type == 'marker') {
		const pseudoContent = getComputedStyle(targetElement, type).content;
		if (pseudoContent == "none") return '';
		const vals = valueParser(pseudoContent).nodes;
		stringSetToContentVals(vals, {element: targetElement, srcElement: targetElement, ctx});
		parseContentVals(vals, {element: srcElement, srcElement: targetElement, ctx});
		return vals;
	}
}

export function createBookmarks(doc: Document, ctx: GCPMContext): Element {
	const root = doc.createElement('ps-bookmarks');
	root.setAttribute('role', 'navigation');
	root.hidden = true;
	const rootList = doc.createElement('ol');
	const stack = [rootList];

	for (const target of ctx.bookmarks) {
		const customStyle = getCustomStyle(target);

		const level = parseInt(getCustomProp(customStyle, 'bookmark-level'));
		if (!isNaN(level) && walk.hasSize(target)) {
			const label = getCustomProp(customStyle, 'bookmark-label');
			const labelVals = valueParser(label).nodes;

			const item = doc.createElement('li');
			const link = item.appendChild(doc.createElement('a'));
			dom.id(link);
			link.href = '#' + dom.id(target);

			stringSetToContentVals(labelVals, {element: target, srcElement: target, ctx});
			parseContentVals(labelVals, {element: link, srcElement: link, ctx});
			const labelValue = valueParser.stringify(labelVals);
			link.style.setProperty('--ps-label', labelValue);

			const list = doc.createElement('ol');
			list.setAttribute('ps-state', getCustomProp(customStyle, 'bookmark-state'));
			item.appendChild(list);

			let parentList;
			for (let l = 0; l < level; l++) {
				if (stack[l]) parentList = stack[l];
				else stack.push(undefined);
			}
			parentList.appendChild(item);

			for (let l = level; l < stack.length; l++) {
				const list = stack[l];
				if (list && !list.firstChild) list.remove();
			}
			stack.length = level;
			stack.push(list);
		}
	}

	for (const list of stack) {
		if (list && !list.firstChild) list.remove();
	}


	if (rootList.firstChild) root.appendChild(rootList);

	return root;
}

function labelToContent(link: HTMLElement): void {
	const labelVals = valueParser(getComputedStyle(link).getPropertyValue('--ps-label')).nodes;
	link.textContent = labelVals.filter((n) => n.type == 'string').map((n) => str.cssUnescape(n.value)).join('');
}

export async function setBookmarkLabels(bookmarksRoot: Element): Promise<void> {
	const linkIterator = walk.createIterator(bookmarksRoot, walk.isHTMLTag('a'), walk.isElement);
	let link;
	if (window.postscriptumBridge.getAccessibleStrings) {
		const links: HTMLElement[] = [];
		while ((link = linkIterator.nextNode() as HTMLElement)) links.push(link);
		const linkStrings = await window.postscriptumBridge.getAccessibleStrings(links.map((link) => link.id));
		for (let i = 0; i < links.length; i++) {
			const str = linkStrings[i];
			if (str === null) {
				logger.warn(`Accessibility tree node not found for bookmark '#${links[i].id}`);
				labelToContent(links[i]);
			} else {
				const link = links[i];
				link.textContent = str;
				link.removeAttribute('style');
			}
		}

	} else {
		while ((link = linkIterator.nextNode() as HTMLElement)) {
			labelToContent(link);
			link.removeAttribute('style');
		}
	}
}

/**
 * Sets the content of the margins of a page
 * @param page
 * @param context
 */
export function setMarginContents(page: Page, ctx: GCPMContext): void {
	const elements = walk.createWalker(page.body, walk.isHTMLElement);

	const pageStringSets = new Map<HTMLElement, string>();
	const pageRunnings = new Map<HTMLElement, string>();
	do {
		const style = getCustomStyle(elements.currentNode);
		const stringSet = getCustomProp(style, 'string-set').trim();
		if (stringSet && stringSet != 'none') pageStringSets.set(elements.currentNode, stringSet);
		const position = getCustomProp(style, 'position');
		if (position.startsWith('running(')) pageRunnings.set(elements.currentNode, position);
	} while (elements.nextNode());

	ctx.pageStringSets = {
		start: {},
		first: {},
		last: {}
	};

	for (const [stringSetElt, stringSetValue] of pageStringSets) {
		const parts = valueParser(stringSetValue).nodes;
		let i = 0;
		while (i < parts.length) {
			const name = parts[i++].value;
			const valStart = i;
			while (i < parts.length && parts[i].type != 'div' && parts[i].value != ',') i++;
			const partVals = parts.slice(valStart, i);
			stringSetToContentVals(partVals, {
				element: stringSetElt as HTMLElement,
				srcElement: stringSetElt as HTMLElement,
				ctx
			});
			const value = valueParser.stringify(partVals);

			elements.currentNode = stringSetElt;
			// start: "If the element is the first element on the page, the value of the first assignment is used"
			const previousElt = elements.previousNode();
			if (!previousElt || previousElt == page.body) ctx.pageStringSets.start[name] = value;
			if (ctx.pageStringSets.first[name] === undefined) ctx.pageStringSets.first[name] = value;
			// last : always rewritten
			ctx.pageStringSets.last[name] = value;
			i++;
		}
	}

	ctx.pageRunnings = {
		start: {},
		first: {},
		last: {}
	};

	// running: same algorithm than the string-set
	for (const [runningElt, runningValue] of pageRunnings) {
		const call = valueParser(runningValue).nodes[0] as FunctionNode;
		const name = call.nodes[0].value;

		elements.currentNode = runningElt;
		const previousElt = elements.previousNode();
		if (!previousElt || previousElt == page.body) ctx.pageRunnings.start[name] = runningElt;
		if (ctx.pageRunnings.first[name] === undefined) ctx.pageRunnings.first[name] = runningElt;
		ctx.pageRunnings.last[name] = runningElt;
	}

	const withContent = new Set<HTMLElement>();
	const withSize = new Set<HTMLElement>();
	for (const position in page.marginBoxes) {
		const box = page.marginBoxes[position];
		const contentStyle = getComputedStyle(box, '::before');
		if (contentStyle.content != "none") {
			parsePseudosVals(box, 'before', ctx);
			withContent.add(box);
			const sizeStyle = getComputedStyle(box);
			const sizeProp = position.startsWith('top') || position.startsWith('bottom') ? 'maxWidth' : 'maxHeight';
			if (sizeStyle[sizeProp] != "none") withSize.add(box);
		} else if (position.endsWith('corner')) {
			box.remove();
		}
	}

	// Balancing of margins
	for (let side: string, i = 0; i < SIDES.length, side = SIDES[i]; i++) {
		const horizontal = i % 2 == 0;
		const [startBox, centerBox, endBox] = (horizontal ? HORIZONTAL_MARGIN_BOXES : VERTICAL_MARGIN_BOXES)
			.map((pos) => page.marginBoxes[side + '-' + pos]);

		const sizeProp = horizontal ? 'width' : 'height';
		const marginProp = horizontal ? 'marginInline' : 'marginBlock';
		const [startMarginProp, endMarginProp,] = [marginProp + 'Start', marginProp + 'End'] as ['marginInlineStart', 'marginInlineEnd'];

		// eslint-disable-next-line no-inner-declarations
		function getSize(elem: Element, style?: CSSStyleDeclaration): number {
			let size = elem.getBoundingClientRect()[sizeProp];
			if (!style) style = getComputedStyle(elem);
			size += parseFloat(style[startMarginProp]) + parseFloat(style[endMarginProp]);
			return size;
		}

		if (withContent.has(centerBox)) {
			/// Central content
			if (!withContent.has(startBox) && !withContent.has(endBox)) {
				// Removes the sides margin if both have no content
				startBox.remove();
				endBox.remove();
				// Center the central margin
				centerBox.parentElement.style.justifyContent = 'center';
			} else {
				const centerStyle = getComputedStyle(centerBox);
				const [startSize, centerSize, endSize] = [getSize(startBox), getSize(centerBox, centerStyle), getSize(endBox)];
				const availSideSize = ((getSize(centerBox.parentElement) - centerSize) / 2);
				const maxSideSize = Math.max(startSize, endSize);
				const centerMargin = Math.max(parseFloat(centerStyle[startMarginProp]), parseFloat(centerStyle[endMarginProp]));

				if (withSize.has(centerBox)) {
					// Central margin with size
					const targetSideSize = availSideSize + centerMargin;
					if (withSize.has(startBox)) centerBox.style[startMarginProp] = (targetSideSize - startSize) + 'px';
					if (withSize.has(endBox)) centerBox.style[endMarginProp] = (targetSideSize - endSize) + 'px';
					centerBox.style.flexGrow = '0';
					startBox.style.flexBasis = endBox.style.flexBasis = maxSideSize + 'px';
				} else if (withSize.has(startBox) || withSize.has(endBox)) {
					// Side margin with size
					const targetSideSize = (maxSideSize > availSideSize ? availSideSize : maxSideSize) + centerMargin;
					centerBox.style[startMarginProp] = (targetSideSize - startSize) + 'px';
					centerBox.style[endMarginProp] = (targetSideSize - endSize) + 'px';
					startBox.style.flexGrow = endBox.style.flexGrow = '0';
				} else {
					// No size on margins
					centerBox.style.flexBasis = centerSize + 'px';
					if (centerMargin) centerBox.style[marginProp] = centerMargin + 'px';
					startBox.style.flexBasis = endBox.style.flexBasis = maxSideSize + 'px';
				}
			}
		} else {
			const margin = centerBox.parentElement;
			/// No central content
			// Removes all the margin if the side margins are also empty
			if (!withContent.has(startBox) && !withContent.has(endBox)) margin.remove();
			else {
				centerBox.remove();
				if (!withContent.has(startBox)) startBox.remove();
				if (!withContent.has(endBox)) endBox.remove();
				// Space out the two side margin
				margin.style.justifyContent = 'space-between';
			}
		}

		// Keep output values for the next page
		for (const stringName in ctx.pageStringSets.last) {
			ctx.stringSets[stringName] = ctx.pageStringSets.last[stringName];
		}

		for (const runningName in ctx.pageRunnings.last) {
			ctx.runnings[runningName] = ctx.pageRunnings.last[runningName];
		}
	}
}

export function setPageTargetCounters(page: Fragment, dest: HTMLElement, ctx: GCPMContext): void {
	if (!ctx.targetCounters) return;
	for (const id in ctx.targetCounters) {
		const target = page.body.querySelector('#' + id);
		if (target) {
			const targetCounters = ctx.targetCounters[id];
			for (const counterName in targetCounters) {
				if (counterName == 'page') {
					const varValue = '"' + page.number.toString() + '"';
					for (const targetCounter of targetCounters[counterName]) {
						targetCounter.element.style.setProperty(targetCounter.varName, varValue);
					}
					delete targetCounters[counterName];
				}
			}
			if (!Object.keys(targetCounters).length) delete ctx.targetCounters[id];
		}
	}
}

export function setTargetCounters(ctx: GCPMContext, pages: Fragment[]): void {
	if (!ctx.targetCounters) return;
	for (const id in ctx.targetCounters) {
		const target = document.getElementById(id);
		const targetCounters = ctx.targetCounters[id as string];

		for (const counterName in targetCounters) {
			let counterValue: number;
			if (!target) counterValue = NaN;
			else if (counterName == 'pages') counterValue = pages.length;
			else if (ctx.rawPageCounters && counterName == 'page') counterValue = getPageCounterValue(pages, target);
			else counterValue = getCounterValue(target, counterName);

			for (const targetCounter of targetCounters[counterName]) {
				if (isNaN(counterValue)) {
					targetCounter.element.style.setProperty(targetCounter.varName, `""`);
				} else {
					const counterResets = parseCounterValue(getComputedStyle(targetCounter.element).counterReset);
					counterResets[targetCounter.varName] = counterValue;
					targetCounter.element.style.counterReset = Object.entries(counterResets).flat().join(' ');
					targetCounter.element.style.setProperty(targetCounter.varName, `counter(${targetCounter.varName}, ${targetCounter.style || 'decimal'})`);
				}
			}
		}
	}
}

export function setTargetsCounters(ctx: GCPMContext): void {
	if (!ctx.targetsCounters) return;
	for (const targetCounter of ctx.targetsCounters) {
		const varValueProp = targetCounter.element.style.getPropertyValue(targetCounter.varName);
		const varValue = valueParser(varValueProp);
		const counters = new Set();
		if (varValue.nodes.length > 1) {
			const sep = varValue.nodes[1].value;
			for (const node of varValue.nodes) {
				if (node.type == "function") {
					// First argument of the var function
					const counter = targetCounter.element.style.getPropertyValue(node.nodes[0].value);
					counters.add(counter);
				}
			}
			targetCounter.element.style.setProperty(targetCounter.varName, Array.from(counters).join(`"${str.cssEscape(sep)}"`));
		}
	}
}

export function setLeaders(ctx: GCPMContext): void {
	if (!ctx.leaders || !ctx.leaders.size) return;
	for (const [element, leader] of ctx.leaders.entries()) {
		// If the element is displayed
		if (element.offsetHeight) {
			let block = element;
			let blockStyle = getComputedStyle(block);
			while (!walk.displayAsBlockStyle(blockStyle)) {
				block = block.parentNode as HTMLElement;
				blockStyle = getComputedStyle(block);
			}
			block.setAttribute('ps-has-leader', '');

			const blockRect = rects.boundingScrollRect(block);

			// We force an alignement when the text is justified in order to have an available space
			const initialTextAlign = blockStyle.textAlign;
			let textAlign = initialTextAlign;
			if (initialTextAlign == 'justify') {
				block.style.textAlign = 'start';
				textAlign = block.style.textAlign;
			}

			let leaderRect = rects.boundingScrollRect(leader.element);
			const patternWidth = leaderRect.width;
			leader.element.textContent = '';
			leaderRect = rects.boundingScrollRect(leader.element);

			const ltr = blockStyle.direction == 'ltr';
			let [start, end] = getLeaderStartEnd(block, blockRect, leader, leaderRect, ltr, textAlign);
			const blockEndWidth = ltr ? parseFloat(blockStyle.paddingRight) + parseFloat(blockStyle.borderRightWidth) : parseFloat(blockStyle.paddingLeft) + parseFloat(blockStyle.borderRightWidth);
			let endWidth = end - blockEndWidth;
			if (endWidth < 0.1) {
				leader.element.parentNode.insertBefore(document.createElement('br'), leader.element);
				leaderRect = rects.boundingScrollRect(leader.element);
				const [newStart, newEnd] = getLeaderStartEnd(block, blockRect, leader, leaderRect, ltr, textAlign);
				const newEndWidth = newEnd - blockEndWidth;
				if (newEndWidth < 0.1) {
					leader.element.previousElementSibling.remove();
					leaderRect = rects.boundingScrollRect(leader.element);
				} else {
					start = newStart;
					end = newEnd;
					endWidth = newEndWidth;
				}

			}

			const space = blockRect.width - end - start;
			leader.element.style.width = Math.floor(space * 10) / 10 - 1 + 'px';

			const leaderEnd = leader.element.appendChild(document.createElement('ps-leader-end'));
			if (blockStyle.direction == 'ltr') leaderEnd.style.marginRight = -(endWidth) + 'px';
			else leaderEnd.style.marginLeft = -(endWidth) + 'px';
			let endText = '';
			const endPatternCount = endWidth / patternWidth;
			for (let i = 0; i < endPatternCount; i++) endText += leader.pattern;
			leaderEnd.textContent = endText;

			let leaderText = '';
			const patternCount = Math.floor(space / patternWidth);
			for (let i = 0; i < patternCount; i++) leaderText += leader.pattern;
			leader.element.appendChild(document.createTextNode(leaderText));
			if (leader.element.scrollWidth > leader.element.clientWidth) {
				leader.element.lastChild.nodeValue = leaderText.slice(0, -1);
			}

			// Restoration of the initial alignement
			if (initialTextAlign == 'justify') {
				block.style.textAlign = '';
			}

		}
	}
}

function getLeaderStartEnd(block: HTMLElement, blockRect: rects.Rect, leader: Leader, leaderRect: rects.Rect, ltr: boolean, textAlign: string): [number, number] {
	const start = ltr ? leaderRect.left - blockRect.left : blockRect.right - leaderRect.right;
	if (textAlign == 'start') block.style.textAlign = 'end';
	else if (textAlign == 'end') block.style.textAlign = 'start';
	else if (textAlign == 'left') block.style.textAlign = 'right';
	else if (textAlign) block.style.textAlign = 'left';
	const leaderRectInverse = rects.boundingScrollRect(leader.element);
	const end = ltr ? blockRect.right - leaderRectInverse.right : leaderRectInverse.left - blockRect.left;
	block.style.textAlign = textAlign;
	return [start, end];
}


function checkTarget(hrefAttr: string): string {
	const urlFragment = hrefAttr.split('#');
	if (urlFragment[0] && urlFragment[0] !== document.baseURI) {
		logger.warn(`Invalid target-counter() content function call : the url does not correspond to the document.`);
		return null;
	}
	if (urlFragment.length < 2 || !urlFragment[1]) {
		logger.warn(`Invalid target-counter() content function call : the url fragment is empty.`);
		return null;
	}
	return urlFragment[1];
}


export function isFloatCall(elem: Element): boolean {
	return elem.localName == 'a' && elem.hasAttribute('ps-float-call');
}

function stringNode(value: string): StringNode {
	return {type: "string", value, quote: '"'} as StringNode;
}
