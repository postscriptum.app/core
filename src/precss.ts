import postcss from "postcss";
import valueParser from "postcss-value-parser";
import {io, logger} from "./util.js";
import type {Node} from "postcss-value-parser";

export type Options = {
	defaultPageSize: [string, string];
	mediaType?: string;
	propertiesRegistered?: boolean;
};

const defaultOptions: Options = {
	defaultPageSize: ['21cm', '29.cm'],
	propertiesRegistered: true
};

interface PrecssPlugin<T = void> {
	(opts?: T): { postcssPlugin: string };

	postcss: boolean;
}

const GCPM_CONTENT_FUNCS = ['target-text', 'target-content', 'target-counter', 'target-counters', '-ps-targets-counter', '-ps-target-attr', 'leader', 'element', 'string'];
const SIDES = ['top', 'right', 'bottom', 'left'];
const LOGICAL_SIDES = ['block-start', 'inline-end', 'block-end', 'inline-start'];
const LOGICAL_SIDES_SHORTHAND = ['block', 'inline'];
const AREA_CSS_PROPS = ['margin', 'padding', 'border', 'outline', 'grid', 'display'];

const CONTENT_PSEUDOS_RE = /::(before|after|marker)\s?/;
const SINGLE_COLON_PSEUDO_RE = /(^|[^:]):(before|after|first-letter|first-line)/g;
const PROPS_TO_PREFIX_RE = /^(?:size|page|break-.+|page-break-.+|orphans|widows|bookmark-.+|string-set|position|float|footnote-.+|margin-break|box-decoration-break|bleed|marks|columns|column-.+|hyphens|-ps-.+|-psp-.+)$/;
const PAGE_BREAKS = ['auto', 'avoid', 'always', 'page', 'left', 'right', 'recto', 'verso'];
/** @see https://developer.mozilla.org/en-US/docs/Web/CSS/url() for the list of properties */
const URI_PROPS_RE = /^(--.+|.+-image|background|list-style|cursor|content|border|border-image-source|mask|-webkit-mask|src|symbols)$/;
const PSEUDO_TO_ATTR_RE = /(?::)?:-((?:ps-|psp-)[A-Za-z-]+)(?:\((.+?)\))?/g;
const FLOAT_PSEUDO_RE = /^(.*)::(-ps-float|footnote)-(marker|call)$/;
const COLUMNS_PSEUDO_RE = /^(.*)::-ps-columns(\b.+)?$/;
const MEDIA_QUERY_OPERATORS = ['not', 'and', 'only'];
const COMMA_VAL_NODE = {type: "div", value: ","} as Node;
const HREF_VAL_NODE = {type: "word", value: "href"} as Node;

// TODO CSS source maps
const plugin: PrecssPlugin<Options> = (options) => {
	const opts = Object.assign(Object.create(defaultOptions), options);
	return {
		postcssPlugin: 'ps-precss',
		Once: (root: postcss.Root) => {
			if (opts.mediaType) selectMediaType(root, opts);
			css3Pseudos(root);
			replaceAtPageRules(root, opts);
			prefixProps(root, opts);
			replacePseudoElements(root);
			replaceContent(root);
		}
	};
};
plugin.postcss = true;

export default plugin;

export const styleAttrPlugin: PrecssPlugin = () => {
	return {
		postcssPlugin: 'ps-precss-styleattr',
		Once: (root: postcss.Root) => {
			const customDecls = prefixPropsDecls(root);
			for (const decl of customDecls) root.append(decl);
		}
	};
};
styleAttrPlugin.postcss = true;

function selectMediaType(root: postcss.Root, options: Options): void {
	root.walkAtRules(/^media|import\b/, (atRule) => {
		atRule.params = replaceMediaType(atRule.params, options.mediaType);
	});
}

export function replaceMediaType(mediaQuery: string, mediaType: string): string {
	const queryList = valueParser(mediaQuery);
	const replacedVals = [];
	for (const val of queryList.nodes) {
		if (val.type == "word" && !MEDIA_QUERY_OPERATORS.includes(val.value) && val.value != "all") {
			if (val.value == mediaType) val.value = "all";
			else val.value = "none";
		}
		replacedVals.push(val);
	}
	return valueParser.stringify(replacedVals);
}

function css3Pseudos(root: postcss.Root): void {
	root.walkRules((rule) => {
		rule.selector = rule.selector.replace(SINGLE_COLON_PSEUDO_RE, '$1::$2');
	});
}


function replaceAtPageRules(root: postcss.Root, options: Options): void {
	root.walkAtRules(/^page\b/, (atPageRule) => {
		// Transformation of "@page <name> :<pseudo>" to "ps-page[ps-name=<name>][ps-pseudos~=<pseudo>]"
		// TODO It's not clear if the pseudo needs to have a space before or not
		// TODO According to example 7 from https://www.w3.org/TR/css-page-3/#page-selectors, both are accepted
		const atPageSelectors = postcss.list.comma(atPageRule.name.substr(4) + atPageRule.params);
		const parent = atPageRule.parent;

		for (const atPageSelector of atPageSelectors) {
			let pageSelector = 'ps-page';
			const parts = atPageSelector.replace(/:/g, ' :').split(' ');

			let name = null;
			for (const part of parts) {
				if (part) {
					if (part[0] == ':') {
						if (part.startsWith(':nth(')) {
							pageSelector += ':nth-of-type' + part.substring(4);
						} else if (part == ':first') {
							// Additionnal attribute selector to give more specificity to :first and :blank selectors
							// While :nth-child(n of [ps-name=]) is not implemented, A special pseudo is used for the first page of a group
							if (name) pageSelector += `[ps-pseudos~=first-of-group][ps-pseudos]`;
							else pageSelector += `[ps-pseudos~=${part.substring(1)}][ps-pseudos]`;
						} else if (part == ':blank') {
							pageSelector += `[ps-pseudos~=${part.substring(1)}][ps-pseudos]`;
						} else {
							pageSelector += `[ps-pseudos~=${part.substring(1)}]`;
						}
					} else {
						name = part;
						pageSelector += `[ps-name=${name}]`;
					}
				}
			}

			/*
			 const comment = postcss.comment({ text: atPageRule.toString() });
			 comment.source = atPageRule.source;
			 parent.insertBefore(atPageRule, comment);*/

			const pageRule = postcss.rule({selector: pageSelector});
			pageRule.source = atPageRule.source;

			const pageAreaRule = postcss.rule({selector: pageSelector + ' > ' + 'ps-page-area'});
			pageAreaRule.source = atPageRule.source;

			for (const node of atPageRule.nodes) {
				if (node.type == 'decl') {
					const decl = node as postcss.Declaration;
					if (decl.prop == 'size') {
						// Transforms the size property to --ps-page-width and --ps-page-height
						const size = getPageSize(decl.value, options.defaultPageSize);
						const widthVar = postcss.decl({prop: '--ps-page-width', value: size[0].toString()});
						const heightVar = postcss.decl({prop: '--ps-page-height', value: size[1].toString()});
						heightVar.source = node.source;
						pageRule.append(widthVar);
						pageRule.append(heightVar);
						// The size property is cloned to be prefixed
						pageRule.append(node.clone());
					} else if (AREA_CSS_PROPS.some((areaProp) => decl.prop.startsWith(areaProp))) {
						if (decl.prop.startsWith('margin')) expandSideDecl(node, pageRule, '--ps-page-margin');
						else if (decl.prop.startsWith('padding')) expandSideDecl(node, pageAreaRule, 'padding');
						else pageAreaRule.append(node.clone());
					} else if (decl.prop != 'width' && decl.prop != 'height') {
						// Other properties are copied to the new rules
						// The width and height property must be ignored (cf test css3-page/page-properties-000.xht)
						pageRule.append(node.clone());
					}
				}
			}

			if (pageRule.nodes.length) parent.insertBefore(atPageRule, pageRule);
			if (pageAreaRule.nodes.length) parent.insertBefore(atPageRule, pageAreaRule);

			// Transformation des atRule de marges et de footnote
			for (const node of atPageRule.nodes) {
				if (node.type == 'atrule') {
					const atRule = node as postcss.AtRule;
					const footnotesRule = atRule.name.startsWith('footnote');
					if (atRule.name == '-ps-body') {
						const bodyRule = postcss.rule({selector: `${pageSelector} > ps-page-area > [ps-page-body]`});
						while (atRule.first) bodyRule.append(atRule.first);
						bodyRule.append(postcss.decl({prop: 'grid-area', value: '-ps-body'}));
						parent.insertBefore(atPageRule, bodyRule);
					} else if (atRule.name == '-ps-area' || footnotesRule) {
						// We supports the atRule @footnote as specified by css-gcpm-3 and the @footnotes which is more
						// coherent with our naming convention (PrinceXML use @footnotes too).
						if (footnotesRule) atRule.params = atRule.name + atRule.params;
						const paramsMatches = atRule.params.match(/^(.+?)\b(.*)$/);
						let areaName = paramsMatches[1];
						if (areaName == 'footnote') areaName = 'footnotes';
						const areaRule = postcss.rule({selector: `${pageSelector} > ps-page-area > ps-area[ps-name=${areaName}]${paramsMatches[2].trim()}`});
						while (atRule.first) areaRule.append(atRule.first);
						areaRule.append(postcss.decl({prop: 'grid-area', value: areaName}));
						parent.insertBefore(atPageRule, areaRule);
					} else if (atRule.name.startsWith('-ps-')) {
						const atRuleType = atRule.name.substr(4);
						if (SIDES.includes(atRuleType)) {
							const marginRule = postcss.rule({selector: `${pageSelector} > ps-margin[ps-side=${atRuleType}]`});
							while (atRule.first) marginRule.append(atRule.first);
							parent.insertBefore(atPageRule, marginRule);
						}
					} else {
						const marginSide = atRule.name.substr(0, atRule.name.indexOf('-'));
						const marginBoxSelector = atRule.name.endsWith('-corner')
							? `${pageSelector} > ps-margin-box[ps-name=${atRule.name}]`
							: `${pageSelector} > ps-margin[ps-side=${marginSide}] > ps-margin-box[ps-name=${atRule.name}]`;

						// Multiple selector to handle normal content with :empty::before and running content by :not(:empty)
						const marginBoxRule = postcss.rule({ selector: marginBoxSelector });
						const marginContentRule = postcss.rule({ selector: `${marginBoxSelector}::before` });
						marginBoxRule.source = atRule.source;
						marginContentRule.source = atRule.source;

						const sizeProp = marginSide == 'top' || marginSide == 'bottom' ? 'width' : 'height';
						while (atRule.first) {
							const decl = atRule.first as postcss.Declaration;
							if (decl.prop == "content") marginContentRule.append(decl);
							else {
								if (decl.prop == sizeProp && decl.value != "auto") {
									const minDecl = postcss.decl({ prop: 'min-' + sizeProp, value: decl.value });
									decl.before(minDecl);
									if (decl.prop == sizeProp) decl.prop = "max-" + sizeProp;
								}
								marginBoxRule.append(atRule.first);
							}
						}

						if (marginBoxRule.nodes.length) parent.insertBefore(atPageRule, marginBoxRule);
						if (marginContentRule.nodes.length) parent.insertBefore(atPageRule, marginContentRule);
					}
				}
			}
		}
		atPageRule.remove();
	});
}

function expandSideDecl(decl: postcss.Declaration, targetRule: postcss.Rule, targetProp: string): void {
	const dashIdx =  decl.prop.indexOf('-');
	const sideVals: { top?: string, right?: string, bottom?: string, left?: string, [key: string]: string } = {};
	if (dashIdx != - 1) {
		let side = decl.prop.substring(dashIdx + 1);
		if (LOGICAL_SIDES_SHORTHAND.includes(side)) {
			const valueParts = postcss.list.space(decl.value);
			if (valueParts.length == 1) valueParts[1] = valueParts[0];
			if (side == "inline") {
				sideVals.left = valueParts[0];
				sideVals.right = valueParts[1];
			} else {
				sideVals.top = valueParts[0];
				sideVals.bottom = valueParts[1];
			}
		} else {
			const logicalSideIdx = LOGICAL_SIDES.indexOf(side);
			if (logicalSideIdx != -1) side = SIDES[logicalSideIdx];
			sideVals[side] = decl.value;
		}
	} else {
		const valueParts = postcss.list.space(decl.value);
		switch (valueParts.length) {
		case 1:
			for (let i = 0; i < 4; i++) sideVals[SIDES[i]] = valueParts[0];
			break;
		case 2:
			sideVals.top = sideVals.bottom = valueParts[0];
			sideVals.left = sideVals.right = valueParts[1];
			break;
		case 3:
			sideVals.top = valueParts[0];
			sideVals.left = sideVals.right = valueParts[1];
			sideVals.bottom = valueParts[2];
			break;
		case 4:
			for (let i = 0; i < 4; i++) sideVals[SIDES[i]] = valueParts[i];
			break;
		}
	}


	for (const side in sideVals) {
		if (sideVals[side] !== undefined) {
			targetRule.append(decl.clone({
				prop: `${targetProp}-${side}`,
				value: pagePercentValue(sideVals[side], side)
			}));
		}
	}
}

function pagePercentValue(value: string, side: string): string {
	const base = side == 'top' || side == 'bottom' ? 'height' : 'width';
	if (value.endsWith('%')) {
		return `calc(var(--ps-page-${base}) * ${parseFloat(value)} / 100)`;
	}
	return value;
}

export function getPageSize(value: string, defaultSize?: [string, string]): [string, string] {
	let size: [string, string] = null;
	const valueParts = postcss.list.space(value);
	const lastPart = valueParts.at(-1);
	const landscape = lastPart == "landscape";
	if (lastPart == "portrait" || landscape) {
		valueParts.pop();
		if (!valueParts.length) valueParts.push("auto");
	}

	if (valueParts.length && valueParts[0].match(/^[a-z]/i)) {
		/* Specified by https://www.w3.org/TR/css-page-3/#typedef-page-size-page-size */
		switch (valueParts[0].toLowerCase()) {
		case 'a5':
			size = ['148mm', '210mm'];
			break;
		case 'a4':
			size = ['210mm', '297mm'];
			break;
		case 'a3':
			size = ['297mm', '420mm'];
			break;
		case 'b5':
			size = ['176mm', '250mm'];
			break;
		case 'b4':
			size = ['250mm', '353mm'];
			break;
		case 'jis-b5':
			size = ['182mm', '257mm'];
			break;
		case 'jis-b4':
			size = ['257mm', '364mm'];
			break;
		case 'letter':
			size = ['8.5in', '11in'];
			break;
		case 'legal':
			size = ['8.5in', '14in'];
			break;
		case 'ledger':
			size = ['11in', '17in'];
			break;
		case 'auto':
			size = defaultSize ? defaultSize : ['210mm', '297mm'];
			break;
			/* Other sizes of A series format */
		case 'a6':
			size = ['105mm', '148mm'];
			break;
		case 'a7':
			size = ['74mm', ' 105mm'];
			break;
		case 'a8':
			size = ['52mm', '74mm'];
			break;
		case 'a9':
			size = ['37mm', '52mm'];
			break;
		case 'a10':
			size = ['26mm', '37mm'];
			break;
		}
	}

	if (!size) {
		if (valueParts.length >= 2) size = [valueParts[0], valueParts[1]];
		else size = [valueParts[0], valueParts[0]];
	}

	if (landscape) {
		size = [size[1], size[0]];
	}
	return size;
}

function prefixProps(root: postcss.Root, options: Options): void {
	root.walkRules((rule: postcss.Rule) => {
		const customDecls = prefixPropsDecls(rule);
		if (customDecls.length) {
			if (options.propertiesRegistered) {
				for (const decl of customDecls) rule.append(decl);
			} else {
				const customRule = postcss.rule();
				customRule.selectors = rule.selectors.map((selector) => selector + '::backdrop');
				rule.parent.insertBefore(rule, customRule);
				for (const decl of customDecls) customRule.append(decl);
			}
		}
		if (!rule.nodes.length) rule.remove();
	});
}

function prefixPropsDecls(root: postcss.Container): postcss.Declaration[] {
	const customDecls: postcss.Declaration[] = [];
	root.walkDecls(PROPS_TO_PREFIX_RE, (decl: postcss.Declaration) => {
		let custom: boolean | RegExpMatchArray;
		let resetCustom = false;
		let customDecl: postcss.Declaration;
		if (decl.prop.startsWith('column')) {
			root.walkDecls(/^-webkit-column/, (webkitDecl) => {
				webkitDecl.remove();
			});
		}
		if (decl.prop.startsWith('page-break-')) {
			if (PAGE_BREAKS.includes(decl.value)) {
				customDecl = decl.clone();
				customDecl.prop = decl.prop.replace(/^page-break-/, '--ps-break-');
				if (decl.value == 'always') customDecl.value = 'page';
				else if (decl.value == 'avoid') customDecl.value = 'avoid-page';
				custom = true;
			} else {
				decl.remove();
			}
		} else if (decl.prop == 'position') {
			custom = decl.value.match(/^running\(/);
			if (!custom) resetCustom = true;
		} else if (decl.prop == 'float') {
			if (decl.value == 'footnote') {
				decl.value = '-ps-area(footnotes)';
				custom = true;
			} else {
				custom = decl.value.match(/^-ps-area\(/);
			}
			if (!custom) resetCustom = true;
		} else if (decl.prop.startsWith('footnote-')) {
			decl.prop = decl.prop.replace('footnote-', '-ps-float-');
			custom = true;
		} else if (decl.prop == 'orphans' || decl.prop == 'widows') {
			// The properties 'orphans' and 'widows' are inherited, we do not hide them in a pseudo
			// Only positive integers are allowed as values of orphans and widows. Negative values and zero are invalid and must cause the declaration to be ignored.
			// https://drafts.csswg.org/css-break-3/#widows-orphans
			if (decl.value.match(/^\d+$/) && parseInt(decl.value) > 0) decl.prop = '--ps-' + decl.prop;
			else decl.remove();
		} else if (decl.prop == 'columns' || decl.prop == '-ps-columns') {
			const widthDecl = decl.cloneAfter();
			widthDecl.prop = '-ps-column-width';
			const values = valueParser(decl.value).nodes.filter((n) => n.type == "word").map((n) => n.value);

			if (values[0].match(/^\d+$/)) {
				decl.value = values[0];
				widthDecl.value = values[1] || "auto";
			} else if (values.length > 1 && values[1].match(/^\d+$/)) {
				decl.value = values[1];
				widthDecl.value = values[0] || "auto";
			} else {
				decl.value = "auto";
				widthDecl.value = values[0] == "auto" ? (values[1] || "auto") : values[0];
			}
			decl.prop = '-ps-column-count';
			custom = true;
		} else if (decl.prop == 'hyphens') {
			// The property 'hyphen' are inherited, we do not hide it in a pseudo
			if (decl.value == "auto") {
				decl.cloneBefore({value: "manual"});
				decl.prop = '--ps-' + decl.prop;
			}
		} else if (decl.prop == '-ps-hyphens') {
			// The property 'hyphen' are inherited, we do not hide it in a pseudo
			if (decl.value == "auto") {
				decl.cloneBefore({prop: 'hyphens', value: "manual"});
				decl.prop = '-' + decl.prop;
			}
		} else if (decl.prop == 'bleed') {
			decl.prop = '--ps-bleed';
			decl.cloneBefore({prop: '--ps-bleed-size', value: decl.value == "auto" ? "0px" : decl.value});
		} else if (decl.prop == 'marks') {
			decl.prop = '--ps-' + decl.prop;
		} else {
			custom = true;
		}
		if (custom || resetCustom) {
			if (!customDecl) {
				customDecl = decl.clone();
				if (decl.prop.startsWith('-ps-')) customDecl.prop = '-' + decl.prop;
				else if (decl.prop.startsWith('-psp-')) customDecl.prop = '-' + decl.prop;
				else customDecl.prop = '--ps-' + decl.prop;
				if (resetCustom) customDecl.value = '';
			}
			customDecls.push(customDecl);
			if (!resetCustom) decl.remove();
		}
	});
	return customDecls;
}

function replacePseudoElements(root: postcss.Root): void {
	function replaceRule(rule: postcss.Rule): void {
		const selectors = rule.selectors.slice();
		for (let i = 0; i < selectors.length; i++) {
			// Floats
			let matches: RegExpMatchArray;
			if ((matches = selectors[i].match(FLOAT_PSEUDO_RE))) {
				const [, baseSelector, selKind, pseudoKind] = matches;

				// Support of ::footnote-call
				if (pseudoKind == "call") {
					let floatSel = `[ps-float-${pseudoKind}]`;
					if (baseSelector) floatSel = baseSelector + ' + ' + floatSel;
					// Support of ::footnote-call and ::footnote-marker specified by css-gcpm-3
					if (selKind == 'footnote') floatSel += '[ps-float-area="footnotes"]';
					selectors[i] = floatSel;

					const floatContentRule = rule.clone({selector: floatSel + '::before', nodes: []});
					rule.walkDecls('content', (decl) => {
						// Replace counter call by target-counter
						const value = valueParser(decl.value);
						for (const node of value.nodes) {
							if (node.type == 'function' && node.value == "counter") {
								node.value = "target-counter";
								node.nodes.unshift({
									type: "function",
									"value": "attr",
									nodes: [HREF_VAL_NODE]
								} as Node, COMMA_VAL_NODE);
							}
						}
						decl.value = value.toString();

						if (i == selectors.length - 1) floatContentRule.append(decl);
						else floatContentRule.append(decl.clone());
					});

					if (floatContentRule.nodes.length) rule.parent.insertBefore(rule, floatContentRule);
				} else {
					// Support of ::footnote-marker
					const floatSel = selKind == 'footnote' ? `[ps-float="footnotes"]` : '[ps-float]';
					// Use of marker for float-display: block, ::before otherwise
					selectors[i] = `${baseSelector}${floatSel}[ps-float-display=block]::marker`;
					selectors.push(`${baseSelector}${floatSel}:not([ps-float-display=block])::before`);
				}
			} else if ((matches = selectors[i].match(COLUMNS_PSEUDO_RE))) {
				// Columns
				const [, preSelector, postSelector] = matches;
				const parentSelector = preSelector ? preSelector + ' > ' : '';
				const columnBodyRule = rule.cloneAfter();
				columnBodyRule.selectors = [`${parentSelector} ps-columns > ps-column > ps-column-body${postSelector || ''}`, `${parentSelector} ps-column-body${postSelector || ''}`];
			} else {
				selectors[i] = selectors[i].replace(PSEUDO_TO_ATTR_RE, (match, p1, p2) => p2 ? `[${p1}=${p2}]` : `[${p1}]`);
			}
		}

		rule.selectors = selectors;
	}

	root.walkRules(replaceRule);

	root.walkAtRules(/^scope\b/, (atRule) => {
		const params = atRule.params.split(' to ').map((param) => {
			const rule = postcss.rule({ selector: param.substring(1, param.length - 1) });
			replaceRule(rule);
			return rule.selector;
		});
		atRule.params = params.map((param) => `(${param})`).join(' to ');
	});
}

function replaceContent(root: postcss.Root): void {
	root.walkRules(CONTENT_PSEUDOS_RE, (rule: postcss.Rule) => {
		let contentDecl: postcss.Declaration = null;
		let hasGcpm = false;
		rule.walkDecls('content', (decl) => {
			contentDecl = decl;
			const value = valueParser(decl.value);
			for (const node of value.nodes) {
				if (node.type == 'function') {
					if (GCPM_CONTENT_FUNCS.includes(node.value)) {
						hasGcpm = true;
						break;
					}
				}
			}
		});
		if (!contentDecl) return;

		// We write one rule for each pseudo of the original rule
		const ruleByPseudo: { [pseudo: string]: postcss.Rule } = {};
		const selectors = rule.selectors.slice();
		for (const selector of selectors) {
			const selectorMatches = selector.match(CONTENT_PSEUDOS_RE);
			const pseudo = selectorMatches ? selectorMatches[1] : '';
			let currentRule = ruleByPseudo[pseudo];
			if (!currentRule) {
				// Cloned before to avoid double parsing
				currentRule = ruleByPseudo[pseudo] = Object.keys(ruleByPseudo).length ? rule.cloneBefore() : rule;
				currentRule.selector = '';
			} else {
				currentRule.selector += ', ';
			}
			currentRule.selector += selector;
		}

		for (const pseudo in ruleByPseudo) {
			if (pseudo) {
				const rule = ruleByPseudo[pseudo];
				if (hasGcpm) {
					rule.walkDecls('content', (decl) => {
						const value = valueParser(decl.value);
						const leaderIndex = value.nodes.findIndex((node) => node.type == 'function' && node.value == 'leader');

						// If the content has a leader, the values before and after are moved on pseudo element of a 'ps-leader' element
						if (leaderIndex != -1) replaceLeader(rule, decl, value, pseudo);

						const vals: valueParser.Node[] = [];
						value.walk((node, index, nodes) => {
							if (node.type == 'function' && GCPM_CONTENT_FUNCS.includes(node.value)) {
								nodes[index] = valueParser(`var(--ps-${pseudo}-val-${vals.length}, "")`).nodes[0];
								vals.push(node);
								return false;
							}
						});

						decl.value = value.toString();
						rule.insertAfter(decl, decl.clone({
							prop: `--ps-${pseudo}-vals`,
							value: vals.map((val) => valueParser.stringify(val)).join(' ')
						}));
					});
				} else {
					rule.append(contentDecl.clone({
						prop: `--ps-${pseudo}-vals`,
						value: "none"
					}));
				}
			}
		}
	});
}

function replaceLeader(rule: postcss.Rule, decl: postcss.Declaration, value: valueParser.ParsedValue, pseudo: string): void {
	const leaderSelector = rule.selectors.map((sel) => {
		const selWithoutPseudo = sel.substring(0, sel.indexOf('::' + pseudo));
		return `${selWithoutPseudo} > ps-leader[ps-position=${pseudo}]`;
	}).join(', ');
	const leaderRule = rule.cloneAfter({selector: leaderSelector});
	leaderRule.walkDecls('content', (decl) => {
		decl.remove();
	});
}

export const importUnfold: PrecssPlugin<{ from: string }> = (options) => {
	return {
		postcssPlugin: 'ps-import-unfold',
		Once: (root: postcss.Root) => {
			const imports: Promise<void>[] = [];
			root.walkAtRules('import', (atImportRule) => {
				const params = valueParser(atImportRule.params).nodes;
				const urlFn = params[0];
				let url;
				if (urlFn.type == 'string') url = new URL(urlFn.value, options.from);
				else if (urlFn.type == 'function' && urlFn.value == 'url' && urlFn.nodes.length) url = new URL(urlFn.nodes[0].value, options.from);
				if (url) {
					const importOptions = Object.create(options);
					importOptions.from = url.href;

					const transformation = io.load(url.href).then((response) => {
						const processor = postcss().use(importUnfold(importOptions));
						return processor.process(response, importOptions);
					}).then((result) => {
						atImportRule.append(result.root.nodes);
					}).catch((error) => {
						logger.warn(error);
					});
					imports.push(transformation);
				}
			});

			root.walkDecls(URI_PROPS_RE, (decl) => {
				const value = valueParser(decl.value);
				value.walk((node) => {
					if (node.type == 'function' && node.value == 'url') {
						const firstParam = node.nodes[0];
						if (firstParam.type == 'word') Object.assign(firstParam, {type: 'string', quote: '"'});
						const url = new URL(firstParam.value, options.from);
						firstParam.value = url.href;
					}
				});
				decl.value = value.toString();
			});
			return Promise.all(imports) as unknown as Promise<void>;
		}
	};
};
importUnfold.postcss = true;

export const importFold: PrecssPlugin = () => {
	function walkAtImportRule(node: postcss.Container): void {
		node.walkAtRules('import', (atImportRule) => {
			if (atImportRule.nodes) {
				walkAtImportRule(atImportRule);
				const params = postcss.list.space(atImportRule.params);
				const importContent = postcss.root();
				importContent.nodes = atImportRule.nodes;
				delete atImportRule.nodes;
				const blob = new Blob([importContent.toString()], {type: 'text/css'});
				params[0] = `url(${URL.createObjectURL(blob)})`;
				atImportRule.params = params.join(' ');
			}
		});
	}

	return {
		postcssPlugin: 'ps-import-fold',
		Once: walkAtImportRule
	};
};
importFold.postcss = true;
