import {logger, ranges, rects, walk} from "./util.js";
import type {Areas, LayoutProcessorOptions, SubFragmentor, SubProcessor, SubProcessorContext, SubProcessorResult} from "./layout.js";
import {LayoutContext, LayoutProcessor, testAvoidBreakInside} from "./layout.js";
import Mutations from "./mutations.js";
import type {Process} from "./process.js";
import type {Fragmentor, FragmentorEvents} from "./fragments.js";

export interface CellLayoutProcessorEvents extends FragmentorEvents {
	'cell-start': Record<string, never>;
	'cell-end': Record<string, never>;
}

class CellLayoutProcessor extends LayoutProcessor<LayoutProcessorOptions, CellLayoutProcessorEvents> implements SubFragmentor {
	limitedFloats: boolean;

	constructor(source: HTMLElement, readonly parentFragmentor: Fragmentor) {
		super(source, {logLevel: parentFragmentor.options.logLevel});

		this.gcpmContext = parentFragmentor.gcpmContext;
		this.avoidBreakTypes = parentFragmentor.avoidBreakTypes;
		this.subProcessors = Array.from(parentFragmentor.subProcessors);
		this.parentEmitter = parentFragmentor;
		this._process = this.cellProcess();
	}

	* cellProcess(): Process {
		if (this.listenerCount('cell-start')) yield 'cell-start';
		logger.addScope(() => {
			const row = this.source.parentElement;
			const cellIndex = Array.prototype.indexOf.call(row.children, this.source);
			const rowIndex = Array.prototype.indexOf.call(row.parentNode.children, row);
			return `row ${rowIndex + 1}, cell ${cellIndex + 1}`;
		});
		const parentCtx = this.parentFragmentor.layoutContext;
		yield* this.contentsProcess(this.source, parentCtx.areas, parentCtx);
		if (this.limitedFloats) logger.info(`Some floats have been deported.`);
		logger.removeScope();
		if (this.listenerCount('cell-end')) yield 'cell-end';
	}

	createLayoutContext(root: HTMLElement, areas: Areas, parentCtx: LayoutContext): LayoutContext {
		class CellLayoutContext extends LayoutContext {
			get bodyBottom(): number {
				return parentCtx.bodyBottom;
			}

			set bodyBottom(val) {
				parentCtx.bodyBottom = val;
			}
		}

		const ctx = new CellLayoutContext(root, parentCtx.body, areas, parentCtx);
		const floatBottomLimit = Math.min(rects.boundingScrollRect(this.source).bottom + ctx.bottomOffsets.get(this.source.parentElement).inside, parentCtx.bodyBottom);
		ctx.on("float-move", ({newAreaBodyRect}) => {
			if (newAreaBodyRect.bottom <= floatBottomLimit) {
				this.limitedFloats = true;
				return 'pending';
			}
		});
		return ctx;
	}
}

export const rowSubFragmentor: SubProcessor = function (this: Fragmentor, ctx: SubProcessorContext): SubProcessorResult {
	const rowType = walk.isRow(ctx.currentStyle);
	if (!rowType) return false;

	return (async () => {
		const row = ctx.currentElement;
		const cells: HTMLElement[] = [];
		for (const cell of row.children as HTMLCollectionOf<HTMLElement>) {
			if (walk.isStatic(cell)) {
				cells.push(cell);
			}
		}

		const { mutations: boxMutations } = ctx.currentBox;
		// Style modification
		boxMutations.setAttr(row, 'ps-row', rowType);
		const verticalStyleMut = new Mutations();
		for (const cell of cells) {
			const props: { [key: string]: string } = {};
			if (rowType == 'flex-row') props['align-items'] = 'flex-start';
			else props['vertical-align'] = 'top';
			verticalStyleMut.setStyle(cell, props);
		}

		const cellProcessors: Map<HTMLElement, CellLayoutProcessor> = new Map();
		let invalid = false;
		let overflow = false;
		for (const cell of cells) {
			if (walk.firstChild(cell, walk.isStaticNode)) {
				const cellProcessor = new CellLayoutProcessor(cell, this);
				cellProcessors.set(cell, cellProcessor);
				await cellProcessor.start().ended;
				if (cellProcessor.lastEvent == 'no-break-point') {
					invalid = true;
					break;
				} else if (cellProcessor.lastEvent != 'no-overflow') {
					overflow = true;
				}
			}
		}

		verticalStyleMut.revert();

		if (invalid) {
			for (const cell of cells) if (cellProcessors.has(cell)) cellProcessors.get(cell).layoutContext.mutations.revert();
			return {event: 'no-break-point' as const};
		} else if (overflow) {
			for (const cell of cells) if (cellProcessors.has(cell)) cellProcessors.get(cell).layoutContext.mutations.commit(boxMutations);
			const destRow = row.cloneNode(false);
			boxMutations.setAttr(row, 'ps-breaked-after');
			boxMutations.setAttr(destRow, 'ps-breaked-before');
			boxMutations.insertNode(row.parentElement, destRow, row.nextSibling);

			for (const cell of cells) {
				const cellProcessor = cellProcessors.get(cell);
				const breakPoint = cellProcessor ? cellProcessor.layoutContext.breakPoint : ranges.positionAtStart(cell);
				const cellAfter = boxMutations.breaks(cell, breakPoint);
				destRow.appendChild(cellAfter);
				if (!walk.firstChild(cellAfter, walk.isStaticNode)) boxMutations.setAttr(cell, 'ps-empty-after');
			}
			return {event: 'valid-break-point' as const, breakPoint: ranges.positionBefore(destRow)};
		} else {
			return {event: 'no-overflow' as const};
		}
	})();
};


interface CellRef {
	cell: HTMLTableCellElement;
	rowSpan: number;
	colSpan: number;
}

function isCellRef(object: any): object is CellRef {
	return 'cell' in object;
}

function getCell(cell: HTMLTableCellElement | CellRef): HTMLTableCellElement {
	return isCellRef(cell) ? cell.cell : cell;
}

type TableRow = (HTMLTableCellElement | CellRef)[];

export const trWithRowSpanSubFragmentor: SubProcessor = function (this: Fragmentor, ctx: SubProcessorContext): SubProcessorResult {
	const trWithRowSpan = ctx.currentStyle.display == 'table-row'
		&& walk.isHTMLTag('tr')(ctx.currentElement)
		&& Array.prototype.some.call(ctx.currentElement.cells, (cell: HTMLTableCellElement) => cell.rowSpan > 1);
	if (!trWithRowSpan) return false;

	return (async () => {
		const { mutations: boxMutations } = ctx.currentBox;
		boxMutations.setAttr(ctx.currentElement, 'ps-row', ctx.currentStyle.display);
		ctx.currentBox.mainBlock = null;

		const rows: TableRow[] = [];
		let currentTR = ctx.currentElement as HTMLTableRowElement;
		const columnCount = Array.prototype.reduce.call(currentTR.cells, (value: number, cell: HTMLTableCellElement) => value + cell.colSpan, 0);
		const rowSpans: Map<number, CellRef> = new Map();
		const trByRow: Map<TableRow, HTMLTableRowElement> = new Map();
		const staticTr = walk.predicate(walk.isHTMLTag('tr'), walk.isStatic);
		do {
			let iCell = 0;
			const row: TableRow = [];
			for (let iCol = 0; iCol < columnCount; iCol++) {
				const precCell = iCol > 0 && row[iCol - 1];
				if (rowSpans.has(iCol)) {
					const cellRef = Object.create(rowSpans.get(iCol));
					boxMutations.setProp(cellRef, "rowSpan", cellRef.rowSpan - 1);
					row.push(cellRef);
					if (cellRef.rowSpan == 1) rowSpans.delete(iCol);
					else rowSpans.set(iCol, cellRef);
				} else if (precCell && precCell.colSpan > 1) {
					const cellRef = {cell: getCell(precCell), rowSpan: precCell.rowSpan, colSpan: precCell.colSpan - 1};
					row.push(cellRef);
					if (precCell.rowSpan > 1) rowSpans.set(iCol, cellRef);
				} else {
					const cell = currentTR.cells[iCell];
					if (walk.isStatic(cell)) {
						row.push(cell);
						if (cell.rowSpan > 1) rowSpans.set(iCol, {cell: cell, rowSpan: cell.rowSpan, colSpan: cell.colSpan});
						iCell++;
					}
				}
			}
			// Copy of the bottomOffset for the following lines
			if (rows.length) this.layoutContext.bottomOffsets.set(currentTR, this.layoutContext.bottomOffsets.get(ctx.currentElement));
			rows.push(row);
			trByRow.set(row, currentTR);
			currentTR = walk.nextSibling(currentTR, staticTr);
		} while (currentTR && rowSpans.size);

		const lastRow = trByRow.get(rows.at(-1));
		const pendingSpans = new Set<HTMLTableCellElement | CellRef>();
		const cellProcessors: Map<HTMLTableCellElement, CellLayoutProcessor> = new Map();
		for (let iRow = 0; iRow < rows.length; iRow++) {
			const row = rows[iRow];
			const tr = trByRow.get(row);

			const verticalStyleMut = new Mutations();
			for (const cell of row) if (!isCellRef(cell)) verticalStyleMut.setStyle(cell, {'vertical-align': 'top'});

			let invalid = false;
			for (const cell of row) {
				if (!isCellRef(cell) && walk.firstChild(cell, walk.isStaticNode)) {
					const cellProcessor = new CellLayoutProcessor(cell, this);
					cellProcessors.set(cell, cellProcessor);
					await cellProcessor.start().ended;
					if (cellProcessor.lastEvent == 'no-break-point') {
						invalid = true;
						break;
					}
				}
			}

			verticalStyleMut.revert();

			if (invalid) {
				for (const cell of row) if (!isCellRef(cell) && cellProcessors.has(cell)) cellProcessors.get(cell).layoutContext.mutations.revert();
				if (iRow > 0) {
					let iBreakRow = iRow;
					while (iBreakRow > 0) {
						// Search of avoided break on cells with rowspan that may involve reverting the previous lines
						let avoidedBreakRowSpanCell = null;
						let minBreakRow = iBreakRow;
						for (const cell of rows[iBreakRow]) {
							const rowSpanDelta = getCell(cell).rowSpan - cell.rowSpan;
							if (rowSpanDelta && (!avoidedBreakRowSpanCell || cell.rowSpan > avoidedBreakRowSpanCell.rowSpan)) {
								if (testAvoidBreakInside(getCell(cell), this.layoutContext, this.avoidBreakTypes, ranges.positionBefore(trByRow.get(rows[iBreakRow])), true)) {
									const cellBreakRow = iBreakRow - rowSpanDelta;
									if (cellBreakRow < minBreakRow) {
										avoidedBreakRowSpanCell = cell;
										minBreakRow = cellBreakRow;
									}
								}
							}
						}
						iBreakRow = minBreakRow;
						// Revert of the mutations to the breaked line
						for (let i = iRow; i >= iBreakRow; i--) {
							for (const cell of rows[i]) if (!isCellRef(cell) && cellProcessors.has(cell)) cellProcessors.get(cell).layoutContext.mutations.revert();
						}

						if (iBreakRow == 0) {
							return {event: 'no-break-point' as const};
						} else if (!avoidedBreakRowSpanCell) {
							// Break possible on a previous row
							const breakRow = rows[iBreakRow];
							const breakTr = trByRow.get(breakRow);
							insertRowSpans(boxMutations, breakRow, breakTr, cellProcessors);
							if (pendingSpans.size) {
								return {event: 'valid-break-point' as const, breakPoint: ranges.positionBefore(breakTr)};
							} else {
								ctx.nodes.currentNode = breakTr;
								break;
							}
						}
					}
				}
				ctx.nodes.currentNode = lastRow;
				return false;
			} else {
				for (const cell of row) {
					const baseCell = getCell(cell);
					if (cellProcessors.has(baseCell)) cellProcessors.get(baseCell).layoutContext.mutations.commit(boxMutations);
				}
				let overflow = false;
				for (const cell of row) {
					if (cell.rowSpan > 1) pendingSpans.add(getCell(cell));
					else {
						const cellProcessor = cellProcessors.get(getCell(cell));
						if (cellProcessor && cellProcessor.lastEvent != 'no-overflow') {
							overflow = true;
							pendingSpans.clear();
							break;
						}
					}
				}

				// TODO Use breakAtPoint to duplicate for the row ?
				if (overflow && !pendingSpans.size) {
					const destRow = tr.cloneNode(false);
					boxMutations.setAttr(tr, 'ps-breaked-after');
					boxMutations.setAttr(destRow, 'ps-breaked-before');
					boxMutations.insertNode(tr.parentElement, destRow, tr.nextSibling);
					tr.parentNode.insertBefore(destRow, tr.nextSibling);
					for (const cell of row) {
						const baseCell = getCell(cell);
						const cellProcessor = cellProcessors.get(baseCell);
						const breakPoint = cellProcessor ? cellProcessor.layoutContext.breakPoint : ranges.positionAtStart(baseCell);
						const cellAfter = boxMutations.breaks(baseCell, breakPoint);
						boxMutations.setProp(cellAfter, 'rowSpan', cell.rowSpan);
						if (cell == baseCell) boxMutations.removeAttr(baseCell, 'rowspan');
						else boxMutations.setProp(cell, "rowSpan", 1);
						destRow.appendChild(cellAfter);
						if (!walk.firstChild(cellAfter, walk.isStaticNode)) boxMutations.setAttr(baseCell, 'ps-empty-after');
					}
					return {event: 'valid-break-point' as const, breakPoint: ranges.positionBefore(destRow)};
				}
			}
			ctx.nodes.currentNode = trByRow.get(row);
		}
	})();
};

function insertRowSpans(mutations: Mutations, row: TableRow, tr: HTMLTableRowElement, cellProcessors: Map<HTMLTableCellElement, CellLayoutProcessor>): void {
	let insertRef: HTMLTableCellElement = null;
	for (let iCell = row.length - 1; iCell >= 0; iCell--) {
		let cell = row[iCell];
		if (isCellRef(cell)) {
			if (cell.cell.rowSpan > 1) {
				// In the case of a cellRef with a colspan, we iterate to the first cellRef of this cell
				let precCell = iCell > 0 && row[iCell - 1];
				while (precCell && isCellRef(precCell) && precCell.cell == cell.cell) {
					iCell--;
					cell = precCell;
					precCell = iCell > 0 && row[iCell - 1];
				}
				const cellProcessor = cellProcessors && cellProcessors.get(cell.cell);
				const breakPoint = cellProcessor ? cellProcessor.layoutContext.breakPoint : ranges.positionAtEnd(cell.cell);
				const cellAfter = mutations.breaks(cell.cell, breakPoint);
				mutations.setProp(cellAfter, 'rowSpan', cell.rowSpan);
				tr.insertBefore(cellAfter, insertRef);
				if (!walk.firstChild(cellAfter, walk.isStaticNode)) mutations.setAttr(cell.cell, 'ps-empty-after');
				insertRef = cellAfter;
			} else {
				insertRef = cell.cell;
			}
		} else {
			insertRef = cell;
		}
	}
}
