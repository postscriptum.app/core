// noinspection CssUnresolvedCustomProperty,CssInvalidFunction,CssInvalidPseudoSelector

export default (): string => /* language=CSS */ `
	@page {
		--ps-page-margin-top: calc(var(--ps-page-width) * 10 / 100);
		--ps-page-margin-right: calc(var(--ps-page-width) * 10 / 100);
		--ps-page-margin-bottom: calc(var(--ps-page-width) * 10 / 100);
		--ps-page-margin-left: calc(var(--ps-page-width) * 10 / 100);
		-ps-areas: footnotes;
	}

	::footnote-call {
		content: target-counter(attr(href), footnote);
		vertical-align: super;
		font-size: 60%;
	}

	::footnote-marker {
		content: counter(footnote) ".\\A0";
	}

	ps-area[ps-name='footnotes'] > [ps-float] {
		counter-increment: footnote;
	}

	h1 {
		bookmark-level: 1;
	}

	h2 {
		bookmark-level: 2;
	}

	h3 {
		bookmark-level: 3;
	}

	h4 {
		bookmark-level: 4;
	}

	h5 {
		bookmark-level: 5;
	}

	h6 {
		bookmark-level: 6;
	}

	h1, h2, h3, h4, h5, h6 {
		break-after: avoid;
		break-inside: avoid;
	}

	body[ps-process] {
		margin: 0;
	}

	[ps-break-before='avoid'] {
		break-after: avoid;
	}

	[ps-break-after='avoid'] {
		break-after: avoid;
	}

	[ps-break-before='page'] {
		break-after: page;
	}

	[ps-break-after='page'] {
		break-after: page;
	}

	[ps-break-inside='avoid'] {
		break-inside: avoid;
	}

	td {
		box-decoration-break: clone;
	}
`;
