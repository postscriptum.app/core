// noinspection CssUnresolvedCustomProperty,CssInvalidFunction,CssInvalidPropertyValue

const customProps: Partial<PropertyDefinition>[] = [
	{
		name: '--ps-size',
		syntax: '*',
		initialValue: 'auto'
	},
	{
		name: '--ps-page',
		syntax: 'auto | <custom-ident>',
		initialValue: 'auto'
	},
	{
		name: '--ps-break-before',
		syntax: 'auto | avoid | always | all | avoid-page | page | left | right | recto | verso | avoid-column | column',
		initialValue: 'auto'
	},
	{
		name: '--ps-break-after',
		syntax: 'auto | avoid | always | all | avoid-page | page | left | right | recto | verso | avoid-column | column',
		initialValue: 'auto'
	},
	{
		name: '--ps-break-inside',
		//syntax: 'auto | avoid | avoid-page | avoid-column',
		// * to allow the -ps-avoid-if-below function
		syntax: '*',
		initialValue: 'auto'
	},
	{
		name: '--ps-position',
		syntax: '*',
		initialValue: 'static'
	},
	{
		name: '--ps-float',
		syntax: '*',
		initialValue: 'none'
	},
	{
		name: '--ps-float-display',
		syntax: 'block | inline | compact',
		initialValue: 'block'
	},
	{
		name: '--ps-float-policy',
		syntax: 'auto | line | block',
		initialValue: 'auto'
	},
	{
		name: '--ps-bookmark-level',
		syntax: 'none | <integer>',
		initialValue: 'none'
	},
	{
		name: '--ps-bookmark-label',
		syntax: '*',
		initialValue: 'content(text)'
	},
	{
		name: '--ps-bookmark-state',
		syntax: 'open | closed',
		initialValue: 'open'
	},
	{
		name: '--ps-column-count',
		syntax: '<integer> | auto',
		initialValue: 'auto'
	},
	{
		name: '--ps-column-width',
		syntax: 'auto | <length>',
		initialValue: 'auto'
	},
	{
		name: '--ps-column-gap',
		syntax: 'normal | <length-percentage>',
		initialValue: 'normal'
	},
	{
		name: '--ps-column-fill',
		syntax: 'auto | balance',
		initialValue: 'balance'
	},
	{
		name: '--ps-column-rule-style',
		syntax: 'none | hidden | dotted | dashed | solid | double | groove | ridge | inset | outset',
		initialValue: 'none'
	},
	{
		name: '--ps-column-rule-width',
		syntax: '<length> | thin | medium | thick',
		initialValue: 'medium'
	},
	{
		name: '--ps-column-rule-color',
		syntax: '<color>',
		initialValue: 'currentcolor'
	},
	{
		name: '--ps-column-span',
		syntax: 'none | all',
		initialValue: 'none'
	},
	{
		name: '--ps-column-span-break',
		syntax: 'element | box',
		initialValue: 'element'
	},
	{
		name: '--ps-repeat-thead',
		syntax: 'repeat | no-repeat',
		initialValue: 'repeat'
	},
	{
		name: '--ps-box-decoration-break',
		syntax: 'slice | clone',
		initialValue: 'slice'
	},
	{
		name: '--ps-box-remaining-extent',
		syntax: 'fill | auto',
		initialValue: 'auto'
	},
	{
		name: '--ps-margin-break',
		syntax: 'auto | keep | discard',
		initialValue: 'auto'
	},
	{
		name: '--ps-areas',
		syntax: 'auto | <custom-ident>+',
		initialValue: 'auto'
	},
	{
		name: '--ps-string-set',
		syntax: '*',
		initialValue: 'none'
	},
	{
		name: '--ps-label',
		syntax: '*',
		initialValue: 'none',
		inherits: true
	},
	{
		name: '--ps-grow',
		syntax: '<number>',
		initialValue: '1',
		inherits: true
	}
];

function declareCustomProps(registerProperties: boolean, doc: Document): string {
	if (registerProperties) {
		for (const prop of customProps) {
			if (!('inherits' in prop)) prop.inherits = false;
			doc.defaultView.CSS.registerProperty(prop as Required<PropertyDefinition>);
		}
		return '';
	} else {
		let rule = "*::backdrop {";
		for (const prop of customProps) {
			rule += `${prop.name}: ${prop.initialValue};`;
		}
		rule += "}";
		return rule;
	}
}

export default (defaultPageSize: [string, string], registerProperties: boolean, doc: Document): string => {
	let css = declareCustomProps(registerProperties, doc);
	if (document.compatMode == 'BackCompat') css += `body { height: fit-content; }`;
	css += /* language=CSS */ `
		:root {
			/* Firefox does not support orphans and widows, we use CSS variables for them */
			--ps-orphans: 2;
			--ps-widows: 2;
			/* The defaut hyphen character of Chrome is the hyphen-minus (U+002D). PS uses the hyphen (U+2010) to allow detection. */
			-webkit-hyphenate-character: '\\2010';
			text-rendering: optimizeLegibility;
		}

		ps-page {
			display: flow-root;

			counter-increment: page;
			box-sizing: border-box;
			page-break-before: always;

			position: relative;
			overflow: clip;

			--ps-page-width: ${defaultPageSize[0]};
			--ps-page-height: ${defaultPageSize[1]};
			--ps-page-sheet-width: var(--ps-page-width);
			--ps-page-sheet-height: var(--ps-page-height);
			width: var(--ps-page-sheet-width);
			height: var(--ps-page-sheet-height);

			--ps-bleed: auto;
			--ps-marks: none;
			--ps-size: auto;
			padding-top: var(--ps-page-margin-top);
			padding-right: var(--ps-page-margin-right);
			padding-bottom: var(--ps-page-margin-bottom);
			padding-left: var(--ps-page-margin-left);
			margin-right: auto;
		}

		ps-page[ps-bleed] {
			border: solid var(--ps-bleed-size, 0) transparent;
			overflow-clip-margin: var(--ps-bleed-size);
			--ps-page-sheet-width: calc(var(--ps-page-width) + var(--ps-bleed-size) + var(--ps-bleed-size));
			--ps-page-sheet-height: calc(var(--ps-page-height) + var(--ps-bleed-size) + var(--ps-bleed-size));
		}

		ps-page[ps-bleed]:not([ps-bleed="none"])::before {
			content: "";
			position: absolute;
			--ps-bleed-inv: calc(var(--ps-bleed-size) * -1);
			top: var(--ps-bleed-inv);
			right: var(--ps-bleed-inv);
			bottom: var(--ps-bleed-inv);
			left: var(--ps-bleed-inv);

			background-image: paint(marks);
			background-repeat: no-repeat;
		}

		@page {
			margin: 0cm !important;
		}

		ps-page:first-of-type {
			page-break-before: auto;
			counter-reset: page 0 footnote 0;
		}

		ps-page-area {
			display: flex;
			flex-direction: column;
			height: 100%;
			box-sizing: border-box;
		}

		ps-area {
			overflow: hidden;
			position: relative;
		}

		ps-area[ps-name=footnotes] {
			overflow: visible;
		}

		ps-area:empty {
			display: none;
		}

		[ps-page-body] {
			display: block;
			flex: 1 1 0;
			position: relative;
			/* TODO doubts about this margin, but consistent with PrinceXML */
			margin: 0;
			order: 0;
			min-height: 0;
		}

		ps-page[ps-processing=content] > ps-margin {
			display: none;
		}

		ps-page[ps-processing=content] > ps-page-area > ps-area {
			visibility: hidden;
		}

		ps-margin {
			display: flex;
			position: absolute;
			z-index: 1;
			box-sizing: border-box;
		}

		ps-margin[ps-side=top] {
			top: 0;
			left: var(--ps-page-margin-left);
			right: var(--ps-page-margin-right);
			height: var(--ps-page-margin-top);
		}

		ps-margin[ps-side=right] {
			right: 0;
			top: var(--ps-page-margin-top);
			bottom: var(--ps-page-margin-bottom);
			width: var(--ps-page-margin-right);
			flex-direction: column;
		}

		ps-margin[ps-side=bottom] {
			bottom: 0;
			right: var(--ps-page-margin-right);
			left: var(--ps-page-margin-left);
			height: var(--ps-page-margin-bottom);
		}

		ps-margin[ps-side=left] {
			left: 0;
			top: var(--ps-page-margin-top);
			bottom: var(--ps-page-margin-bottom);
			width: var(--ps-page-margin-left);
			flex-direction: column;
		}

		ps-margin-box {
			display: flex;
			flex: 1 1 content;
			--ps-direction: row;
			--ps-justify: center;
			align-items: center;
			flex-direction: var(--ps-direction);
			justify-content: var(--ps-justify);
			text-align: var(--ps-justify);
			--ps-margin-size: none;
			position: relative;
		}

		ps-page[ps-processing] > ps-margin > ps-margin-box {
			flex-grow: 0;
		}

		ps-margin-box:not(:empty)::before {
			content: none !important;
		}

		ps-margin-box[ps-name=top-left-corner] {
			position: absolute;
			top: 0;
			left: 0;
			width: var(--ps-page-margin-left);
			height: var(--ps-page-margin-top);
			--ps-justify: end;
		}

		ps-margin-box[ps-name=top-right-corner] {
			position: absolute;
			top: 0;
			right: 0;
			width: var(--ps-page-margin-right);
			height: var(--ps-page-margin-top);
			--ps-justify: start;
		}

		ps-margin-box[ps-name=bottom-right-corner] {
			position: absolute;
			right: 0;
			bottom: 0;
			height: var(--ps-page-margin-bottom);
			width: var(--ps-page-margin-right);
			--ps-justify: start;
		}

		ps-margin-box[ps-name=bottom-left-corner] {
			position: absolute;
			left: 0;
			bottom: 0;
			height: var(--ps-page-margin-bottom);
			width: var(--ps-page-margin-left);
			--ps-justify: end;
		}

		ps-margin-box[ps-name^=left-],
		ps-margin-box[ps-name^=right-] {
			--ps-direction: column;
		}

		ps-margin-box[ps-name$=-left],
		ps-margin-box[ps-name$=-top] {
			--ps-justify: start;
		}

		ps-margin-box[ps-name$=-right],
		ps-margin-box[ps-name$=-bottom] {
			--ps-justify: end;
		}

		[ps-float-base] {
			display: none !important;
		}

		[ps-float-call] {
			color: inherit;
			text-decoration: inherit;
			unicode-bidi: isolate;
		}

		/* cf https://drafts.csswg.org/css-gcpm/#float-call */
		[ps-float-display='block'] {
			display: list-item;
			list-style-position: inside;
			list-style-type: none;
		}

		/* TODO */
		[ps-float-display='inline'] {
		}

		[ps-float-display='compact'] {
			display: inline-flex;
		}

		[ps-float-call], [ps-float-call]::before {
			all: unset;
		}

		ps-bookmarks {
			display: block;
		}

		ps-bookmarks[hidden] {
			position: absolute;
			height: 0;
			width: 0;
			overflow: hidden;
		}

		ps-bookmarks ol {
			list-style: none;
		}

		ps-bookmarks a::after {
			content: var(--ps-label);
		}

		[ps-running] {
			display: none !important;
		}

		[ps-breaked-before] {
			counter-increment: none !important;
			text-indent: 0 !important;
		}

		[ps-breaked-before]:not([ps-box-decoration-break=clone]) {
			padding-top: 0 !important;
			border-top: none !important;
			border-top-left-radius: 0 !important;
			border-top-right-radius: 0 !important;
		}

		[ps-breaked-after]:not([ps-box-decoration-break=clone]) {
			padding-bottom: 0 !important;
			border-bottom: none !important;
			border-bottom-left-radius: 0 !important;
			border-bottom-right-radius: 0 !important;
		}

		[ps-margin-break-before=discard] {
			margin-top: 0 !important;
		}

		[ps-breaked-after]:not([ps-margin-break-after=keep]) {
			margin-bottom: 0 !important;
		}

		/* TODO Is there some case where a counter-reset in not overridden ? */
		/*
		[ps-breaked-before]:not([ps-counters]) {
		  counter-reset: none !important;
		  counter-increment: none !important;
		}
		*/

		li[ps-breaked-before] {
			list-style-type: none !important;
			list-style-image: none !important;
		}

		[ps-breaked-before]::before,
		[ps-breaked-before]::marker {
			content: none !important;
		}

		[ps-breaked-after]::after {
			content: none !important;
		}

		[ps-counters-on-desc]::before {
			content: '' !important;
			counter-reset: var(--ps-counters-on-desc);
		}

		[ps-row=table-row] > *[ps-breaked-before] {
			vertical-align: top;
		}

		[ps-row=table-row] > *[ps-breaked-after]:not([ps-empty-after]) {
			vertical-align: bottom;
		}

		[ps-row=table-row] > *[ps-breaked-after][ps-breaked-before] {
			vertical-align: middle;
		}

		/* Font kerning is disabled to avoid invalid leader position on pdf rendering */
		[ps-has-leader] {
			font-kerning: none;
		}

		ps-leader {
			-moz-user-select: none;
			-ms-user-select: none;
			-webkit-user-select: none;
			direction: rtl;
			text-align: right;
			white-space: pre;
			display: inline-block;
			position: relative;
		}

		ps-leader[ps-direction=ltr] {
			direction: ltr;
			text-align: left;
		}

		ps-leader-end {
			visibility: hidden;
		}

		[ps-columns] {
		}

		ps-columns {
			display: flex;
		}

		ps-column {
			display: flex;
			flex-direction: column;
			flex: 1;
			min-width: 0;
		}

		ps-column-body {
			display: block;
		}

		ps-column > ps-column-body {
			flex: 1 1 0;
			min-height: 0;
		}

		ps-column-gap {
			margin: 0 calc(var(--ps-gap) / 2);
			position: relative;
		}

		ps-column-rule {
			position: absolute;
			top: 0;
			bottom: 0;
			border-left: var(--ps-rule);
			left: 50%;
			transform: translate(-50%, 0);
		}


		ps-columns[ps-process=ended] > ps-column > ps-column-body {
			min-height: auto;
		}

		ps-columns[ps-process=pending] > ps-column:not([ps-processing]),
		ps-columns[ps-process=pending] > ps-column-rule {
			visibility: hidden;
		}

		/* TODO Chrome bug report? The display property of noscript tags is 'inline' */
		noscript {
			display: none;
		}

		/* Decoration are cloned by default on cells in the case of a repeated thead */
		td::backdrop {
			--ps-box-decoration-break: clone;
		}

		[ps-baseline-loc] {
			all: initial !important;
		}
	`;
	return css;
};
