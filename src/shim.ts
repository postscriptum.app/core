/**
 * Minimal shim for css selection and ranges manipulation.
 *
 * @param window  The context window to shim
 */
export default function (window: any): void {
	//cssTypedOm(window);

	// Element.prototype.matches is not supported by Edge
	if (!window.Element.prototype.matches && window.Element.prototype.msMatchesSelector) {
		window.Element.prototype.matches = window.Element.prototype.msMatchesSelector;
	}

	// Range.prototype.isPointInRange and Range.prototype.intersectsNode are not supported by Edge
	if (!window.Range.prototype.isPointInRange) {
		window.Range.prototype.isPointInRange = function (referenceNode: Node, offset: number): boolean {
			const pointRange = referenceNode.ownerDocument.createRange();
			pointRange.setStart(referenceNode, offset);
			return pointRange.compareBoundaryPoints(Range.START_TO_START, this) >= 0 && pointRange.compareBoundaryPoints(Range.END_TO_END, this) <= 0;
		};
	}
	if (!window.Range.prototype.intersectsNode) {
		window.Range.prototype.intersectsNode = function (referenceNode: Node): boolean {
			const range = referenceNode.ownerDocument.createRange();
			range.selectNode(referenceNode);
			return range.compareBoundaryPoints(Range.START_TO_END, this) >= 0 || range.compareBoundaryPoints(Range.END_TO_END, this) <= 0;
		};
	}

	class ShimCaretPosition implements CaretPosition {
		readonly offset: number;
		readonly offsetNode: Node;

		constructor(range: Range) {
			this.offsetNode = range.startContainer;
			this.offset = range.startOffset;
		}

		getClientRect(): DOMRect | null {
			return null;
		}
	}

	// Chrome use the non-standard carttRangeFromPosition
	// Ticket of the implementation of caretPositionFromPoint: https://bugs.chromium.org/p/chromium/issues/detail?id=388976
	if (!window.Document.prototype.caretPositionFromPoint && window.Document.prototype.caretRangeFromPoint) {
		window.Document.prototype.caretPositionFromPoint = function (x: number, y: number): CaretPosition {
			const range = this.caretRangeFromPoint(x, y);
			if (!range) return null;
			return new ShimCaretPosition(range);
		};
	}
}

const uas = navigator.userAgent.match(/\b\w+\/[\d.]+\b/g).map((ua) => ua.split('/'));
type BrowserVersions = { [key: string]: number };
const versions: BrowserVersions = uas.reduce((o: BrowserVersions, [name, value]) => {
	o[name] = parseInt(value);
	return o;
}, {});

const browserNamePriority = ['Edge', 'Chrome', 'Firefox', 'Safari'];
let browser: { name: string, version: number };
for (const name of browserNamePriority) {
	if (name in versions) {
		browser = {name, version: versions[name]};
		break;
	}
}
if (!browser) browser = {name: 'Unknow', version: NaN};
export {browser};
