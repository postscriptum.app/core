import {events, io, logger} from "./util.js";

export type ProcessEvent = string | { type: string, [key: string]: any };

export type Process = IterableIterator<Promise<any> | ProcessEvent>;

export interface ProcessorOptions {
	logLevel: "error" | "warn" | "info" | "debug" | "trace";
	pluginsUri: string | null;
}

export interface ProcessorEvents {
	'process-start': { processor: Processor };
	'process-end': { processor: Processor };
	'error': { error: Error };
}

export abstract class Processor<Options extends ProcessorOptions = ProcessorOptions, Events extends ProcessorEvents = ProcessorEvents>
	extends events.EventEmitter<Events> {

	static defaultOptions: ProcessorOptions = {
		logLevel: 'warn',
		pluginsUri: null
	};

	options: Options;
	protected _process: Process;

	protected _pluginsToLoad: Promise<void>[] = [];

	protected constructor(options?: Partial<Options>) {
		super();
		const defaultOptions = (this.constructor as typeof Processor).defaultOptions;
		this.options = io.mergeOptions(Object.create(defaultOptions), options);
		logger.level = this.options.logLevel;
	}

	protected _ended: Promise<void>;

	get ended(): Promise<void> {
		return this._ended;
	}

	use(pluginName: string, useOptions?: object): this {
		const initPlugin = (): void => {
			const plugin = pluginRegistry[pluginName];
			if (!plugin) throw new Error(`Plugin '${pluginName}' not found in the registry.`);
			const options = Object.create(plugin.defaultOptions || {});
			if (plugin.scriptOptions) io.mergeOptions(options, plugin.scriptOptions);
			if (useOptions) io.mergeOptions(options, useOptions);
			logger.info(`Plugin '${pluginName}' used with options ${JSON.stringify(options)}.`);
			plugin.initializer(this, options);
		};

		if (pluginName in pluginRegistry) initPlugin();
		else {
			let loadPromise: Promise<void>;
			if (this.options.pluginsUri) {
				logger.debug(`Loading ${pluginName} from ${this.options.pluginsUri}`);
				loadPromise = io.loadScript(new URL(pluginName + ".js", this.options.pluginsUri + "/").href);
			} else if (window.postscriptumBridge?.loadPlugin) {
				logger.debug(`Loading ${pluginName} from cli interface`);
				loadPromise = window.postscriptumBridge?.loadPlugin(pluginName);
			} else throw new Error("Plugin '" + pluginName + "' not found");
			this._pluginsToLoad.push(loadPromise.then(initPlugin));
		}

		return this;
	}

	start(): this {
		this._ended = this._start();
		this._ended.then(() => {
			this.emit({type: 'process-end', processor: this});
		});
		return this;
	}

	emitError(error: Error): void {
		if (!this.emit({type: 'error', error})) throw error;
	}

	protected async _start<KE extends keyof Events>(): Promise<void> {
		try {
			const pluginsToLoadCount = this._pluginsToLoad.length;
			if (pluginsToLoadCount) {
				logger.debug(`Awaiting ${pluginsToLoadCount} plugin(s) to load.`);
				await Promise.all(this._pluginsToLoad);
			}
			this.emit({type: 'process-start', processor: this});

			let tick = this._process.next();
			while (!tick.done) {
				let result;
				if (tick.value instanceof Promise) {
					// Promise yielded
					result = await tick.value;
				} else {
					// Event
					const eventObj = (typeof tick.value == 'string' ? {type: tick.value} : tick.value) as events.Event<KE, Events>;
					result = this.emit(eventObj);
					// Promise returned by event listener
					if (result instanceof Promise) result = await result;
				}
				tick = this._process.next(result);
			}
		} catch (error) {
			this.emitError(error);
		}
	}
}

export interface Plugin<Options> {
	(processor: Processor, options: Options, ...args: any[]): any;
}

export type PluginEntry<Options> = {
	initializer: Plugin<Options>
	defaultOptions: Options;
	scriptOptions: Options;
	script?: HTMLScriptElement;
};

export type PluginRegistry = { [pluginName: string]: PluginEntry<any> };
export const pluginRegistry: PluginRegistry = {};
