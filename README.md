# postscriptum-core

This javascript library is the core component of Postscriptum.

It transforms the HTML content into a paginated one, without relying on the browser fragmentation support.

## Postscriptum

Postscriptum is a solution to produce PDF from HTML with the help of CSS Print.

Please refer to [__https://postscriptum.app__](https://postscriptum.app) for a global picture of the project.

## Installation

*postscriptum-core* is downloadable from the [release page](https://gitlab.com/postscriptum.app/core/-/releases "Releases · Postscriptum / core · GitLab") of the project on GitLab. It is also available on [npm](https://www.npmjs.com/package/@postscriptum.app/core).

## Usage

To paginate the complete page as soon as possible, include the Postscriptum script in the page with the attribute `ps-paginate`.

```html
<script src="postscriptum.min.js" ps-paginate></script>
```


To start programmatically the pagination then the print:

```html
<script src="postscriptum.min.js"></script>
<script>
	document.addEventListener('DOMContentLoaded', async () => {
		await postscriptum.pagination({ source: document.body }).start().ended;
		print();
	});
</script>
```


## License

[MIT](https://gitlab.com/postscriptum.app/core/-/blob/master/LICENSE)