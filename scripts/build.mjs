import fsp from 'node:fs/promises';
import {exec} from 'node:child_process';
import {buildConfigs, hasBuildOptions, options, pkgDatas, projectDir, tscBin} from "./configs.mjs";
import * as esbuild from "esbuild";

const builds = buildConfigs.map((config) => esbuild.build(config));

if (!hasBuildOptions || options.types) {
	builds.push(new Promise((resolve, reject) => {
		exec(`${tscBin} --emitDeclarationOnly true --noEmit false --declaration true --declarationDir dist/d.ts --strict false`, {cwd: projectDir}, (error, stdout, stderr) => {
			if (stdout) console.log(stdout);
			if (stderr) console.error(stderr);
			if (error) reject(error);
			else resolve();
		});
	}));
}

if (!hasBuildOptions || options.hyph) {
	builds.push((async () => {
		await fsp.mkdir(`${projectDir}/dist/hyph`, {recursive: true});
		for (const file of await fsp.readdir(`${projectDir}/hyph`)) {
			const hyphFile = `${projectDir}/hyph/${file}`;
			if (file.endsWith('.json')) {
				const dict = JSON.parse(await fsp.readFile(hyphFile, 'utf-8'));
				await fsp.writeFile(`${projectDir}/dist/hyph/${file}`, JSON.stringify(dict));
			} else {
				await fsp.copyFile(hyphFile, `${projectDir}/dist/hyph/${file}`);
			}
		}
	})());
}

if (!hasBuildOptions || options.postcssPlugin) {
	builds.push((async () => {
		const pluginDir = `${projectDir}/dist/postcss-ps-precss`;
		await fsp.mkdir(pluginDir, {recursive: true});
		await fsp.writeFile(`${pluginDir}/package.json`, JSON.stringify({
			name: '@postscriptum.app/postcss-ps-precss',
			version: pkgDatas.version,
			type: 'module',
			main: 'index.mjs',
			dependencies: {
				'postcss': pkgDatas.devDependencies['postcss'],
				'postcss-value-parser': pkgDatas.devDependencies['postcss-value-parser'],
				'cssesc': pkgDatas.devDependencies['cssesc'],
			}
		}, null, 2));
	})());
}

await Promise.all(builds);


