import {Command} from 'commander';
import * as url from 'node:url';
import * as fs from 'node:fs';
import fsp from 'node:fs/promises';
import * as csso from 'csso';
import path from "node:path";

export const options = new Command()
	.option('--bundle')
	.option('--esm')
	.option('--hyph')
	.option('--types')
	.option('--plugins')
	.option('--postcss-plugin')
	.parse().opts();

export const hasBuildOptions = Object.keys(options).length > 0;

export const projectDir = url.fileURLToPath(new URL('..', import.meta.url));
export const pkgDatas = JSON.parse(await fsp.readFile(`${projectDir}/package.json`, 'utf8'));
const now = new Date();
const dateFormat = new Intl.DateTimeFormat('en-GB', {dateStyle: 'medium'});

const pluginPostscriptumGlobal = {
	name: 'postscriptum-global',
	setup(build) {
		build.onLoad({filter: /postscriptum\.ts$/}, (args) => {
			return {
				contents: fs.readFileSync(args.path, 'utf8') + `window.postscriptum = postscriptum`,
				loader: 'default',
			};
		});
	}
};

const pluginNoSourceMapFromNodeModules = {
	name: 'no-source-map-node-modules',
	setup(build) {
		build.onLoad({filter: /\/node_modules\//}, (args) => {
			if (args.path.endsWith('js')) {
				return {
					contents: fs.readFileSync(args.path, 'utf8') +
						'\n//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIiJdLCJtYXBwaW5ncyI6IkEifQ==',
					loader: 'default',
				};
			}
		});
	}
}

const pluginSameDirSources = {
	name: 'same-dir-sources',
	setup(build) {
		async function parseSourceMap(sourcemapFile) {
			const sourcemap = JSON.parse(await fsp.readFile(sourcemapFile, 'utf8'));
			sourcemap.sources = sourcemap.sources.map((source) => source.replace(/^(..\/)+src\//, ''));
			await fsp.writeFile(sourcemapFile, JSON.stringify(sourcemap, null, 2));
		}

		build.onEnd(async () => {
			if (build.initialOptions.outfile) {
				const outfile = `${projectDir}${build.initialOptions.outfile}.map`;
				await parseSourceMap(outfile);
			} else {
				const outdir = `${projectDir}${build.initialOptions.outdir}`;
				for (const file of await fsp.readdir(outdir)) {
					if (file.endsWith('.map')) await parseSourceMap(`${outdir}/${file}`);
				}
			}
		});
	}
}

const cssLangComment = '/* language=CSS */';
const pluginMinifyCss = {
	name: 'minify-css',
	setup(build) {
		build.onLoad({filter: /\.ts$/}, async (args) => {
			let contents = await fs.promises.readFile(args.path, 'utf8');
			let cssStartIdx = contents.indexOf(cssLangComment);
			while (cssStartIdx != -1) {
				cssStartIdx = contents.indexOf('`', cssStartIdx + cssLangComment.length) + 1;
				const cssEndIdx = contents.indexOf('`', cssStartIdx);
				const css = csso.minify(contents.substring(cssStartIdx, cssEndIdx)).css;
				contents = contents.substring(0, cssStartIdx) + css + contents.substring(cssEndIdx);
				cssStartIdx = contents.indexOf(cssLangComment, cssEndIdx);
			}
			return {
				contents,
				loader: 'ts'
			};
		})
	}
}

const pluginUtf8Bom = {
	name: 'utf8-bom',
	setup(build) {
		build.onEnd(async () => {
			async function writeBom(file) {
				await fsp.writeFile(file, "\ufeff" + await fsp.readFile(file, 'utf8'));
			}

			if (build.initialOptions.outfile) {
				await writeBom(build.initialOptions.outfile);
			} else {
				const outdir = `${projectDir}${build.initialOptions.outdir}`;
				for (const file of await fsp.readdir(outdir)) {
					if (file.endsWith('.js')) await writeBom(`${outdir}/${file}`);
				}
			}
		});
	}
}

export const baseConfig = {
	absWorkingDir: `${projectDir}`,
	bundle: true,
	splitting: false,
	sourcemap: true,
	minify: false,
	charset: 'utf8',
	treeShaking: true,
	target: 'es2022',
	legalComments: 'none',
	banner: {
		js: `/*!
 * ${pkgDatas.name} ${pkgDatas.version} (${dateFormat.format(now)})
 * ${pkgDatas.homepage}
 *
 * Includes:
 * - Postcss: http://postcss.org/ (MIT License)
 * - Hypher: https://github.com/bramstein/hypher (BSD license)
 *
 * Copyright © 2015-${now.getFullYear()} David Rivron
 * Released under the MIT license (https://opensource.org/licenses/mit-license.php))
 */`
	}
};

export const buildConfigs = [];

if (!hasBuildOptions || options.bundle) {
	buildConfigs.push(Object.assign(Object.create(baseConfig), {
		entryPoints: ['src/postscriptum.ts'],
		outfile: 'dist/postscriptum.js',
		format: 'iife',
		plugins: [
			pluginPostscriptumGlobal,
			pluginNoSourceMapFromNodeModules,
			pluginSameDirSources,
			pluginMinifyCss,
			pluginUtf8Bom
		]
	}));
}

if (!hasBuildOptions || options.esm) {
	buildConfigs.push(Object.assign(Object.create(baseConfig), {
		entryPoints: ['src/**/*.ts'],
		bundle: false,
		outdir: 'dist/esm',
		format: 'esm',
		plugins: [
			pluginNoSourceMapFromNodeModules,
			pluginSameDirSources,
			pluginMinifyCss,
			pluginUtf8Bom
		]
	}));
}

if (!hasBuildOptions || options.plugins) {
	buildConfigs.push(Object.assign(Object.create(baseConfig), {
		entryPoints: ['plugins/*.ts'],
		bundle: false,
		outdir: 'dist/plugins',
		format: 'iife',
		plugins: [
			pluginSameDirSources,
			pluginMinifyCss,
			pluginUtf8Bom
		]
	}));
}

if (!hasBuildOptions || options.postcssPlugin) {
	buildConfigs.push(Object.assign(Object.create(baseConfig), {
		entryPoints: ['src/precss.ts'],
		outfile: 'dist/postcss-ps-precss/index.mjs',
		sourcemap: false,
		minify: false,
		format: 'esm',
		platform: 'node',
		external: ['postcss', 'postcss-value-parser', 'cssesc']
	}));
}

export const tscBin = url.fileURLToPath(await import.meta.resolve('typescript/bin/tsc'));