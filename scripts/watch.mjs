import * as esbuild from "esbuild";
import {buildConfigs} from "./configs.mjs";

await Promise.all(buildConfigs.map((config) => {
	return esbuild.context(config).then((context) => context.watch())
}));