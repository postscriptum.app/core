#!/bin/bash
set -e
hyphDir=$(readlink -f $(dirname "$BASH_SOURCE")/../hyph)
rm -f "$hyphDir"/*.json
rm -f "$hyphDir"/*.README
tmpDir=$(tempfile)
rm $tmpDir
git clone https://github.com/bramstein/hyphenation-patterns $tmpDir
patternsDir=$tmpDir/patterns
for pattern in $tmpDir/patterns/*; do
  baseName=$(basename -s .js $pattern)
  if [ $baseName == "nb-no" ]; then baseName="nb";
  elif [ $baseName == "el-monoton" ]; then baseName="el";
  # Sorry for the polytonic Greek orthography
  elif [ $baseName == "el-polyton" ]; then continue; fi

  jsonFile="$hyphDir/$baseName.json"
  readmeFile="$hyphDir/$baseName.README"
  node -e "const lang = require('$pattern'); process.stdout.write(JSON.stringify(lang));" > "$jsonFile"
  grep '^//' $pattern | sed -E -e 's/^\/\/ (%( )?)?//g' > "$readmeFile"
  [ -s "$readmeFile" ] || rm "$readmeFile"
done
rm $tmpDir -Rf