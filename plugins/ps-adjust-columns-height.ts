import type {Postscriptum} from "../dist/d.ts/postscriptum";
import type {Columnizator} from "../dist/d.ts/columns";

interface PropertyDefinition {
	name: string;
	syntax: string;
	inherits: boolean;
	initialValue: string;
}

/**
 * Le cas d'usage par défaut est réduire la police jusqu'à trouver une solution valide
 * 'normal' comme valeur de layout pour indiquer un test sans layout mais avec critère
 * 'none' autorisé que comme valeur seul ou en fin : si en fin, il n'y a pas de test de validité au niveau de l'élément
 * la valeur 'auto' pour criteria indique "selected-before max-content min-height / no-inline-overflow" excépté si set de layout est 'normal' dans ce cas, il correspond à 'none'
 */
// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace CSS {
	function registerProperty(definition: PropertyDefinition): void
}

(postscriptum as Postscriptum).plugin<void>('ps-adjust-columns-height', (processor: Columnizator) => {
	const {css, util, mutations} = postscriptum;
	const {logger} = util;

	CSS.registerProperty({
		name: '--psp-ach-steps',
		syntax: '*',
		initialValue: 'none',
		inherits: false
	});


	processor.on("columnization-end", function (this: Columnizator) {
		if (this.source.closest('[psp-sl-layout-test]')) return;
		if (this.lastEvent == 'no-break-point') return;
		const sourceStyle = getComputedStyle(this.source);

		const stepsPropList = css.getCustomPluginProp(sourceStyle, 'ach-steps');
		if (stepsPropList == "none") return;
		const stepCounts = [];
		const animsList = stepsPropList.split(',').map((stepProp) => {
			const [keyframes, stepsCount, timing] = stepProp.trim().split(' ');
			stepCounts.push(parseInt(stepsCount));
			return `${stepsCount}s ${timing || 'linear'} paused ${keyframes}`;
		});
		const animsProp = animsList.join(',');


		const range = document.createRange();
		let maxBottom = -Infinity;
		let maxBottomColumn = null;
		const bottoms = [];
		for (let iCol = 0; iCol < this.columnCount; iCol++) {
			const column = this.fragments[iCol];
			if (column.body?.hasAttribute('ps-breaked-after')) {
				range.selectNodeContents(column.body);
				const contentBottom = range.getBoundingClientRect().bottom;
				bottoms[iCol] = contentBottom;
				if (contentBottom > maxBottom) {
					maxBottom = contentBottom;
					maxBottomColumn = column;
				}
			}
		}

		for (let iCol = 0; iCol < this.columnCount; iCol++) {
			const column = this.fragments[iCol];
			if (column.body && column != maxBottomColumn) {
				logger.addScope(`column ${column.number}`);
				const animMutations = new mutations.Mutations();
				animMutations.setStyle(column.body, {animation: animsProp});
				const anims = column.body.getAnimations() as CSSAnimation[];
				if (anims.length != animsList.length) {
					logger.warn(`Unable to define the animation 'animsProp'. Are some keyframes at-rules missing ?`);
					return;
				}
				const resultSteps = [];
				for (let i = 0; i < anims.length; i++) {
					const anim = anims[i];
					const steps = stepCounts[i];
					range.selectNodeContents(column.body);
					let lastStep = 0;
					let lastBottom = -Infinity;
					for (let step = 1; step <= steps; step++) {
						anim.currentTime = step * 1000 - 1;
						const bottom = range.getBoundingClientRect().bottom;
						if (bottom > maxBottom) break;
						lastStep = step;
						lastBottom = bottom;
					}
					if (lastStep && lastBottom > bottoms[iCol]) {
						anim.currentTime = lastStep * 1000 - 1;
						resultSteps.push(lastStep);
					} else {
						anim.cancel();
						resultSteps.push(0);
					}
				}
				const adjusted = resultSteps.filter((rs) => rs);
				if (adjusted.length) {
					this.layoutContext.mutations.setAnimations(column.body);
					logger.info("Column ajusted: " + anims.map((a, i) => `${a.animationName} ${resultSteps[i]}/${stepCounts[i]}`).join(', '));
				}
				animMutations.revert();
				logger.removeScope();
			}
		}

	}, {inherited: true});
});
