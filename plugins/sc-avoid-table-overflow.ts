import type {Postscriptum} from "../dist/d.ts/postscriptum.js";
import type {Page, Paginator} from "../dist/d.ts/pages";
import type {LogStringLevel} from "../dist/d.ts/util/log";

interface AvoidTableOverflowOptions {
	logLevel?: LogStringLevel;
}

/** Warning: the calculation of the height does not take into account the sub-processes (except on the breakedBox) */
(postscriptum as Postscriptum).plugin('sc-avoid-table-overflow', (processor: Paginator, options) => {
	const {util} = postscriptum;
	const {logger, walk, ranges, log} = util;

	processor.on('valid-break-point', function (this: Paginator): string | void {
		const {breakPoint} = this.layoutContext;
		const startNode = ranges.nodeAtPoint(breakPoint);
		const tables = [];
		let table = startNode;
		while ((table = walk.previous(table, walk.isHTMLTable, this.currentPage.body))) tables.push(table);
		if (tables.length) return testOverflow(this.currentPage, tables);
		else if (this.currentPage.container.hasAttribute('psp-sc-ato-rotate')) {
			this.currentPage.container.style.setProperty('--ps-page-width', '');
			this.currentPage.container.style.setProperty('--ps-page-height', '');
		}
	});

	processor.on('no-overflow', function (this: Paginator): string | void {
		const tables = this.currentPage.body.querySelectorAll('table');
		if (tables.length) return testOverflow(this.currentPage, tables);
	});

	function testOverflow(page: Page, tables: HTMLTableElement[] | NodeListOf<HTMLTableElement>): string {
		if (page.container.style.getPropertyValue('--ps-page-width')) return;

		let overflow = false;
		const parentWidths = new Map();
		for (const table of tables) {
			let parentWidth = parentWidths.get(table.parentElement);
			if (!parentWidth) {
				parentWidth = Math.ceil(parseFloat(getComputedStyle(table.parentElement).width));
				parentWidths.set(table.parentElement, parentWidth);
			}
			if (!overflow && (page.container.hasAttribute('psp-sc-ato-rotate') || table.clientWidth > parentWidth)) overflow = true;
		}

		if (overflow) {
			page.container.setAttribute('psp-sc-ato-rotate', '');
			const containerStyle = getComputedStyle(page.container);
			if (parseFloat(containerStyle.height) > parseFloat(containerStyle.width)) {
				const width = containerStyle.getPropertyValue('--ps-page-width');
				processor.layoutContext.mutations.revert();
				page.container.style.setProperty('--ps-page-width', containerStyle.getPropertyValue('--ps-page-height'));
				page.container.style.setProperty('--ps-page-height', width);
				logger.log(log.Logger.levelFromString(options.logLevel), 'Page rotated.', 'sc-avoid-table-overflow');
				return "detect-overflow";
			}
		}
	}
}, { logLevel: 'info' } as AvoidTableOverflowOptions);



