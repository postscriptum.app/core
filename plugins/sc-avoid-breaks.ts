import type {Postscriptum} from "../dist/d.ts/postscriptum";
import type {Paginator} from "../dist/d.ts/pages";

interface AvoidBreaksPluginOptions {
	afterColon?: boolean;
	afterStrongPara?: boolean;
	strongSelector?: string;
	avoidInsideStrong?: boolean;
}

(postscriptum as Postscriptum).plugin<AvoidBreaksPluginOptions>('sc-avoid-breaks', (processor: Paginator, options) => {
	const {logger, walk} = postscriptum.util;

	let afterColonCount = 0;
	let afterStrongParaCount = 0;

	if (options.afterColon || options.afterStrongPara) processor.on('prepare', function (this: Paginator, {node}): void {
		if (options.afterColon && node.nodeType == Node.TEXT_NODE) {
			// Paragraph ending with a colon
			const trimValue = node.nodeValue.trim();
			if (trimValue.at(- 1) == ":") {
				let parent = node;
				while (!walk.displayAsBlock(parent)) {
					if (parent != walk.lastChild(parent.parentElement, walk.isStaticNode)) return;
					parent = parent.parentElement;
				}

				(parent as Element).setAttribute('ps-break-after', 'avoid');
				afterColonCount++;
			}
		} else if (options.afterStrongPara && walk.isElement(node) && node.matches(options.strongSelector)) {
			// Strong paragraph
			const parent = node.parentElement;
			const blockNotLast = walk.displayAsBlock(parent) && walk.nextSibling(parent, walk.isStaticNode);
			if (blockNotLast && node == walk.firstChild(parent, walk.isStaticNode) && node == walk.lastChild(parent, walk.isStaticNode)) {
				parent.setAttribute('ps-break-after', 'avoid');
				if (options.avoidInsideStrong) parent.setAttribute('ps-break-inside', 'avoid');
				afterStrongParaCount++;
			}
		}
	});

	processor.on('pagination-start', () => {
		logger.addScope('sc-avoid-breaks');
		if (afterColonCount) logger.info(`${afterColonCount} break(s) avoided after colon.`);
		if (afterStrongParaCount) logger.info(`${afterStrongParaCount} break(s) after strong para.`);
		logger.removeScope();
	});
}, {
	afterColon: true,
	afterStrongPara: false,
	strongSelector: 'strong',
	avoidInsideStrong: true
} as AvoidBreaksPluginOptions);
