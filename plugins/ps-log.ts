import type {Postscriptum} from "../dist/d.ts/postscriptum";
import type {Page, Paginator, PaginatorEvents} from "../dist/d.ts/pages";

interface LogPluginOptions {
	logEvents: string[];
	logBreakPoints: boolean;
}

(postscriptum as Postscriptum).plugin<LogPluginOptions>('ps-log', (processor: Paginator, options) => {
	const {util} = postscriptum;
	const {logger} = util;

	if (options.logEvents) options.logEvents.forEach((eventName: keyof PaginatorEvents) => {
		processor.on(eventName, function (...args): void {
			if (args.length) logger.debug(eventName, args.join(','));
			else logger.debug(eventName);
		});
	});

	if (options.logBreakPoints) {
		const paths = [];
		processor.on('page-end', function ({page}: { page: Page }): void {
			const path = [];
			path.push(this.currentBreakPoint.offset);
			if (this.currentBreakPoint.container == page.body) [path];
			let current = this.currentBreakPoint.container.parentNode;
			while (current && page.body.contains(current)) {
				path.push(Array.prototype.indexOf.call(current.parentNode.childNodes, current));
				current = current.parentNode;
			}
			paths.push(path.reverse().join('/'));
		});
		processor.on('pagination-end', function (): void {
			logger.debug(paths.join(', '));
		});
	}
}, {
	logEvents: [
		'start', 'end', 'pagination-start', 'pagination-end', 'print-start', 'print-end', 'sequence-start', 'sequence-end',
		'page-start', 'page-end', 'overflow-detected', 'no-overflow', 'break-point-invalid',
		'valid-break-point', 'new-break-point-to-valid'
	],
	logBreakPoints: false
});
