import type {Postscriptum} from "../dist/d.ts/postscriptum";
import type {Paginator} from "../dist/d.ts/pages";

(postscriptum as Postscriptum).plugin('ps-print-preview', (processor: Paginator): void => {
	const { util, css } = postscriptum;
	const stylesheet = (destId): string => `@media screen {

	#${destId} {
		background-color: #ccc;
	}

	ps-page {
		margin: 1em auto;
		box-shadow: 0px 0px 20px #555;
		position: relative;
	}
	
	ps-page::before {
		content: "";
		position: absolute;
		top: 0;
		right: 0;
		bottom: 0;
		left: 0;
		background-color: white;
		z-index: -1;
	}
	

	ps-page:first-of-type {
		margin-top: 0.5em;
	}

	ps-page:last-of-type {
		margin-bottom: 0.5em;
	}
}`;

	processor.on('pagination-end', () => {
		const destId = util.dom.id(processor.dest);
		const doc = processor.source.ownerDocument;
		css.insertStyleSheet(doc, stylesheet(destId), true, 'ps-print-preview-' + destId, doc.head.firstChild);
	});
});
