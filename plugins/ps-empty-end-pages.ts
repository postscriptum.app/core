import type {Postscriptum} from "../dist/d.ts/postscriptum";
import type {Paginator} from "../dist/d.ts/pages";

interface PsEmptyPagesOnEndOptions {
	multipleOf?: number;
	beforeLast?: number | boolean;
	pageName?: string;
	minToAdd?: number;
	blank?: boolean
}

(postscriptum as Postscriptum).plugin<PsEmptyPagesOnEndOptions>('ps-empty-end-pages', (processor: Paginator, options) => {
	processor.on('pagination-end', function (this: Paginator) {
		let pagesToAdd = options.minToAdd;
		const remainder = (this.pages.length + pagesToAdd) % options.multipleOf;
		if (remainder != 0) pagesToAdd += options.multipleOf - remainder;

		if (options.beforeLast === true) options.beforeLast = 1;
		const beforePage = options.beforeLast ? this.pages[this.pages.length - options.beforeLast] : null;
		for (let i = 0; i < pagesToAdd; i++) {
			this.addStandalonePage(this.currentPage.body.cloneNode(false) as HTMLElement, options.pageName, options.blank, beforePage);
		}

		// Updates the global counter of the page count
		const destCounterReset = postscriptum.counters.parseCounterValue(getComputedStyle(this.dest).counterReset);
		destCounterReset.pages += pagesToAdd;
		this.dest.style.counterReset = Object.entries(destCounterReset).flat().join(' ');
	});
}, {multipleOf: 4, beforeLast: true, pageName: '', minToAdd: 0, blank: true});
