import type {Postscriptum} from "../dist/d.ts/postscriptum";
import type {Paginator} from "../dist/d.ts/pages";

type Timers = {
	process: number,
	preparation: number,
	pagination: number,
	print: number,
	page: number
};

interface PerformancesPluginOption {
	pagesDetails: boolean;
}

(postscriptum as Postscriptum).plugin('ps-performances', (paginator: Paginator, options: PerformancesPluginOption) => {
	const {layout, util} = postscriptum;
	const {logger} = util;

	const starts: Timers = {process: NaN, preparation: NaN, pagination: NaN, print: NaN, page: NaN};
	const durations: Timers = {process: NaN, preparation: NaN, pagination: NaN, print: NaN, page: 0};

	function formatTime(time: number): string {
		return Math.ceil(time) / 1000 + 's';
	}

	if (paginator.ended instanceof Promise) starts.process = performance.now();

	paginator
		.on('process-start', function () {
			starts.process = performance.now();
		})
		.on('process-end', function () {
			durations.process = performance.now() - starts.process;
			const messages = [
				`Process: ${formatTime(durations.process)}`,
				`preparation: ${formatTime(durations.preparation)}`,
				`pagination: ${formatTime(durations.pagination)} (${paginator.fragments.length} pages)`
			];
			if (options.pagesDetails) messages.push(`pages average: ${formatTime(durations.page)}`);
			logger.info(messages.join(', '));
		})
		.on('preparation-start', function () {
			starts.preparation = performance.now();
		})
		.on('preparation-end', function () {
			durations.preparation = performance.now() - starts.preparation;
		})
		.on('pagination-start', function () {
			starts.pagination = performance.now();
		})
		.on('pagination-end', function () {
			durations.pagination = performance.now() - starts.pagination;
		})
		.on('print-start', function () {
			starts.print = performance.now();
		})
		.on('print-end', function () {
			durations.print = performance.now() - starts.print;
			logger.info(`Print: ${formatTime(durations.print)}`);
		});
	if (options.pagesDetails) {
		paginator
			.on('page-start', function () {
				starts.page = performance.now();
			})
			.on('page-end', function () {
				const pageDuration = performance.now() - starts.page;
				durations.page += (pageDuration - durations.page) / paginator.currentFragment.number;
				console.log(`Page ${paginator.currentFragment.number}: ${formatTime(pageDuration)}, average: ${formatTime(durations.page)}`);
			});
	}
}, {pagesDetails: false});
