import type {Postscriptum} from "../dist/d.ts/postscriptum";
import type {Paginator} from "../dist/d.ts/pages";
import type {Fragmentor} from "../dist/d.ts/fragments";

/**
 * Plugin Scenari that shrinks or expands the images of a page to avoid blank area.
 *  - Requires images to be blocks.
 *  - The shrink algorithm handles the break points avoided inside ancestors,
 *    but does not check the validity of the new break point (the break-before of the next box is not known).
 *  - Should be included last.
 */
(postscriptum as Postscriptum).plugin('sc-avoid-image-blanks', (processor: Paginator) => {
	const {css, util, layout} = postscriptum;
	const {logger, rects, ranges, walk} = util;

	logger.warn("The plugin 'sc-avoid-image-blanks' is deprecated. Please replace it by the plugin 'sc-scale-to-fit'.");
	function process(this: Fragmentor, shrinkOnly = false): string {
		const ctx = this.layoutContext;
		if (!walk.isBlockElement(ranges.nodeAtPoint(ctx.breakPoint))) return;
		const {breakBox, boxes} = ctx;
		const breakBoxIndex = boxes.indexOf(breakBox);
		if (breakBoxIndex < 1 || !breakBox.mainBlock) return;

		const lastFullBox = boxes[breakBoxIndex - 1];
		const pageBodyRect = rects.boundingScrollRect(this.currentFragment.body);
		const minBlankHeightProp = css.getCustomPluginProp(this.currentFragment.container, 'sc-aib-min-blank-height');
		if (!minBlankHeightProp) return;
		const minBlankHeight = css.computeLength(minBlankHeightProp, pageBodyRect.height);
		const blankHeight = pageBodyRect.bottom - lastFullBox.bottom;
		if (blankHeight < minBlankHeight) return;

		const dimByImg: Map<HTMLElement, { width: number, height: number }> = new Map();
		const shrinkByImg: Map<HTMLElement, number> = new Map();
		let shrinkImgsHeight = 0;
		let shrinkImgsMinHeight = 0;

		const expandByImg: Map<HTMLElement, number> = new Map();
		let expandImgsHeight = 0;

		// TODO max-width not supported
		// TODO max-height not supported
		for (const box of boxes) {
			if (box.mainBlock) {
				const img = box.mainBlock;
				const shrinkProp = css.getCustomPluginProp(img, 'sc-aib-max-shrink');
				const expandProp = css.getCustomPluginProp(img, 'sc-aib-max-expand');
				if (shrinkProp || expandProp) {
					const style = getComputedStyle(img);
					const width = parseFloat(style.width);
					const height = parseFloat(style.height);
					dimByImg.set(img, {width, height});

					const parentStyle = getComputedStyle(img.parentElement);
					const parentWidth = parseFloat(parentStyle.width);
					const parentHeight = parseFloat(parentStyle.height);

					if (shrinkProp) {
						const minWidth = css.computeMinOrMaxLength(style.minWidth, parentWidth, 0);
						const minHeight = css.computeMinOrMaxLength(style.minHeight, parentHeight, 0);
						if (!isNaN(minWidth) && !isNaN(minHeight)) {
							const shrink = Math.max(minWidth / width, minHeight / height, parseFloat(shrinkProp));
							if (shrink < 1) {
								shrinkByImg.set(img, shrink);
								shrinkImgsHeight += height;
								shrinkImgsMinHeight += height * shrink;
							}
						}
					}

					if (!shrinkOnly && expandProp && box != breakBox) {
						// TODO Handles the padding on the page body
						const maxWidth = css.computeMinOrMaxLength(style.maxWidth, parentWidth, pageBodyRect.width);
						const maxHeight = css.computeMinOrMaxLength(style.maxHeight, parentHeight, pageBodyRect.height);
						if (!isNaN(maxWidth) && !isNaN(maxHeight)) {
							const expand = Math.min(maxWidth / width, maxHeight / height, parseFloat(expandProp));
							if (expand > 1) {
								expandByImg.set(img, expand);
								expandImgsHeight += height;
							}
						}
					}
				}
				if (box == breakBox) break;
			}
		}

		if (shrinkByImg.has(breakBox.mainBlock) && !breakBox.avoidBreakAfter) {
			let bottom = breakBox.bottom;
			let breakAvoidedParent = layout.testAvoidBreakInside(breakBox.mainBlock, ctx, this.avoidBreakTypes, ctx.breakPoint);
			if (breakAvoidedParent) {
				while (!walk.isStatic(breakAvoidedParent.nextElementSibling)) {
					const parent = breakAvoidedParent.parentElement;
					if (parent == ctx.root.parentNode) break;
					breakAvoidedParent = breakAvoidedParent.parentElement;
				}
				if (breakAvoidedParent != this.currentFragment.body) bottom = util.rects.boundingScrollRect(breakAvoidedParent).bottom;
			}
			let overflowHeight = bottom - pageBodyRect.bottom;
			if (overflowHeight > 0 && overflowHeight <= shrinkImgsHeight - shrinkImgsMinHeight) {
				const sortedByShrink = Array.from(shrinkByImg.keys()).sort((img1: HTMLElement, img2: HTMLElement): number => {
					return shrinkByImg.get(img2) * dimByImg.get(img2).height - shrinkByImg.get(img1) * dimByImg.get(img1).height;
				});
				let targetShrink = (shrinkImgsHeight - overflowHeight) / shrinkImgsHeight;
				for (const img of sortedByShrink) {
					const shrink = Math.max(targetShrink, shrinkByImg.get(img));
					const {width, height} = dimByImg.get(img);
					const ratio = width / height;
					const newHeight = height * shrink;
					img.style.height = newHeight + 'px';
					img.style.width = (newHeight * ratio) + 'px';
					shrinkImgsHeight -= height;
					overflowHeight -= (height - newHeight);
					targetShrink = (shrinkImgsHeight - overflowHeight) / shrinkImgsHeight;
				}
				ctx.breakPoint = breakAvoidedParent ? ranges.positionAfter(breakAvoidedParent) : ranges.endPoint(breakBox.range);
				ctx.breakBox = null;
				logger.info(`${shrinkByImg.size} images shrinked.`, 'sc-avoid-image-blanks');
				return 'break-point-found';
			}
		}

		if (expandByImg.size) {
			let underflowHeight = pageBodyRect.bottom - lastFullBox.bottom;
			const sortedByExpand = Array.from(expandByImg.keys()).sort((img1: HTMLElement, img2: HTMLElement): number => {
				return expandByImg.get(img2) * dimByImg.get(img2).height - expandByImg.get(img1) * dimByImg.get(img1).height;
			});
			let targetExpand = (expandImgsHeight + underflowHeight) / expandImgsHeight;
			for (const img of sortedByExpand) {
				const expand = Math.min(targetExpand, expandByImg.get(img));
				const {width, height} = dimByImg.get(img);
				const ratio = width / height;
				const newHeight = height * expand;
				img.style.height = newHeight + 'px';
				img.style.width = (newHeight * ratio) + 'px';
				expandImgsHeight -= height;
				underflowHeight -= (newHeight - height);
				targetExpand = (expandImgsHeight + underflowHeight) / expandImgsHeight;
			}
			logger.info(`${expandByImg.size} images expanded.`, 'sc-avoid-image-blanks');
			return 'break-point-found';
		}
	}

	processor.on('valid-break-point', function (this: Fragmentor): string {
		return process.call(this);
	});
	processor.on('no-break-point', function (this: Fragmentor): string {
		return process.call(this, true);
	});

});


