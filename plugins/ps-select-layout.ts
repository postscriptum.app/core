import type {Postscriptum} from "../dist/d.ts/postscriptum";
import type {Paginator} from "../dist/d.ts/pages";
import type {Areas, LayoutContext, LayoutProcessorEvents, LayoutProcessorOptions, SubFragmentor, SubProcessorContext} from "../dist/d.ts/layout";
import type {Fragmentor} from "../dist/d.ts/fragments";
import type {Process} from "../dist/d.ts/process";
import type {SubProcessorResult} from "../src/layout.js";

interface PropertyDefinition {
	name: string;
	syntax: string;
	inherits: boolean;
	initialValue: string;
}

/**
 * Le cas d'usage par défaut est réduire la police jusqu'à trouver une solution valide
 * 'normal' comme valeur de layout pour indiquer un test sans layout mais avec critère
 * 'none' autorisé que comme valeur seul ou en fin : si en fin, il n'y a pas de test de validité au niveau de l'élément
 * la valeur 'auto' pour criteria indique "selected-before max-content min-height / no-inline-overflow" excépté si set de layout est 'normal' dans ce cas, il correspond à 'none'
 */
// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace CSS {
	function registerProperty(definition: PropertyDefinition): void
}

type LayoutResult = {
	layout: string;
	score: number;
	step?: number;
	animProp?: string;
}
type ValidFlags = ('none' | 'no-inline-overflow' | 'stop-on-valid-step')[];

type StepStops = 'first-valid' | 'first-invalid';

interface CritContext {
	layoutBefore?: string;
	stepBefore?: number;
	animBefore?: string;
}

(postscriptum as Postscriptum).plugin<void>('ps-select-layout', (processor: Paginator) => {
	const {css, layout, util, mutations} = postscriptum;
	const {walk, logger} = util;

	CSS.registerProperty({
		name: '--psp-sl-layouts',
		syntax: '*',
		initialValue: 'normal',
		inherits: false
	});

	CSS.registerProperty({
		name: '--psp-sl-criteria',
		syntax: '*',
		initialValue: 'valid',
		inherits: false
	});

	CSS.registerProperty({
		name: '--psp-sl-steps',
		syntax: '*',
		initialValue: 'none',
		inherits: false
	});

	interface TestLayoutProcessEvents extends LayoutProcessorEvents {
		'inline-overflow': Record<string, never>;
	}

	class TestLayoutProcessor extends layout.LayoutProcessor<LayoutProcessorOptions, TestLayoutProcessEvents> implements SubFragmentor {
		limitedFloats: boolean;

		constructor(source: HTMLElement, readonly parentFragmentor: Fragmentor, readonly captureInlineOverflow: boolean) {
			super(source, {logLevel: parentFragmentor.options.logLevel});

			this.gcpmContext = parentFragmentor.gcpmContext;
			this.avoidBreakTypes = parentFragmentor.avoidBreakTypes;
			this.logNoValidBreakPoint = false;
			this.startOnRoot = true;
			this.subProcessors = Array.from(parentFragmentor.subProcessors);
			this.parentEmitter = parentFragmentor;
			this._process = this.testProcess();
		}

		* testProcess(): Process {
			const parentCtx = this.parentFragmentor.layoutContext;
			const logLevel = this.options.logLevel;
			logger.level = 'error';

			let inlineOverflow = false;
			if (this.captureInlineOverflow) {
				this.on("contents-end", function (this: Fragmentor) {
					const currentBody = this.currentFragment?.body || this.source;
					if (!inlineOverflow && currentBody.scrollWidth > currentBody.clientWidth) inlineOverflow = true;
				}, { inherited: true });
			}

			yield* this.contentsProcess(this.source, parentCtx.areas, parentCtx);
			logger.level = logLevel;

			if (inlineOverflow) this.lastEvent = "inline-overflow";
		}

		createLayoutContext(root: HTMLElement, areas: Areas, parentCtx: LayoutContext): LayoutContext {
			class ElementLayoutContext extends layout.LayoutContext {
				get bodyBottom(): number {
					return parentCtx.bodyBottom;
				}

				set bodyBottom(val) {
					parentCtx.bodyBottom = val;
				}
			}

			return new ElementLayoutContext(root, parentCtx.body, areas, parentCtx);
		}
	}

	type CritFunction = (subProcessor: TestLayoutProcessor, ctx: CritContext) => number;

	const CRIT_FUNCTIONS: Record<string, CritFunction> = {
		'valid': function () {
			return Infinity;
		},
		'selected-before': function (subProcessor: TestLayoutProcessor, ctx: CritContext): number {
			const layout = subProcessor.source.getAttribute('psp-sl-layout');
			const stepAttr = subProcessor.source.getAttribute('psp-sl-step');
			const step = stepAttr ? parseInt(stepAttr) : undefined;
			if (ctx.layoutBefore == layout && ctx.stepBefore == step) return Infinity;
			return 0;
		},
		'max-content-on-break': function (subProcessor: TestLayoutProcessor): number {
			if (subProcessor.lastEvent != "valid-break-point") return 0;
			const {breakPoint} = subProcessor.layoutContext;
			const range = document.createRange();
			range.setStartBefore(subProcessor.source);
			range.setEnd(breakPoint.container, breakPoint.offset);
			return range.toString().length;
		},
		'min-height': function (subProcessor: TestLayoutProcessor): number {
			if (subProcessor.lastEvent != "no-overflow") return 0;
			return subProcessor.layoutContext.bodyBottom - subProcessor.source.clientHeight;
		},
		/*
		'min-blank': function (subProcessor	): number {
			subProcessor.on("valid-break-point", function () {
				console.log(this);
			}, { inherited: true });
		}*/
	};


	const subProcessor = function (this: Fragmentor, ctx: SubProcessorContext): SubProcessorResult {
		const {currentElement: elem, currentCustomStyle: customStyle} = ctx;
		// The subprocessor is not called again when testing this element or if a parent has a selected layout (solutions for his descendants have been already selected).
		if (elem.hasAttribute('psp-sl-layout-test') || elem.closest('psp-sl-layout:not([psp-sl-layout-test])')) return false;

		const layoutsProp = css.getCustomPluginProp(customStyle, 'sl-layouts');
		const criteriaProp = css.getCustomPluginProp(customStyle, 'sl-criteria');
		if (layoutsProp == 'normal' && criteriaProp == 'valid') return;

		if (!walk.firstChild(elem, walk.isStaticNode)) return;

		return (async () => {
			elem.setAttribute('psp-sl-layout-test', '');
			const layoutsList = layoutsProp.split(/\s*,\s*/).map((list) => list.split(/\s+/));
			const validFlagsList: ValidFlags[] = [];
			const criteriaList = criteriaProp.split(/\s*,\s*/).map((list) => {
				const [criteria, validFlags] = list.split(/\s*\/\s*/);
				validFlagsList.push(validFlags ? validFlags.split(/\s+/) as ValidFlags : []);
				return criteria.split(/\s+/);
			});

			const critCtx: CritContext = { };
			if (elem.hasAttribute('ps-breaked-before')) {
				if (elem.hasAttribute('psp-sl-layout')) critCtx.layoutBefore = elem.getAttribute('psp-sl-layout');
				if (elem.hasAttribute('psp-sl-step')) critCtx.stepBefore = parseInt(elem.getAttribute('psp-sl-step'));
				if (elem.style.animation) critCtx.animBefore = elem.style.animation;
				elem.removeAttribute('psp-sl-layout');
				elem.removeAttribute('psp-sl-step');
				elem.removeAttribute('ps-anim-time');
				elem.style.animation = '';
			}


			let bestResult: LayoutResult;
			const testSubFragmentor = async (validFlags: ValidFlags, validCb: (subProcessor: TestLayoutProcessor) => void): Promise<boolean> => {
				const noInlineOverflow = validFlags && validFlags.includes('no-inline-overflow');
				const subProcessor = new TestLayoutProcessor(elem, this, noInlineOverflow);
				await subProcessor.start().ended;

				const {lastEvent, layoutContext: subCtx} = subProcessor;
				const valid = lastEvent == 'valid-break-point' || lastEvent == 'no-overflow';
				if (valid) validCb(subProcessor);
				subCtx.mutations.revert();
				return valid;
			};

			//elem.setAttribute('psp-sl-layout-test', '');
			for (let i = 0; i < layoutsList.length && !bestResult; i++) {
				const layouts = layoutsList[i];
				let criteria = criteriaList[i];
				let validFlags = validFlagsList[i];
				if (!criteria) criteria = criteriaList.at(-1);
				if (!validFlags) validFlags = validFlagsList.at(-1) || ['none'];

				if (criteria[0] == 'selected-before') {
					if (layouts.includes(critCtx.layoutBefore)) {
						if (critCtx.stepBefore) {
							const anim = elem.getAnimations().at(-1);
							anim.currentTime = critCtx.stepBefore * 1000;
						}
						await testSubFragmentor(validFlags, () => bestResult = {layout: critCtx.layoutBefore, animProp: critCtx.animBefore, step: critCtx.stepBefore, score: Infinity});
						if (bestResult) break;
					}
					criteria.shift();
				}

				const results: LayoutResult[][] = Array.from({length: criteria.length}, () => []);
				for (const layoutCode of layouts) {
					const testMuts = new mutations.Mutations();
					testMuts.setAttr(elem, 'psp-sl-layout', layoutCode);
					const stepsProp = css.getCustomPluginProp(elem, 'sl-steps');
					let steps = -1;
					let stepStop: StepStops;
					let animProp: string;
					let anim: Animation;
					if (stepsProp != "none") {
						const stepParts = stepsProp.split(/\s*\/\s*/);
						if (stepParts.length > 1) stepStop = stepParts[1] as StepStops;
						const [keyframes, stepsCount, timing] = stepParts[0].split(' ');
						animProp = `${stepsCount}s ${timing || 'linear'} paused ${keyframes}`;
						testMuts.setStyle(elem, { animation:  animProp });
						anim = elem.getAnimations().at(-1);
						if (!anim) logger.warn(`Unable to define the steps '${stepsProp}'. Is the corresponding keyframes at-rule missing ?`);
						else steps = parseInt(stepsCount);
					}

					if (steps && anim) {
						for (let step = 0; step < steps; step++) {
							anim.currentTime = step * 1000;
							const valid = await testSubFragmentor(validFlags, (subProcessor: TestLayoutProcessor) => {
								for (let j = 0; j < criteria.length; j++) {
									const criterion = criteria[j];
									const critFn = CRIT_FUNCTIONS[criterion];
									if (critFn) {
										const score = critFn(subProcessor, critCtx);
										results[j].push({layout: layoutCode, animProp, step, score});
									} else {
										logger.warn(`Unknown criterion '${criterion}'`);
									}
								}
							});
							if (stepStop) {
								if (valid && stepStop == 'first-valid') break;
								else if (!valid && stepStop == 'first-invalid') break;
							}
						}
					} else {
						await testSubFragmentor(validFlags, (subProcessor: TestLayoutProcessor) => {
							for (let j = 0; j < criteria.length; j++) {
								const criterion = criteria[j];
								const critFn = CRIT_FUNCTIONS[criteria[j]];
								if (critFn) {
									const score = critFn(subProcessor, critCtx);
									results[j].push({layout: layoutCode, score});
								} else {
									logger.warn(`Unknown criterion '${criterion}'`);
								}
							}
						});
					}

					testMuts.revert();
				}

				let selectedLayouts = Array.from(layouts);
				for (const critResults of results) {
					let selectedResults: LayoutResult[] = [];
					for (const critResult of critResults) {
						if (selectedLayouts.includes(critResult.layout)) {
							if (!selectedResults.length || critResult.score > selectedResults[0].score) selectedResults = [ critResult ];
							else if (critResult.score == selectedResults[0].score) selectedResults.push(critResult);
						}
					}
					bestResult = selectedResults[0];
					if (selectedResults.length > 1) selectedLayouts = selectedResults.map((result) => result.layout);
					else break;
				}
			}

			elem.removeAttribute('psp-sl-layout-test');
			elem.removeAttribute('psp-sl-layout-before');
			elem.removeAttribute('psp-sl-step-before');
			if (bestResult) {
				//boxMutations.setAttr(elem, 'psp-sl-layout', bestLayout.code);
				elem.setAttribute('psp-sl-layout', bestResult.layout);
				if (bestResult.step !== undefined) {
					logger.info(`Layout selected: ${bestResult.layout}.${bestResult.step} on '${elem.className}'`, 'ps-select-layout');
					const { mutations: boxMutations } = ctx.currentBox;
					boxMutations.setStyle(elem, { 'animation': bestResult.animProp });
					boxMutations.setAttr(elem, 'psp-sl-step', bestResult.step.toString());
					boxMutations.setAttr(elem, 'ps-anim-time', (bestResult.step * 1000).toString());
					const anim = elem.getAnimations().at(-1);
					anim.currentTime = bestResult.step * 1000;
				} else {
					logger.info(`Layout selected: ${bestResult.layout} on ${elem.className}`, 'ps-select-layout');
				}
				return false;
			} else {
				elem.removeAttribute('icdt-layout');
				elem.style.animation = '';
				return {event: 'no-break-point' as const};
			}
		})();
	};
	processor.prependSubProcessor(subProcessor);
});
