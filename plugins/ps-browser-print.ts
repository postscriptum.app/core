import type {PaginatorOptions} from "../dist/d.ts/pages";

const currentScriptSrc = (document.currentScript as HTMLScriptElement).src;

interface PrintOptions extends Omit<PaginatorOptions, 'source'> {
	scriptSrc: string;
	source: HTMLElement | ((doc: Document) => HTMLElement);
}

(window as any).postscriptumPrint = async function (src: string, options?: PrintOptions): Promise<void> {
	const printFrame = document.createElement('iframe');

	try {
		const loadPrintFrame = new Promise((resolve, reject) => {
			printFrame.src = src;
			printFrame.setAttribute('style', 'position: absolute; left: 0; top: 0; width: 100vw; height: 100vh; border: none; opacity: 0');
			printFrame.addEventListener('error', reject, { once: true });
			printFrame.addEventListener('load', resolve, { once: true });
			document.body.appendChild(printFrame);
		});

		const loadPsScript = 'postscriptum' in window ? Promise.resolve() : new Promise((resolve, reject) => {
			const psScript = document.createElement('script');
			psScript.src = options?.scriptSrc || new URL('../postscriptum.js', currentScriptSrc).href;
			psScript.addEventListener('load', resolve, { once: true });
			psScript.addEventListener('error', reject, { once: true });
			document.head.appendChild(printFrame);
		});

		await Promise.all([loadPrintFrame, loadPsScript]);

		const printOptions = Object.assign({}, options);
		if (typeof printOptions.source == "function") printOptions.source = printOptions.source(printFrame.contentDocument);

		if (!printOptions.defaultPageSize) {
			const useLetterSize = [ 'US', 'CA' ].includes(navigator.language.split('-')[1]);
			printOptions.defaultPageSize = 'A4';
			// Safari imposes non modifiable margins on print: 0.25in on each side except on bottom (why 0.57in ?)
			if (navigator.userAgent.match(/\bSafari\b/)) printOptions.defaultPageSize = useLetterSize ? 'calc(8.5in - .5in) calc(11in - .82in)' : 'calc(21cm - .5in) calc(29.7cm - .82in)';
			else if (useLetterSize) printOptions.defaultPageSize = 'letter';
		}

		const printProcess = postscriptum.pagination(printOptions as PaginatorOptions);
		await printProcess.start().ended;
		printFrame.contentWindow.print();
	} finally {
		printFrame.remove();
	}

};
