import type {Paginator} from "../dist/d.ts/pages";

interface LoadingErrorPluginOption {
	displayError: boolean;
	color: string;
	backgroundColor: string;
}

postscriptum.plugin<LoadingErrorPluginOption>('ps-loading', (processor: Paginator, options) => {
	const stylesheet = (destId, color, backgroundColor): string => /* language=CSS */ `#${destId} > ps-loading-mask {
		display: block;
		position: fixed;
		left: 0;
		top: 0;
		width: 100%;
		height: 100%;
		background-color: ${backgroundColor};
		pointer-events: none;
		z-index: 99999;
	}

	ps-loading-spinner {
		display: block;
		margin: 0 auto;
		width: 40px;
		height: 40px;
		position: relative;
		border: 4px solid ${color};
		animation: spinner 2.3s infinite ease;

		position: absolute;
		left: 50%;
		top: 50%;
		margin-top: -20px;
		margin-left: -20px;
	}

	ps-loading-spinner::before {
		content: "";
		display: block;
		vertical-align: top;
		width: 100%;
		background-color: ${color};
		animation: spinner-inner 2.3s infinite ease-in;
	}

	ps-message {
		display: block;
		position: relative;
		top: 50%;
		transform: translateY(-50%);
		text-align: center;
		font-size: 1.1em;
		color: ${color};
		font-family: monospace;

	}

	@keyframes spinner {
		0% {
			transform: rotate(0deg);
		}
		25% {
			transform: rotate(180deg);
		}
		50% {
			transform: rotate(180deg);
		}
		75% {
			transform: rotate(360deg);
		}
		100% {
			transform: rotate(360deg);
		}
	}

	@keyframes spinner-inner {
		0% {
			height: 100%;
		}
		25% {
			height: 100%;
		}
		50% {
			height: 0%;
		}
		75% {
			height: 0%;
		}
		100% {
			height: 100%;
		}
	}

	#${destId} {
		overflow: hidden;
	}

	::-webkit-scrollbar {
		display: none;
	}`;

	const doc = processor.source.ownerDocument;
	const {util, css} = postscriptum;

	const mask = processor.dest.appendChild(doc.createElement('ps-loading-mask'));
	const spinner = mask.appendChild(doc.createElement('ps-loading-spinner'));

	const destId = util.dom.id(processor.dest);
	const style = css.insertStyleSheet(doc, stylesheet(destId, options.color, options.backgroundColor), true, 'ps-loading-' + destId);

	processor.on('pagination-end', () => {
		mask.remove();
		(style.ownerNode as Element).remove();
	});

	processor.on('error', ({error}) => {
		console.error(error);

		if (options.displayError) {
			spinner.remove();
			const messageElt = mask.appendChild(doc.createElement('ps-message'));
			messageElt.textContent = 'Error: ' + error.message;
			setTimeout(() => {
				mask.remove();
				(style.ownerNode as Element).remove();
			}, 2000);
		}
		return true;
	});

}, {backgroundColor: 'white', color: 'grey', displayError: true});
