import type {Postscriptum} from "../dist/d.ts/postscriptum";
import type {Paginator} from "../dist/d.ts/pages";

interface MathjaxPluginOption {
	scriptUrl?: string;
	fontsUrl?: string;
	elements?: string[];
	config?: any;
}

(postscriptum as Postscriptum).plugin('ps-mathjax', (processor: Paginator, options: MathjaxPluginOption) => {
	const SCRIPT_URL = "https://cdn.jsdelivr.net/npm/mathjax@3/es5/startup.js";
	const {layout, util} = postscriptum;
	const {io} = util;

	let config = {
		startup: {
			elements: [processor.source],
		},
		loader: {
			load: ["input/tex-full", "input/mml", "output/chtml"]
		},
		chtml: {}
	} as any;

	if (options.config) config = io.mergeOptions({}, config, options.config);
	if (options.elements?.length) config.startup.elements = options.elements;
	if (options.fontsUrl) config.chtml.fontURL = options.fontsUrl;

	processor.on('process-start', async function (): Promise<void> {
		window.MathJax = config;
		await io.loadScript(options.scriptUrl || SCRIPT_URL);
		await window.MathJax.startup.promise;
	});
}, {scriptUrl: "", fontsUrl: "", elements: [], config: null});

declare global {
	interface Window {
		// eslint-disable-next-line @typescript-eslint/naming-convention
		MathJax: any;
	}
}