import type {Postscriptum} from "../dist/d.ts/postscriptum";
import type {Paginator} from "../dist/d.ts/pages";
import type {Fragmentor} from "../dist/d.ts/fragments";

interface ScaleToFitOptions {
	allowExpandBeforeBreak?: boolean;
}

/**
 * Plugin Scenari that shrinks or expands elements of a page to avoid blank area.
 *  - Requires elements to be blocks.
 *  - The shrink algorithm handles the break points avoided inside ancestors,
 *    but does not check the validity of the new break point (the break-before of the next box is not known).
 *  - Should be included last.
 */
(postscriptum as Postscriptum).plugin<ScaleToFitOptions>('sc-scale-to-fit', (processor: Paginator, options) => {
	const {css, util, layout} = postscriptum;
	const {logger, rects, ranges, walk} = util;

	CSS.registerProperty({
		name: '--psp-sc-stf-min-blank-height',
		syntax: '<length> | none',
		initialValue: 'none',
		inherits: false
	});
	CSS.registerProperty({
		name: '--psp-sc-stf-max-shrink',
		syntax: '<number>',
		initialValue: '1',
		inherits: false
	});
	CSS.registerProperty({
		name: '--psp-sc-stf-max-expand',
		syntax: '<number>',
		initialValue: '1',
		inherits: false
	});
	const embeddedContent = [ "img", "iframe", "embed", "object", "video" ];

	function process(this: Fragmentor, only?: "shrink" | "expand" ): string {
		const ctx = this.layoutContext;
		if (!options.allowExpandBeforeBreak && !walk.isBlockElement(ranges.nodeAtPoint(ctx.breakPoint))) return;
		const {breakBox, boxes} = ctx;

		let lastFullBoxIndex = -1;
		if (breakBox) lastFullBoxIndex = boxes.indexOf(breakBox) - (breakBox.unbreakable == "subFragmentor" ? 0 : 1);
		else if (options.allowExpandBeforeBreak) lastFullBoxIndex = boxes.length - 1;
		if (lastFullBoxIndex == -1) return;

		const lastFullBox = boxes[lastFullBoxIndex];
		if (!lastFullBox || !lastFullBox.mainBlock) return;

		const pageBodyRect = rects.boundingScrollRect(this.currentFragment.body);
		const minBlankHeightProp = css.getCustomPluginProp(this.currentFragment.container, 'sc-stf-min-blank-height');
		if (!minBlankHeightProp || minBlankHeightProp == "none") return;

		const minBlankHeight = css.computeLength(minBlankHeightProp, pageBodyRect.height);
		const blankHeight = pageBodyRect.bottom - lastFullBox.bottom;
		if (blankHeight < minBlankHeight) return;

		const dimByBlock: Map<HTMLElement, { width: number, height: number }> = new Map();
		const shrinkByBlock: Map<HTMLElement, number> = new Map();
		let shrinkBlocksHeight = 0;
		let shrinkBlocksMinHeight = 0;

		const expandByBlock: Map<HTMLElement, number> = new Map();
		let expandBlocksHeight = 0;

		for (const box of boxes) {
			if (box.mainBlock) {
				const elem = box.mainBlock;
				const shrinkProp = css.getCustomPluginProp(elem, 'sc-stf-max-shrink') || "1";
				const expandProp = css.getCustomPluginProp(elem, 'sc-stf-max-expand') || "1";
				if (shrinkProp != "1" || expandProp != "1") {
					const { width, height } = elem.getBoundingClientRect();
					dimByBlock.set(elem, {width, height});

					const style = getComputedStyle(elem);
					const parentStyle = getComputedStyle(elem.parentElement);
					const parentWidth = parseFloat(parentStyle.width);
					const parentHeight = parseFloat(parentStyle.height);

					if ((!only || only == "shrink") && shrinkProp != "1") {
						const minWidth = css.computeMinOrMaxLength(style.minWidth, parentWidth, 0);
						const minHeight = css.computeMinOrMaxLength(style.minHeight, parentHeight, 0);
						if (!isNaN(minWidth) && !isNaN(minHeight)) {
							const shrink = Math.max(minWidth / width, minHeight / height, parseFloat(shrinkProp));
							if (shrink < 1) {
								shrinkByBlock.set(elem, shrink);
								shrinkBlocksHeight += height;
								shrinkBlocksMinHeight += height * shrink;
							}
						}
					}

					if ((!only || only == "expand") && expandProp != "1" && box != breakBox) {
						// TODO Handles the padding on the page body
						const maxWidth = css.computeMinOrMaxLength(style.maxWidth, parentWidth, pageBodyRect.width);
						const maxHeight = css.computeMinOrMaxLength(style.maxHeight, parentHeight, pageBodyRect.height);
						if (!isNaN(maxWidth) && !isNaN(maxHeight)) {
							const expand = Math.min(maxWidth / width, maxHeight / height, parseFloat(expandProp));
							if (expand > 1) {
								expandByBlock.set(elem, expand);
								expandBlocksHeight += height;
							}
						}
					}
				}
				if (breakBox && box == breakBox) break;
			}
		}

		if (breakBox && shrinkByBlock.has(breakBox.mainBlock) && !breakBox.avoidBreakAfter) {
			let bottom = breakBox.bottom;
			let breakAvoidedParent = layout.testAvoidBreakInside(breakBox.mainBlock, ctx, this.avoidBreakTypes, ctx.breakPoint);
			if (breakAvoidedParent) {
				while (breakAvoidedParent.parentElement != ctx.root && !walk.isStatic(breakAvoidedParent.nextElementSibling)) {
					breakAvoidedParent = breakAvoidedParent.parentElement;
				}
				bottom = util.rects.boundingScrollRect(breakAvoidedParent).bottom;
			}
			let overflowHeight = bottom - pageBodyRect.bottom;
			if (overflowHeight > 0 && overflowHeight <= shrinkBlocksHeight - shrinkBlocksMinHeight) {
				const sortedByShrink = Array.from(shrinkByBlock.keys()).sort((img1: HTMLElement, img2: HTMLElement): number => {
					return shrinkByBlock.get(img2) * dimByBlock.get(img2).height - shrinkByBlock.get(img1) * dimByBlock.get(img1).height;
				});
				let targetShrink = (shrinkBlocksHeight - overflowHeight) / shrinkBlocksHeight;
				for (const block of sortedByShrink) {
					const shrink = Math.max(targetShrink, shrinkByBlock.get(block));
					const {width, height} = dimByBlock.get(block);
					const newHeight = height * shrink;
					if (embeddedContent.includes(block.localName)) {
						const ratio = width / height;
						block.style.height = newHeight + 'px';
						block.style.width = (newHeight * ratio) + 'px';
						block.style.boxSizing = "border-box";
					} else {
						const style = getComputedStyle(block);
						const newWidth = width * shrink;
						setTransformMargin(block, style, newWidth - width, newHeight - height);
						block.style.scale = shrink.toString();
					}

					shrinkBlocksHeight -= height;
					overflowHeight -= (height - newHeight);
					targetShrink = (shrinkBlocksHeight - overflowHeight) / shrinkBlocksHeight;
				}
				if (breakAvoidedParent) {
					if (breakAvoidedParent.parentElement == ctx.root && walk.lastChild(ctx.root, walk.isStaticNode) == breakAvoidedParent) ctx.breakPoint = ranges.positionAfter(ctx.root.lastChild);
					else ctx.breakPoint = ranges.positionAfter(breakAvoidedParent);
				} else {
					ctx.breakPoint = ranges.endPoint(breakBox.range);
				}
				ctx.breakBox = null;
				logger.info(`${shrinkByBlock.size} blocks shrinked.`, 'sc-scale-to-fit');
				return 'break-point-found';
			}
		}

		if (expandByBlock.size) {
			let underflowHeight = pageBodyRect.bottom - lastFullBox.bottom;
			const sortedByExpand = Array.from(expandByBlock.keys()).sort((img1: HTMLElement, img2: HTMLElement): number => {
				return expandByBlock.get(img2) * dimByBlock.get(img2).height - expandByBlock.get(img1) * dimByBlock.get(img1).height;
			});
			let targetExpand = (expandBlocksHeight + underflowHeight) / expandBlocksHeight;
			for (const block of sortedByExpand) {
				const expand = Math.min(targetExpand, expandByBlock.get(block));
				const {width, height} = dimByBlock.get(block);
				const newHeight = height * expand;
				if (embeddedContent.includes(block.localName)) {
					const ratio = width / height;
					block.style.height = newHeight + 'px';
					block.style.width = (newHeight * ratio) + 'px';
					block.style.boxSizing = "border-box";
				} else {
					const style = getComputedStyle(block);
					const newWidth = width * expand;
					setTransformMargin(block, style, newWidth - width, newHeight - height);
					block.style.scale = expand.toString();
				}
				expandBlocksHeight -= height;
				underflowHeight -= (newHeight - height);
				targetExpand = (expandBlocksHeight + underflowHeight) / expandBlocksHeight;
			}
			logger.info(`${expandByBlock.size} blocks expanded.`, 'sc-scale-to-fit');
			return 'break-point-found';
		}
	}

	processor.on('valid-break-point', function (this: Fragmentor): string {
		return process.call(this);
	});
	processor.on('no-break-point', function (this: Fragmentor): string {
		return process.call(this, !options.allowExpandBeforeBreak ? "skrink" : null);
	});
	if (options.allowExpandBeforeBreak) {
		processor.on('no-overflow', function (this: Fragmentor): string {
			return process.call(this, "expand");
		});
	}

	function setTransformMargin(block: HTMLElement, style: CSSStyleDeclaration, widthDelta: number, heightDelta: number): void {
		const { marginTop, marginRight, marginBottom, marginLeft } = style;
		const transformOrigin = [ "center", "center"];
		if (marginRight != "auto") {
			transformOrigin[0] = "left";
			block.style.marginRight = (parseFloat(marginRight) + widthDelta) + 'px';
		} else if (marginLeft != "auto") {
			transformOrigin[0] = "right";
			block.style.marginLeft = (parseFloat(marginLeft) + widthDelta) + 'px';
		}
		if (marginBottom != "auto") {
			transformOrigin[1] = "top";
			block.style.marginBottom = (parseFloat(marginBottom) + heightDelta) + 'px';
		} else if (marginTop != "auto") {
			transformOrigin[1] = "bottom";
			block.style.marginTop = (parseFloat(marginTop) + heightDelta) + 'px';
		}
		block.style.transformOrigin = transformOrigin.join(' ');
	}

}, { allowExpandBeforeBreak: false });


