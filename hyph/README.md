This directory contains the [hyphenation patterns](https://github.com/bramstein/hyphenation-patterns) for use with the [Hypher](https://github.com/bramstein/hypher) library, included in Postscriptum.

## License
The hyphenation language patterns are licensed under the LGPL (unless otherwise noted in the corresponding .README file) and copyrighted to their respective creators and maintainers.